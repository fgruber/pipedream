#!/usr/bin/env python3

__doc__ = '''
  Example showing how to use the pipedream benchmark module as a library.
  Generates a benchmark library and runs the benchmarks.
'''

import pathlib
import sys
from typing import *

try:
  import pipedream
except ImportError:
  sys.path.append(str(pathlib.Path(__file__).parent.parent / 'src'))

import pipedream.benchmark as pipebench
import pipedream.asm.ir    as pipeasm


def main():
  instruction_names = [
    'ADDPD_VR128f64x2_MEM64f64x2',
    'ADD_GPR64i64_MEM64i64',
    'ADD_GPR64i64_MEM64i64',
  ]

  ## optional, but will make your benchmark results much more reliable
  pipebench.set_scheduler_params()

  ## set up benchmark

  arch         = pipeasm.Architecture.for_name('x86')
  inst_set     = arch.instruction_set()
  instructions = [inst_set.instruction_for_name(name) for name in instruction_names]

  benchmark: pipebench.Benchmark_Spec = pipebench.Benchmark_Spec.from_instructions(
    arch=arch,
    kind=pipebench.Benchmark_Kind.THROUGHPUT,
    ## kernel will contain these instructions, in this order
    instructions             = instructions,
    ## benchmark will execute this many instructions in total (~approximate)
    num_dynamic_instructions = 2_000_000,
    ## benchmark kernel will be unrolled to be this many instructions long
    unrolled_length          = 200,
  )

  ## run benchmarks

  should_measure_performance_with_papi: bool = False#True

  if should_measure_performance_with_papi:
    perf_counters = pipebench.Perf_Counter_Spec.make_throughput_counters()
  else:
    perf_counters = None

  results: Iterable[pipebench.Benchmark_Run] = pipebench.run_benchmarks(
    arch                  = arch,
    benchmarks            = [benchmark],
    perf_counters         = perf_counters,
    ## how often is each benchmark run?
    num_iterations        = 50,
    num_warmup_iterations = 10,
    ## optional:
    ##   Integers between 0 and 100.
    ##   Results below/above the given percentiles are dropped.
    ##   Helps filter out noise, like runs where the OS scheduler did something weird.
    outlier_low  = 5,
    outlier_high = 100,
    ## where should we store the temporay files we generate?
    tmp_dir = '/tmp/some-place-I-do-not-really-care-about-where-exactly',
    ## extra log messages
    verbose = True,
    ## generated code contains errors checks
    debug = True,
  )

  ## print results

  for result in results:
    ipc: pipebench.Statistics = result.ipc

    print(result.name)
    print('  mean IPC:', ipc.mean)


if __name__ == '__main__':
  main()
