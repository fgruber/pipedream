#!/usr/bin/env python3

from pathlib import Path
from setuptools import setup, find_packages


def parse_requirements():
    reqs = []
    with open("requirements.txt", "r") as handle:
        for line in handle:
            reqs.append(line)
    return reqs


def check_instructions_xed():
    """ Checks that `instructions_xed.py` exists. Raises an Exception on failure. """
    root_dir = Path(__file__).parent
    instr_xed_path = root_dir / "src/pipedream/asm/x86/instructions_xed.py"
    if not instr_xed_path.is_file():
        raise Exception(
            (
                "Cannot find `instructions_xed.py` at {path}. Please check the "
                "README for more details."
            ).format(path=instr_xed_path.as_posix())
        )


check_instructions_xed()
packages = find_packages(where="src/")


setup(
    name="pipedream",
    version="0.0.1",
    description="Pipedream",
    author="CORSE",
    url="https://gitlab.inria.fr/fgruber/pipedream",
    packages=packages,
    package_dir={package: "src/" + package.replace(".", "/") for package in packages},
    include_package_data=True,
    long_description=open("README.md").read(),
    install_requires=parse_requirements(),
)
