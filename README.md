# Pipedream

## Install

### Generate the x86 instruction database

Before installing Pipedream, it is necessary to generate the database of x86
instructions from Intel XED:

```bash
  git submodule init
  git submodule update
  cd tools/extract-xed-instruction-database/
  cmake -B build -S .
  make -C build -j
  build/extract-xed-instruction-database print-instr-db -o ../../src/pipedream/asm/x86/instructions_xed.py
```

Alternatively, if you already have generated this file on another machine, you
may copy it over as `src/pipedream/asm/x86/instructions_xed.py`.

### Installing pipedream

It is recommended to install python dependencies, as well as pipedream itself,
into a virtualenv:

```bash
  virtualenv -p python3 venv
  source venv/bin/activate
  pip install -e .
```

Note that this will break if you did not first generate the x86 instructions
database (see above).
