
from pipedream.asm.ir import *
from pipedream.utils  import *
from pipedream.utils  import abc
import math
import random
import typing as ty

__all__ = [
  'Register_Allocator',
  'Self_Deps_Register_Allocator',
  'Minimize_Deps_Register_Allocator',
  'Minimize_Deps_Pooled_Register_Allocator',
  'Maximize_Deps_Register_Allocator',
]

LValue = RValue = None


class CacheAllocator():
  """
    Simple view of a cache. Allocates from two distincts locations: one for loads,
    the other for stores, following a round-robin policy)
  """

  def __init__(self, size, cacheline_size, associativity, global_offset = 0):
    self.size = size
    self.cacheline_size = cacheline_size
    self.associativity = associativity
    self.base = 0
    self.offset = 0
    self.global_offset = int(global_offset)
    self.nb_sets = int(size/(cacheline_size*associativity))

  def allocate(self, size):
    ret = self.base + self.offset + self.global_offset
    assert(self.cacheline_size%size == 0)
    self.offset += self.cacheline_size
    self.offset %= int(self.cacheline_size*self.nb_sets)
    return ret


class Register_Allocator(abc.ABC):
  """
    Abstract base class for all register allocators.
    These are not really register allocators in the classical sense,
    they just randomly assign registers to instructions in a way that either
    creates or avoid data dependencies (depending on the sub-class).
  """

  USE     = Use_Def.USE
  DEF     = Use_Def.DEF
  USE_DEF = Use_Def.USE_DEF

  @abc.abstractmethod
  def allocate(self, instructions: ty.List[Instruction], irb: IR_Builder) -> ty.List[Instruction]:
    """
      Allocate all unallocated operands (i.e. register operands with no register assigned)
      for all instructions in *instructions*.
      Returns a list with the updated instructions, the input list is not modified.
    """


class Self_Deps_Register_Allocator(Register_Allocator):
  """
    Allocator that gives at most one register per type to an instruction.
    Does not care about USE/DEF.
    Thus, if the kernel is always generated in the same order, this introduces
    flow-dependencies between versions of each instruction.

    This allocator keeps no state accross kernel iterations.
  """

  def __init__(self, regs: ty.List[Register]):
    self.regs = tuple(sorted(regs))

    assert self.regs, "allocator initialized with zero available registers?"

    self.alloc  = RegisterAllocator(self.regs)
    self.cache  = {}
    self.random = random.Random("look, I'm sooo random")

  @abc.override
  def allocate(self, instructions: ty.List[Instruction], irb: 'ir.IR_Builder') -> ty.List[Instruction]:
    return [self._allocate_inst(i) for i in instructions]

  def _allocate_inst(self, inst: Instruction) -> Instruction:
    self.cache.clear()

    for op in inst.operands:
      inst = inst.update_operand(op.name, self._make_op)

    return op

  @abc.override
  def allocate_operand(self, optype: ty.Type[RValue], usedef: Use_Def):
    if issubclass(optype, Immediate):
      return self.allocate_imm(optype)

    if usedef is self.USE:
      return self.allocate_src(optype)

    if usedef is self.DEF:
      return self.allocate_dst(optype)

    raise Allocation_Error('Cannot allocate optype ' + repr(optype) + ', usedef ' + repr(usedef))

  def _make_op(self, op):
    if isinstance(op, Immediate_Operand):
      val = op._arbitrary(self.random)
      return op.with_value(val)

    if isinstance(op, Register_Operand):
      usedef   = op.use_def
      regclass = op.register_class

      if usedef is self.USE:
        return op.with_register(self._allocate_src(regclass))

      assert usedef in (self.DEF, self.USE_DEF)

      return op.with_register(self._allocate_dst(regclass))

    raise NotImplementedError(inst, idx, op)

  def _allocate_src(self, optype: ty.Type[RValue]):
    return self._allocate_reg(optype)

  def _allocate_dst(self, optype: ty.Type[LValue]):
    return self._allocate_reg(optype)

  def _allocate_reg(self, optype):
    try:
      return self.cache[optype]
    except KeyError:
      reg = self.alloc.take_any(optype)
      self.cache[optype] = reg
      return reg


class Minimize_Deps_Register_Allocator(Register_Allocator):
  """
    Allocator that tries to minimize flow/true deps in output code.
    I.e. create code that is as parallel as possible.
    It still creates anti-dependences.
  """

  ALIGNMENT = 64

  def __init__(self, regs: [Register], memory_reg_load: Register = None,
               memory_reg_store: Register = None, alignment: int = 32,
               insert_dependency_breakers: bool = False,
               l1_size: int = 32768, l1_cacheline_size: int = 64,
               l1_associativity: int = 8
               ):
    self.regs = tuple(sorted(regs))

    assert self.regs, "allocator initialized with zero available registers?"

    assert memory_reg_load not in regs
    assert memory_reg_store not in regs

    assert alignment > 0 and math.log2(alignment).is_integer()

    self.alloc      = Register_Liveness_Tracker(self.regs)
    self.reg_load   = memory_reg_load
    self.reg_store  = memory_reg_store
    self.cache      = {}
    self.random     = random.Random("look, I'm sooo random")
    self.insert_dependency_breakers = insert_dependency_breakers
    self.mem_load   = CacheAllocator(l1_size/2, l1_cacheline_size, l1_associativity)
    self.mem_store  = CacheAllocator(l1_size/2, l1_cacheline_size, l1_associativity)
    self.alignment  = alignment

  @classmethod
  def memory_arena_size(clss, instructions: ty.List[Instruction], alignment: int = ALIGNMENT) -> int:
    """
      Calculate how big of a memory arena is needed when allocating a benchmark kernel.
    """

    offset = 0

    for i in instructions:
      for op in i.operands:
        if isinstance(op, Memory_Operand) and op.is_def:
          offset = clss._align_to(offset + op.memory_width, alignment)

    return offset + 4096

  @abc.override
  def allocate(self, instructions: ty.List[Instruction], irb: 'ir.IR_Builder') -> ty.List[Instruction]:
    out = []

    insert_dependency_breakers = self.insert_dependency_breakers

    for i in instructions:
      if insert_dependency_breakers:
        for op in i.operands:
          needs_breaker = False
          if isinstance(op, Register_Operand) and op.is_def and len(op.register_class) == 1:
            needs_breaker = True
          if needs_breaker:
            out += irb.emit_dependency_breaker(op.register_class[0])

      i = self._allocate_inst(i)
      out.append(i)

    return out

  def _allocate_inst(self, inst: Instruction) -> Instruction:
    for op in (op for op in inst.operands if op.is_def and op.is_virtual):
      inst = inst.update_operand(op.name, self._make_op)

    for op in (op for op in inst.operands if op.is_use and op.is_virtual):
      ## USE_DEF should already have been treated
      assert op.use_def is Use_Def.USE, [op, op.is_virtual, op.register]

      inst = inst.update_operand(op.name, self._make_op)

    return inst

  def _make_op(self, op):
    if isinstance(op, Immediate_Operand):
      val = op._arbitrary(self.random)
      return op.with_value(val)

    if isinstance(op, Register_Operand):
      usedef   = op.use_def
      regclass = op.register_class

      if usedef is self.USE:
        return op.with_register(self._allocate_src(regclass))

      assert usedef in (self.DEF, self.USE_DEF)

      return op.with_register(self._allocate_dst(regclass))

    if isinstance(op, Base_Displacement_Address_Operand):
      base = self._allocate_src(op.base.register_class)
      disp = op.displacement._arbitrary(self.random)

      return op.with_base(base).with_displacement(disp)

    if isinstance(op, Base_Displacement_Memory_Operand):
      assert self.reg_load is not None
      assert self.reg_store is not None

      if op.is_def:
          return op.with_base(self.reg_store).with_displacement(self.mem_store.allocate(op.memory_width//8))
      else:
          return op.with_base(self.reg_load).with_displacement(self.mem_load.allocate(op.memory_width//8))

    raise NotImplementedError(op)

  def _allocate_src(self, optype: Register_Class):
    try:
      return self.cache[optype]
    except KeyError:
      reg = self.cache[optype] = self._allocate_reg(optype)
      return reg

  def _allocate_dst(self, optype: Register_Class):
    return self._allocate_reg(optype)

  def _allocate_reg(self, optype: Register_Class):
    try:
      return self.alloc.take_any(optype)
    except Allocation_Error:
      ## free all registers and carry on
      ## TODO: add dependency breaker on newly added register
      self.alloc = Register_Liveness_Tracker(self.regs)
      self.cache.clear()
      return self._allocate_dst(optype)

  @staticmethod
  def _align_to(ptr: int, alignment: int) -> int:
    return ptr + alignment - (ptr % alignment)


class Maximize_Deps_Register_Allocator(Register_Allocator):
  """
    Allocator that tries to maximize flow/true deps in output code.
    I.e. create code that executes completely sequential.
    It still creates anti-dependences.
  """

  def __init__(self, regs: [Register], memory_reg_load: Register = None, memory_reg_store = None):
    self.regs = tuple(sorted(regs))

    assert self.regs, "allocator initialized with zero available registers?"

    assert memory_reg_load not in regs
    assert memory_reg_store not in regs

    self.alloc      = Register_Liveness_Tracker(self.regs)
    self.memory_reg = memory_reg_load
    self.cache      = {}
    self.random     = random.Random("look, I'm sooo random")

  @classmethod
  def memory_arena_size(clss, instructions: ty.List[Instruction], alignment: int = 0) -> int:
    """
      Calculate how big of a memory arena is needed when allocating a benchmark kernel.
    """

    # all loads/store will go to the same address, just make sure the biggest machine load/store is supported.
    return 4096

  @abc.override
  def allocate(self, instructions: ty.List[Instruction], irb: 'ir.IR_Builder') -> ty.List[Instruction]:
    return [self._allocate_inst(i) for i in instructions]

  def _allocate_inst(self, inst: Instruction) -> Instruction:
    for op in (op for op in inst.operands if op.is_virtual):
      inst = inst.update_operand(op.name, self._make_op)

    return inst

  def _make_op(self, op):
    if isinstance(op, Immediate_Operand):
      val = op._arbitrary(self.random)
      return op.with_value(val)

    if isinstance(op, Register_Operand):
      usedef   = op.use_def
      regclass = op.register_class

      if usedef is self.USE:
        return op.with_register(self._allocate_reg(regclass))

      assert usedef in (self.DEF, self.USE_DEF)

      return op.with_register(self._allocate_reg(regclass))

    if isinstance(op, Base_Displacement_Address_Operand):
      base = self._allocate_reg(op.base.register_class)
      disp = op.displacement._arbitrary(self.random)

      return op.with_base(base).with_displacement(disp)

    if isinstance(op, Base_Displacement_Memory_Operand):
      assert self.memory_reg is not None

      base_reg     = self.memory_reg.as_width(op.address_width)
      displacement = 0

      return op.with_base(self.memory_reg).with_displacement(displacement)

    raise NotImplementedError(op)

  def _allocate_reg(self, optype: Register_Class):
    try:
      return self.cache[optype]
    except KeyError:
      bits: int = optype[0].width
      assert all(r.width == bits for r in optype), f'TODO: variable width operands: {optype}'

      ## check if we can reuse a register used by another register class
      for reg_class, reg in self.cache.items():
        for optype_reg in optype:
          if optype_reg.widest == reg.widest:
            reg = reg.as_width(bits)
            self.cache[optype] = reg
            return reg

      ## allocate a new register
      reg = self.cache[optype] = self.alloc.take_any(optype)
      return reg

class Minimize_Deps_Pooled_Register_Allocator(Minimize_Deps_Register_Allocator):
  """
    Enriched version of the Minimize_Deps_Register_Allocator that allocates from
    operands from a precomputed pool of registers.
  """

  def __init__(self, reg_pools: ty.Dict[Operand, ty.List[Register]],\
          memory_reg_load: Register = None,
          memory_reg_store: Register = None,
          alignment: int = 32,
          insert_dependency_breakers: bool = False,
          l1_size: int = 32768, l1_cacheline_size: int = 64,
          l1_associativity: int = 8):
    self.reg_pools = reg_pools
    self.cache      = {}
    self.alloc:ty.Dict[Register_Operand, Register_Liveness_Tracker] = {}
    allocs:ty.Dict[Register_Class, Register_Liveness_Tracker]   = {}
    for op, pool in self.reg_pools.items():
      assert memory_reg_load not in pool
      assert memory_reg_store not in pool
      assert len(pool) > 0, "allocator initialized with zero available registers?"
      if (allocs.get(pool) == None):
        allocs[pool] = Register_Liveness_Tracker(pool)
      self.alloc[op] = allocs[pool]
      self.cache[pool] = {}

    assert alignment > 0 and math.log2(alignment).is_integer()
    self.reg_load   = memory_reg_load
    self.reg_store  = memory_reg_store
    self.random     = random.Random("look, I'm sooo random")
    self.insert_dependency_breakers = insert_dependency_breakers
    self.mem_load   = CacheAllocator(l1_size/2, l1_cacheline_size, l1_associativity)
    self.mem_store  = CacheAllocator(l1_size/2, l1_cacheline_size, l1_associativity)
    self.alignment  = alignment

  @abc.override
  def allocate(self, instructions: ty.List[Instruction], irb: 'ir.IR_Builder') -> ty.List[Instruction]:
    out = []

    insert_dependency_breakers = self.insert_dependency_breakers

    for i in instructions:
      if insert_dependency_breakers:
        for op in i.operands:
          needs_breaker = False
          if isinstance(op, Register_Operand) and op.is_def and len(op.register_class) == 1:
            needs_breaker = True
          if needs_breaker:
            out += irb.emit_dependency_breaker(op.register_class[0])

      i = self._allocate_inst(i)
      out.append(i)

    return out

  def _allocate_inst(self, inst: Instruction) -> Instruction:
    for op in (op for op in inst.operands if op.is_def and op.is_virtual):
      inst = inst.update_operand(op.name, self._make_op)

    for op in (op for op in inst.operands if op.is_use and op.is_virtual):
      ## USE_DEF should already have been treated
      assert op.use_def is Use_Def.USE, [op, op.is_virtual, op.register]

      inst = inst.update_operand(op.name, self._make_op)

    return inst

  def _make_op(self, op):
    if isinstance(op, Immediate_Operand):
      val = op._arbitrary(self.random)
      return op.with_value(val)

    if isinstance(op, Register_Operand):
      usedef   = op.use_def

      if usedef is self.USE:
        return op.with_register(self._allocate_src(op))

      assert usedef in (self.DEF, self.USE_DEF)

      return op.with_register(self._allocate_dst(op))

    if isinstance(op, Base_Displacement_Address_Operand):
      base = self._allocate_src(op.base)
      disp = op.displacement._arbitrary(self.random)

      return op.with_base(base).with_displacement(disp)

    if isinstance(op, Base_Displacement_Memory_Operand):
      assert self.reg_load is not None
      assert self.reg_store is not None

      if op.is_def:
          return op.with_base(self.reg_store).with_displacement(self.mem_store.allocate(op.memory_width//8))
      else:
          return op.with_base(self.reg_load).with_displacement(self.mem_load.allocate(op.memory_width//8))

    raise NotImplementedError(op)

  def _allocate_src(self, op: Register_Operand):
    try:
      return self.cache[self.reg_pools[op]][op.register_class]
    except KeyError:
      reg = self.cache[self.reg_pools[op]][op.register_class] = self._allocate_reg(op)
      return reg

  def _allocate_dst(self, op: Register_Operand):
    return self._allocate_reg(op)

  def _allocate_reg(self, op: Register_Operand):
    try:
      return self.alloc[op].take_any(op.register_class)
    except Allocation_Error:
      ## free all registers and carry on
      ## TODO: add dependency breaker on newly added register
      self.alloc[op].free_all()
      self.cache[self.reg_pools[op]].clear()
      return self._allocate_dst(op)
