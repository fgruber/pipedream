
from pipedream.asm   import ir
from pipedream.utils import abc

import typing as ty


__all__ = [
  'ASM_Writer',
]


class ASM_Writer(abc.ABC):
  def __init__(self, file: ty.IO[str]):
    self._file = file

  @abc.abstractmethod
  def begin_file(self, name):
    pass

  @abc.abstractmethod
  def end_file(self, name):
    pass

  @abc.abstractmethod
  def emit_label(self, label):
    pass

  @abc.abstractmethod
  def align(self):
    pass

  @abc.abstractmethod
  def insts(self, insts: ty.Sequence[ir.Instruction]):
    """
      Print the given instructions
    """

  @abc.abstractmethod
  def emit_operand(self, arg: ir.Operand) -> str:
    """
      Render an operand of an assembly instruction to a string.
    """

  @abc.abstractmethod
  def comment(self, *args):
    """Copy args into output as comment."""

  @abc.abstractmethod
  def begin_function(self, function_name: str):
    """ Emit code to start a new function """

  @abc.abstractmethod
  def end_function(self, function_name: str):
    """ Emit code to end a function """

  @abc.abstractmethod
  def global_byte_array(self, name: str, size: int, alignment: int):
    """ Emit directives to declare a global array with given name and size. """

  def print(self, indent, *args):
    if args:
      if indent:
        print(' ' * (indent * 2), end='', file=self._file)

      print(*args, file=self._file)
    else:
      print(file=self._file)
