
from pipedream.utils         import abc
from pipedream.asm.ir        import *

__all__ = [
  'X86_Register_Set',

  'X86_Register',

  'ANY_REGISTER',
  'ARGUMENT_REGISTER',
  'CALLER_SAVED',
  'CALLEE_SAVED',
  'GPR',
  'GPR64',
  'GPR32',
  'GPR16',
  'GPR8',
  'GPR8NOREX',
  'VR',
  'VR64',
  'VR128',
  'VRX128',
  'VR256',
  'VRX256',
  'VRX512',
  'CL_REGISTER',
  'FLAGS_REGISTER',
  'PC_REGISTER',
  'STACK_POINTER_REGISTER',
  'BASE_REGISTER_64',
  'BASE_REGISTER_32',
  'BASE_REGISTER_16',
  'INDEX_REGISTER_64',
  'INDEX_REGISTER_32',
  'INDEX_REGISTER_16',
]


class X86_Register(Register):
  def __init__(self, name: str, att_asm_name: str, idx: int, width: int, subs: set):
    self._idx          = idx
    self._name         = name
    self._att_asm_name = att_asm_name
    self._width        = width

    self._subs   = set(subs)
    self._supers = set()

  @property
  @abc.override
  def name(self) -> str:
    return self._name

  @property
  @abc.override
  def att_asm_name(self) -> str:
    return self._att_asm_name

  @property
  @abc.override
  def width(self):
    """Bit width of register."""
    return self._width

  @abc.override
  def as_width(self, bits: int) -> 'X86_Register':
    return self._as_width[bits]

  @property
  @abc.override
  def widest(self) -> 'X86_Register':
    return self._widest

  @property
  @abc.override
  def sub_registers(self):
    return iter(self._subs)

  @property
  @abc.override
  def super_registers(self):
    return iter(self._supers)

  @property
  @abc.override
  def aliases(self):
    return iter(self._aliases)

  def __lt__(self, other):
    if type(self) is not type(other):
      return NotImplemented

    return self._idx < other._idx

  def __str__(self):
    return type(self).__name__ + '(' + self.name + ')'

  def __repr__(self):
    return type(self).__name__ + '(' + self.name + ')'

  def __deepcopy__(self, memo):
    return self


_ALL_REGISTERS_ = []

## hack to allow putting register objects in module scope
self = vars()


def _def_x86_reg(name: str, width: int, *immediate_sub_regs, att_asm_name: str = None):
  assert name not in self, 'duplicate register def'

  if not att_asm_name:
    att_asm_name = '%' + name.lower()

  idx = len(_ALL_REGISTERS_)
  reg = X86_Register(name, att_asm_name, idx, width, frozenset(immediate_sub_regs))

  self[name] = reg
  _ALL_REGISTERS_.append(reg)
  __all__.append(reg.name)


## in order of encoding
## http://wiki.osdev.org/X86-64_Instruction_Encoding#Registers

# TODO: AVX write mask registers k0-k7

# Return value, caller-saved
_def_x86_reg('RAX',      64, 'EAX')
_def_x86_reg('EAX',      32, 'AX')
_def_x86_reg('AX',       16, 'AL', 'AH')
_def_x86_reg('AL',        8)
_def_x86_reg('AH',        8)
# 4th argument, caller-saved
_def_x86_reg('RCX',      64, 'ECX')
_def_x86_reg('ECX',      32, 'CX')
_def_x86_reg('CX',       16, 'CL', 'AH')
_def_x86_reg('CL',        8)
_def_x86_reg('CH',        8)
# 3rd argument, caller-saved
_def_x86_reg('RDX',      64, 'EDX')
_def_x86_reg('EDX',      32, 'DX')
_def_x86_reg('DX',       16, 'DL', 'DH')
_def_x86_reg('DL',        8)
_def_x86_reg('DH',        8)
# Local variable, callee-saved
_def_x86_reg('RBX',      64, 'EBX')
_def_x86_reg('EBX',      32, 'BX')
_def_x86_reg('BX',       16, 'BL', 'BH')
_def_x86_reg('BL',        8)
_def_x86_reg('BH',        8)
# Stack pointer, callee-saved
_def_x86_reg('RSP',      64, 'ESP')
_def_x86_reg('ESP',      32, 'SP')
_def_x86_reg('SP',       16, 'SPL')
_def_x86_reg('SPL',       8)
# Local variable, callee-saved
_def_x86_reg('RBP',      64, 'EBP')
_def_x86_reg('EBP',      32, 'BP')
_def_x86_reg('BP',       16, 'BPL')
_def_x86_reg('BPL',       8)
# 2nd argument, caller-saved
_def_x86_reg('RSI',      64, 'ESI')
_def_x86_reg('ESI',      32, 'SI')
_def_x86_reg('SI',       16, 'SIL')
_def_x86_reg('SIL',       8)
# 1st argument, caller-saved
_def_x86_reg('RDI',      64, 'EDI')
_def_x86_reg('EDI',      32, 'DI')
_def_x86_reg('DI',       16, 'DIL')
_def_x86_reg('DIL',       8)
# 5th argument, caller-saved
_def_x86_reg('R8',       64, 'R8D')
_def_x86_reg('R8D',      32, 'R8W')
_def_x86_reg('R8W',      16, 'R8B')
_def_x86_reg('R8B',       8)
# 6th argument, caller-saved
_def_x86_reg('R9',       64, 'R9D')
_def_x86_reg('R9D',      32, 'R9W')
_def_x86_reg('R9W',      16, 'R9B')
_def_x86_reg('R9B',       8)
# Scratch/temporary, caller-saved
_def_x86_reg('R10',      64, 'R10D')
_def_x86_reg('R10D',     32, 'R10W')
_def_x86_reg('R10W',     16, 'R10B')
_def_x86_reg('R10B',      8)
# Scratch/temporary, caller-saved
_def_x86_reg('R11',      64, 'R11D')
_def_x86_reg('R11D',     32, 'R11W')
_def_x86_reg('R11W',     16, 'R11B')
_def_x86_reg('R11B',      8)
# Local variable, callee-saved
_def_x86_reg('R12',      64, 'R12D')
_def_x86_reg('R12D',     32, 'R12W')
_def_x86_reg('R12W',     16, 'R12B')
_def_x86_reg('R12B',      8)
# Local variable, callee-saved
_def_x86_reg('R13',      64, 'R13D')
_def_x86_reg('R13D',     32, 'R13W')
_def_x86_reg('R13W',     16, 'R13B')
_def_x86_reg('R13B',      8)
# Local variable, callee-saved
_def_x86_reg('R14',      64, 'R14D')
_def_x86_reg('R14D',     32, 'R14W')
_def_x86_reg('R14W',     16, 'R14B')
_def_x86_reg('R14B',      8)
# Local variable, callee-saved
_def_x86_reg('R15',      64, 'R15D')
_def_x86_reg('R15D',     32, 'R15W')
_def_x86_reg('R15W',     16, 'R15B')
_def_x86_reg('R15B',      8)

# Instruction pointer
_def_x86_reg('RIP',      64, 'EIP')
_def_x86_reg('EIP',      32, 'IP')
_def_x86_reg('IP',       16)

# Status/condition code bits
_def_x86_reg('RFLAGS',  64, 'EFLAGS')
_def_x86_reg('EFLAGS',  32, 'FLAGS')
_def_x86_reg('FLAGS',   16)

# weird system registers
# TODO: fix bit widths
_def_x86_reg('CS', 16)
_def_x86_reg('DS', 16)
_def_x86_reg('ES', 16)
_def_x86_reg('FS', 16)
_def_x86_reg('GS', 16)
_def_x86_reg('SS', 16)
_def_x86_reg('FSBASE', 16)
_def_x86_reg('GSBASE', 16)
_def_x86_reg('TR', 16)
_def_x86_reg('LDTR', 16)
_def_x86_reg('GDTR', 16)
_def_x86_reg('SSP', 16)
_def_x86_reg('TSC', 32)
_def_x86_reg('TSCAUX', 16)
_def_x86_reg('X87STATUS', 16)
_def_x86_reg('X87TAG', 16)
_def_x86_reg('X87CONTROL', 16)
_def_x86_reg('MXCSR', 32)
_def_x86_reg('MSRS', 64)


# CPU control registers
# TODO: fix bit widths
for i in range(16):
  _def_x86_reg(f'CR{i}', 16)
_def_x86_reg('XCR0', 32)

for i in range(8):
  ## FIXME: this is probably wrong
  _def_x86_reg(f'MM{i}', 64)
  _def_x86_reg(f'ST{i}', 80, f'MM{i}', att_asm_name=f'%st({i})')

for i in range(32):
  _def_x86_reg(f'XMM{i}', 128)

for i in range(32):
  _def_x86_reg(f'YMM{i}', 256, f'XMM{i}')

for i in range(32):
  _def_x86_reg(f'ZMM{i}', 512, f'YMM{i}')


## fill in sub/super registers
for reg in _ALL_REGISTERS_:
  subs = set()

  for sub in reg._subs:
    if type(sub) is str:
      sub = self[sub]

    assert isinstance(sub, X86_Register)
    subs.add(sub)
    sub._supers.add(reg)

  reg._subs = subs

for reg in _ALL_REGISTERS_:
  reg._supers  = tuple(sorted(reg._supers))
  reg._subs    = tuple(sorted(reg._subs))
  reg._aliases = tuple(sorted(set(reg.all_sub_registers) | set(reg.all_super_registers)))

  as_width = {
    reg.width: reg
  }
  widest = reg

  for alias in reg._aliases:
    if alias in [AH, BH, CH, DH]:
      continue

    alias.width not in as_width, (reg, alias, as_width)

    as_width[alias.width] = alias

    if alias.width > widest.width:
      widest = alias

  reg._as_width = as_width
  reg._widest   = widest

del self


################################################################################
### register classes

_ALL_REGISTER_CLASSES_ = []


def _def_x86_register_class(name, *registers, **aliases):
  ## put register aliases in module scope
  for k, v in aliases.items():
    assert k not in globals() or globals()[k] is v

    globals()[k] = v
    __all__.append(k)

  clss = Register_Class(name, registers, aliases)
  _ALL_REGISTER_CLASSES_.append(clss)
  globals()[name] = clss
  __all__.append(name)


_def_x86_register_class(
  'ANY_REGISTER',
  *_ALL_REGISTERS_
)

_def_x86_register_class(
  'ARGUMENT_REGISTER',
  RDI, RSI, RDX, RCX, R8, R9,
  ARG_1=RDI,
  ARG_2=RSI,
  ARG_3=RDX,
  ARG_4=RCX,
  ARG_5=R8,
  ARG_6=R9,
)

_def_x86_register_class(
  'CALLER_SAVED',
  RAX, RDI, RSI, RDX, RCX, R8, R9, R10, R11
)

_def_x86_register_class(
  'CALLEE_SAVED',
  RBX, RBP, R12, R13, R14, R15,
)

_def_x86_register_class(
  'GPR',
  RAX,
  EAX,
  AX,
  AL,
  # 1st argument, caller-saved
  RDI,
  EDI,
  DI,
  DIL,
  # 2nd argument, caller-saved
  RSI,
  ESI,
  SI,
  SIL,
  # 3rd argument, caller-saved
  RDX,
  EDX,
  DX,
  DL,
  # 4th argument, caller-saved
  RCX,
  ECX,
  CX,
  CL,
  # 5th argument, caller-saved
  R8,
  R8D,
  R8W,
  R8B,
  # 6th argument, caller-saved
  R9,
  R9D,
  R9W,
  R9B,
  # Scratch/temporary, caller-saved
  R10,
  R10D,
  R10W,
  R10B,
  # Scratch/temporary, caller-saved
  R11,
  R11D,
  R11W,
  R11B,
  # Stack pointer, callee-saved
  RSP,
  ESP,
  SP,
  SPL,
  # Local variable, callee-saved
  RBX,
  EBX,
  BX,
  BL,
  # Local variable, callee-saved
  RBP,
  EBP,
  BP,
  BPL,
  # Local variable, callee-saved
  R12,
  R12D,
  R12W,
  R12B,
  # Local variable, callee-saved
  R13,
  R13D,
  R13W,
  R13B,
  # Local variable, callee-saved
  R14,
  R14D,
  R14W,
  R14B,
  # Local variable, callee-saved
  R15,
  R15D,
  R15W,
  R15B,
)

_def_x86_register_class(
  'GPR64',
  *[r for r in GPR if r.width == 64]
)

_def_x86_register_class(
  'GPR32',
  *[r for r in GPR if r.width == 32]
)

_def_x86_register_class(
  'GPR16',
  *[r for r in GPR if r.width == 16]
)

_def_x86_register_class(
  'GPR8',
  *[r for r in GPR if r.width == 8 if r not in (AH, BH, CH, DH)]
)

_def_x86_register_class(
  'GPR8NOREX',
  # AL, BL, CL, DL,
  AH, BH, CH, DH,
)

_def_x86_register_class(
  'FPST',
  ST0, ST1, ST2, ST3, ST4, ST5, ST6, ST7
)

_def_x86_register_class(
  'VR',
  MM0,
  MM1,
  MM2,
  MM3,
  MM4,
  MM5,
  MM6,
  MM7,

  XMM0,
  XMM1,
  XMM2,
  XMM3,
  XMM4,
  XMM5,
  XMM6,
  XMM7,
  XMM8,
  XMM9,
  XMM10,
  XMM11,
  XMM12,
  XMM13,
  XMM14,
  XMM15,
  XMM16,
  XMM17,
  XMM18,
  XMM19,
  XMM20,
  XMM21,
  XMM22,
  XMM23,
  XMM24,
  XMM25,
  XMM26,
  XMM27,
  XMM28,
  XMM29,
  XMM30,
  XMM31,

  YMM0,
  YMM1,
  YMM2,
  YMM3,
  YMM4,
  YMM5,
  YMM6,
  YMM7,
  YMM8,
  YMM9,
  YMM10,
  YMM11,
  YMM12,
  YMM13,
  YMM14,
  YMM15,
  YMM16,
  YMM17,
  YMM18,
  YMM19,
  YMM20,
  YMM21,
  YMM22,
  YMM23,
  YMM24,
  YMM25,
  YMM26,
  YMM27,
  YMM28,
  YMM29,
  YMM30,
  YMM31,

  ZMM0,
  ZMM1,
  ZMM2,
  ZMM3,
  ZMM4,
  ZMM5,
  ZMM6,
  ZMM7,
  ZMM8,
  ZMM9,
  ZMM10,
  ZMM11,
  ZMM12,
  ZMM13,
  ZMM14,
  ZMM15,
  ZMM16,
  ZMM17,
  ZMM18,
  ZMM19,
  ZMM20,
  ZMM21,
  ZMM22,
  ZMM23,
  ZMM24,
  ZMM25,
  ZMM26,
  ZMM27,
  ZMM28,
  ZMM29,
  ZMM30,
  ZMM31,
)

_def_x86_register_class(
  'VR64',
  *[r for r in VR if r.width == 64]
)

_def_x86_register_class(
  'VRX128',
  *[r for r in VR if r.width == 128]
)

_def_x86_register_class(
  'VRX256',
  *[r for r in VR if r.width == 256]
)

_def_x86_register_class(
  'VRX512',
  *[r for r in VR if r.width == 512]
)

_def_x86_register_class(
  'VR128',
  *VRX128[0:16]
)
_def_x86_register_class(
  'VR256',
  *VRX256[0:16]
)

_def_x86_register_class(
  'CL_REGISTER',
  CL
)

_def_x86_register_class(
  'STACK_POINTER_REGISTER',
  SP, ESP, RSP,
)

_def_x86_register_class(
  'FLAGS_REGISTER',
  FLAGS, EFLAGS, RFLAGS,
)

_def_x86_register_class(
  'PC_REGISTER',
  IP, EIP, RIP,
)

## singleton register classes
for reg in [
  FLAGS, EFLAGS, RFLAGS,
  IP, EIP, RIP,
  SP, ESP, RSP,

  CS, DS, ES, FS, GS, SS,
  FSBASE, GSBASE,
  TR, LDTR, GDTR,

  SSP,

  TSCAUX,

  CR0, CR1, CR2, CR3, CR4, CR5, CR6, CR7, CR8, CR9, CR10, CR11, CR12, CR13, CR14, CR15,
  XCR0,

  AL,
  CL,

  AH,

  AX,
  BX,
  CX,
  DX,
  DI,
  SI,
  BP,
  SP,

  EAX,
  EBX,
  ECX,
  EDX,
  EDI,
  ESI,
  EBP,
  ESP,

  RAX,
  RBX,
  RCX,
  RDX,
  RDI,
  RSI,
  RBP,
  RSP,
  R8,
  R9,
  R10,
  R11,
  R12,
  R13,
  R14,
  R15,

  XMM0,

  ST0,
  ST1,
  ST2,
  ST3,
  ST4,
  ST5,
  ST6,
  ST7,

  X87STATUS,
  X87CONTROL,
  X87TAG,
  MXCSR,
  TSC,
  MSRS,
]:
  _def_x86_register_class('RC_' + reg.name, reg)


### memory addressing
## https://en.wikipedia.org/wiki/X86#Addressing_modes

_def_x86_register_class(
  'BASE_REGISTER_16',
  BX, BP,
)
_def_x86_register_class(
  'BASE_REGISTER_32',
  EAX, EBX, ECX, EDX, ESP, EBP, ESI, EDI,
)
_def_x86_register_class(
  'BASE_REGISTER_64',
  *GPR64, RSP, RIP,
)

_def_x86_register_class(
  'INDEX_REGISTER_16',
  SI, DI,
)
_def_x86_register_class(
  'INDEX_REGISTER_32',
  EAX, EBX, ECX, EDX, EBP, ESI, EDI,
)
_def_x86_register_class(
  'INDEX_REGISTER_64',
  *GPR64,
)


################################################################################
### legacy

# 'GPRS',
# [Reg64, Reg32, Reg16, Reg8],
# ['rax', 'eax',  'ax',   'al'],    # Return value, caller-saved
# ['rdi', 'edi',  'di',   'dil'],   # 1st argument, caller-saved
# ['rsi', 'esi',  'si',   'sil'],   # 2nd argument, caller-saved
# ['rdx', 'edx',  'dx',   'dl'],    # 3rd argument, caller-saved
# ['rcx', 'ecx',  'cx',   'cl'],    # 4th argument, caller-saved
# ['r8',  'r8d',  'r8w',  'r8b'],   # 5th argument, caller-saved
# ['r9',  'r9d',  'r9w',  'r9b'],   # 6th argument, caller-saved
# ['r10', 'r10d', 'r10w', 'r10b'],  # Scratch/temporary, caller-saved
# ['r11', 'r11d', 'r11w', 'r11b'],  # Scratch/temporary, caller-saved
# ['rsp', 'esp',  'sp',   'spl'],   # Stack pointer, callee-saved
# ['rbx', 'ebx',  'bx',   'bl'],    # Local variable, callee-saved
# ['rbp', 'ebp',  'bp',   'bpl'],   # Local variable, callee-saved
# ['r12', 'r12w', 'r12w', 'r12b'],  # Local variable, callee-saved
# ['r13', 'r13d', 'r13w', 'r13b'],  # Local variable, callee-saved
# ['r14', 'r14d', 'r14w', 'r14b'],  # Local variable, callee-saved
# ['r15', 'r15d', 'r15w', 'r15b'],  # Local variable, callee-saved
# ['rip', 'eip', 'ip'],             # Instruction pointer
# ['rflags', 'eflags'],             # Status/condition code bits


class X86_Register_Set(Register_Set):
  @abc.override
  def stack_pointer_register(self) -> 'Register':
    return RSP

  @abc.override
  def register_classes(self) -> ['Register_Class']:
    return _ALL_REGISTER_CLASSES_

  @abc.override
  def all_registers(self) -> 'Register_Class':
    return _ALL_REGISTERS_

  @abc.override
  def argument_registers(self) -> 'Register_Class':
    return ARGUMENT_REGISTER

  @abc.override
  def callee_save_registers(self) -> 'Register_Class':
    return CALLEE_SAVED
