"""
  I tried to keep this file clean, I swear.
  x86 is just an extremely complex instruction set.
"""

import typing as ty
import random

import pipedream.utils.abc as abc

from pipedream.utils import *
from pipedream.asm.ir import *
from pipedream.asm.allocator import *
from pipedream.asm.asmwriter import *

from pipedream.benchmark.types import Loop_Overhead

from . import registers
from . import operands

from .asmwriter import *
from .operands import *
from .registers import *
from .instructions import *

__all__ = [
    *registers.__all__,
    *operands.__all__,
    *instructions.__all__,
]


class X86_Architecture(Architecture):
    def __init__(self):
        self._instruction_set = X86_Instruction_Set()
        self._register_set = X86_Register_Set()
        self._asm_dialects = [X86_ATT_ASM_Dialect()]

    @property
    @abc.override
    def name(self) -> str:
        return "x86"

    @property
    @abc.override
    def nb_vector_reg(self) -> int:
        return len(VR256)

    @property
    @abc.override
    def max_vector_size(self) -> int:
        return 256 // 8

    @abc.override
    def instruction_set(self) -> Instruction_Set:
        return self._instruction_set

    @abc.override
    def register_set(self) -> Register_Set:
        return self._register_set

    @abc.override
    def asm_dialects(self) -> ty.List[ASM_Dialect]:
        return self._asm_dialects

    @abc.override
    def make_asm_writer(self, dialect: ASM_Dialect, file: ty.IO[str]) -> ASM_Writer:
        return X86_ASM_Writer(dialect, file)

    @abc.override
    def make_ir_builder(self) -> "X86_IR_Builder":
        return X86_IR_Builder(self)

    @abc.override
    def make_register_allocator(self) -> Register_Liveness_Tracker:
        regs = self.register_set()

        return Register_Liveness_Tracker(
            all_registers=regs.all_registers(),
            callee_save_registers=regs.callee_save_registers(),
        )

    @abc.override
    def loop_overhead(self, num_iterations: int) -> Loop_Overhead:
        # there are two overhead instructions per iterations in a our X86 counting
        # loops:
        # ```asm
        #     xorq loop_counter, loop_counter
        #   .align 16
        #   loop:
        #     ... kernel ...
        #     subq loop_counter, $1
        #     jne loop.
        # ```
        # the align also adds a small constant number of instructions,
        # but that is hard to predict without writing our own assembler,
        # so we ignore it.
        #
        # FIXME: it is super ugly that this method knows so much about
        #        the internals of our kernels.

        ## instructions
        insts_init = 1
        insts_per_iteration = 2 * num_iterations
        insts_total = insts_init + insts_per_iteration

        ## muops
        muops_init = 1
        # the subq+jne at the end of the loop are macro-fused
        muops_per_iteration = num_iterations
        muops_total = muops_init + muops_per_iteration

        return Loop_Overhead(insts_total, muops_total)


class X86_ASM_Dialect(ASM_Dialect):
    @abc.abstractmethod
    def _mnemonic(self, inst: "X86_Instruction") -> str:
        ...


class X86_ATT_ASM_Dialect(X86_ASM_Dialect):
    @abc.override
    def _mnemonic(self, inst: "X86_Instruction") -> str:
        return ATT_MNEMONICS[inst.name]


class X86_IR_Builder(IR_Builder):
    def __init__(self, arch):
        self._arch = arch
        self._insts = arch.instruction_set()

    @abc.override
    def get_return_register(self) -> Register:
        return RAX

    @abc.override
    def get_argument_register(self, idx: int) -> Register:
        return {0: ARG_1, 1: ARG_2, 2: ARG_3, 3: ARG_4, 4: ARG_5, 5: ARG_6}[idx]

    @abc.override
    def get_scratch_register(self, idx: int) -> Register:
        return {0: R12, 1: R13, 2: R14, 3: R15, 4: RBP}[idx]

    @abc.override
    def select_memory_base_register(
        self,
        insts: ty.List[Instruction],
        free_regs: ty.Set[Register],
        address_width: int,
    ) -> Register:
        """
        Select a register that can be used as a memory base register for all memory accesses
        in the given list of instructions :insts:.
        """

        if address_width == 32:
            candidates = BASE_REGISTER_32
            candidates = set([EBP, EDI, ESI])
        elif address_width == 64:
            candidates = BASE_REGISTER_64
            candidates = set([RBP, RDI, RSI])
        else:
            raise NotImplementedError(f"TODO: {address_width}-bit addressing")

        candidates = set(candidates) & free_regs

        # # used in sequentialize CPU
        # candidates -= set([AL, AH, AX, EAX, RAX,
        #                    BL, BH, BX, EBX, RBX,
        #                    CL, CH, CX, ECX, RCX,
        #                    DL, DH, DX, EDX, RDX,
        #                    ])
        # do not clobber stack
        candidates -= set([SPL, SP, ESP, RSP])

        for i in insts:
            for op in i.operands:
                if not op.is_def:
                    continue
                if not isinstance(op, Register_Operand):
                    continue
                if len(op.register_class) != 1:
                    continue

                reg = op.register_class[0].widest

                candidates -= set([reg])

        assert candidates, [candidates, sorted(set([r.widest for r in free_regs]))]

        reg = sorted(candidates)[0]

        assert reg.widest != RCX

        return reg.as_width(address_width)

    @abc.override
    def preallocate_benchmark(
        self, alloc: Register_Liveness_Tracker, instructions: ty.Sequence[Instruction]
    ) -> ty.Tuple[object, ty.List[Instruction]]:
        stolen_regs = []
        preallocated_insts = []

        if self._benchmark_contains_scalar_int_div(instructions):
            alloc.take(RDX)
            stolen_regs.append(RDX)

        for i in instructions:
            if i.name.startswith("DIV_") or i.name.startswith("IDIV_"):
                pass

            preallocated_insts.append(i)

        return stolen_regs, preallocated_insts

    @abc.override
    def free_stolen_benchmark_registers(
        self, alloc: Register_Liveness_Tracker, stolen_regs: object
    ):
        for reg in stolen_regs:
            alloc.free(reg)

    def _benchmark_contains_scalar_int_div(self, kernel: ty.Sequence[Instruction]):
        for i in kernel:
            if i.name.startswith("DIV_") or i.name.startswith("IDIV_"):
                return True

    @abc.override
    def emit_loop_prologue(
        self,
        kernel: ty.Sequence[Instruction],
        free_regs: ty.Sequence[Register],
        reg_values: Register,
    ) -> ty.List[Instruction]:
        """`reg_values` is a register containing a pointer to a memory region from
        where vector registers can be initialized"""

        out: ty.List[Instruction] = []

        # Initialize YMMi
        for ymmid, ymm in enumerate(VR256):
            out += self.emit_put_basedisplacement_in_register(
                reg_values, 32 * ymmid, ymm
            )

        out += self.emit_push_to_stack(reg_values)

        return out

    @abc.override
    def emit_benchmark_prologue(
        self,
        kernel: ty.Sequence[Instruction],
        free_regs: ty.Sequence[Register],
    ) -> ty.List[Instruction]:
        scalar_int_div = False
        leave = False

        for i in kernel:
            if i.name.startswith("DIV_GPR") or i.name.startswith("IDIV_GPR"):
                scalar_int_div = True

                if i.get_operand("src").register.widest == RDX:
                    raise ValueError(
                        "can't run benchmark where RDX is source operand of division"
                    )

            if i.name == "LEAVE":
                leave = True

        out = []

        for gpr in filter(lambda r: r in GPR64, free_regs):
            if gpr == RSP:
                continue

            # Somewhere between int32_max+43 and int64_max-42 with non-null first byte
            rand_value_upper = random.randint(2 ** 24 + 42, 2 ** 55 - 42) << 8
            rand_lower_byte = random.randint(1, 0xFF)
            rand_value = rand_value_upper | rand_lower_byte

            out += self.emit_put_const_in_register(rand_value, gpr)

        if leave:
            out += self.emit_put_const_in_register(
                Label("_memory_arena_@GOTPCREL(%rip)"), RBP
            )
            i = self._set_registers(Harness.STORE_REG64_TO_MEM64, "src", RSP)
            i = i.update_operand(
                "dst", lambda op: op.with_base(RBP).with_displacement(0)
            )
            out += [i]

        return out

    @abc.override
    def emit_benchmark_epilogue(
        self,
        kernel: ty.Sequence[Instruction],
        free_regs: ty.Sequence[Register],
    ) -> ty.List[Instruction]:
        leave = False

        for i in kernel:
            if i.name == "LEAVE":
                leave = True

        out = []

        if leave:
            # out += self.emit_put_const_in_register(0, RAX)
            i = self._set_registers(Harness.LOAD_MEM64_TO_REG64, "dst", RSP)
            i = i.update_operand(
                "src",
                lambda op: op.with_base(RIP).with_displacement(
                    Label("_memory_arena_@GOTPCREL")
                ),
            )
            out += [i]

        return out

    @abc.override
    def emit_loop_epilogue(
        self,
        kernel: ty.Sequence[Instruction],
        free_regs: ty.Sequence[Register],
        reg_values: Register,
    ) -> ty.List[Instruction]:
        out = []
        out += self.emit_pop_from_stack(reg_values)
        return out

    def emit_dependency_breaker(self, reg: Register) -> ty.List[Instruction]:
        if reg in GPR8:
            inst = Harness.XOR_GPR8i8_GPR8i8
        if reg in GPR16:
            inst = Harness.XOR_GPR16i16_GPR16i16
        elif reg in GPR32:
            inst = Harness.XOR_GPR32i32_GPR32i32
        elif reg in GPR64:
            inst = Harness.XOR_GPR64i64_GPR64i64
        else:
            return []

        return [self._set_registers(inst, "src", reg, "src-dst", reg)]

    @abc.override
    def emit_sequentialize_cpu(
        self, alloc: Register_Liveness_Tracker
    ) -> ty.List[Instruction]:
        regs = (EAX, EBX, ECX, EDX)
        for reg in regs:
            alloc.take(reg)

        out = self.emit_put_const_in_register(0, RAX) + [Harness.CPUID]

        for reg in regs:
            alloc.free(reg)

        return out

    @abc.override
    def emit_copy(self, src: Register, dst: Register) -> ty.List[Instruction]:
        assert (
            src in GPR64 and dst in GPR64
        ), f"can only do 64-bit GPR for now ({src}, {dst})"

        return [self._set_registers(Harness.MOV_GPR64, "src", src, "dst", dst)]

    @abc.override
    def emit_push_to_stack(self, src: Register) -> ty.List[Instruction]:
        assert src in GPR64, f"can only do 64-bit GPR for now ({src})"

        return [self._set_register(Harness.PUSH_GPR64, "src", src)]

    @abc.override
    def emit_pop_from_stack(self, dst: Register) -> ty.List[Instruction]:
        assert dst in GPR64, f"can only do 64-bit GPR for now {dst}"

        return [self._set_register(Harness.POP_GPR64, "dst", dst)]

    @abc.override
    def emit_branch_if_not_zero(
        self, reg: Register, dst: Label
    ) -> ty.List[Instruction]:
        assert reg in GPR64, f"can only do 64-bit GPR for now ({reg})"

        return [
            self._set_registers(Harness.TEST_GPR64, "src1", reg, "src2", reg),
            self._set_value(Harness.JNE_32, "target", dst),
        ]

    @abc.override
    def emit_branch_if_zero(self, reg: Register, dst: Label) -> ty.List[Instruction]:
        assert reg in GPR64, f"can only do 64-bit GPR for now ({reg})"

        return [
            self._set_registers(Harness.TEST_GPR64, "a", reg, "b", reg),
            self._set_value(Harness.JEQ_32, "target", dst),
        ]

    @abc.override
    def emit_call(self, dst: Label) -> ty.List[Instruction]:
        return [
            # FIXME: the generated operand names are a bit wonky
            self._set_value(Harness.CALL, "dst", dst),
        ]

    @abc.override
    def emit_return(self, reg: Register) -> ty.List[Instruction]:
        assert reg in GPR, f"can only do GPR for now ({reg})"

        out = []
        R64 = reg.as_width(64)
        if R64 != RAX:
            out.append(self.emit_copy(R64, RAX))
        out.append(Harness.RET)
        return out

    @abc.override
    def emit_put_const_in_register(
        self, const: int, reg: Register
    ) -> ty.List[Instruction]:
        assert reg in GPR, f"can only do GPR for now ({reg})"

        assert type(const) in (int, Label)
        assert type(const) != int or 0 <= const <= (2 ** 64) - 1

        reg32 = reg.as_width(32)
        reg64 = reg.as_width(64)

        if const == 0:
            i = self._set_registers(Harness.XOR_GPR32, "src", reg32, "src-dst", reg32)
        elif type(const) is int and const <= 2 ** 32 - 1:
            i = Harness.MOV_IMM32_GPR32
            i = self._set_value(i, "src", const)
            i = self._set_register(i, "dst", reg32)
        else:
            i = Harness.MOV_IMM64_GPR64
            i = self._set_value(i, "src", const)
            i = self._set_register(i, "dst", reg64)
        return [i]

    @abc.override
    def emit_put_basedisplacement_in_register(
        self, base: "Register", displacement: int, reg: "Register"
    ) -> ty.List["Instruction"]:
        assert reg in VR256, f"can only do VR256 (aka ymmX) for now ({reg})"
        i = Harness.VMOVDQA_VR256_MEM64
        i = self._set_registers(i, "dst", reg)
        i = self._set_base_displacement(i, "src", base, displacement)
        return [i]

    @abc.override
    def emit_mul_reg_const(self, reg: Register, const: int) -> ty.List[Instruction]:
        assert reg in GPR64, f"can only do 64-bit GPR for now ({reg})"

        i = Harness.IMUL_IMM_GPR64
        i = self._set_register(i, "src1", reg)
        i = self._set_value(i, "src2", const)
        i = self._set_register(i, "dst", reg)
        return [i]

    @abc.override
    def emit_add_registers(
        self, src_reg: Register, src_dst_reg: Register
    ) -> ty.List[Instruction]:
        return [
            self._set_registers(
                Harness.ADD_GPR64, "src", src_reg, "src-dst", src_dst_reg
            )
        ]

    @abc.override
    def emit_substract_one_from_reg_and_branch_if_not_zero(
        self, loop_counter: Register, dst: "Label"
    ) -> ty.List[Instruction]:
        sub = Harness.SUB_IMM8_GPR64
        sub = self._set_value(sub, "src", 1)
        sub = self._set_register(sub, "src-dst", loop_counter)

        jmp = self._set_value(Harness.JNE_32, "target", dst)

        return [sub, jmp]

    ####

    def _set_registers(self, instr: Instruction, *args) -> Instruction:
        assert len(args) % 2 == 0

        for i in range(0, len(args), 2):
            instr = self._set_register(instr, args[i], args[i + 1])

        return instr

    def _set_register(
        self, instr: Instruction, op_idx_or_name, reg: X86_Register
    ) -> Instruction:
        return instr.update_operand(op_idx_or_name, lambda op: op.with_register(reg))

    def _set_value(self, instr: Instruction, op_idx_or_name, val: int) -> Instruction:
        return instr.update_operand(op_idx_or_name, lambda op: op.with_value(val))

    def _set_base_displacement(
        self,
        instr: Instruction,
        op_idx_or_name,
        base: X86_Register,
        displacement: int,
    ) -> Instruction:
        return instr.update_operand(
            op_idx_or_name,
            lambda op: op.with_base(base).with_displacement(displacement),
        )
