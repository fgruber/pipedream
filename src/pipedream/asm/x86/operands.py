
from pipedream.asm.x86.flags import X86_Flags
from pipedream.asm   import ir
from pipedream.utils import abc

from .registers import *
import copy
import typing as ty

__all__ = [
  'X86_Operand',
  'X86_Register_Operand',
  'X86_Immediate_Operand',
  'X86_Flags_Operand',

  'X86_Memory_Operand',
  'X86_Address_Operand',

  'X86_Base_Displacement_Operand',
  'X86_Base_Displacement_Memory_Operand',
  'X86_Base_Displacement_Address_Operand',

  'Imm8',
  'Imm16',
  'Imm32',
  'Imm64',
  'ImmU8',
  'ImmU16',
  'ImmU32',
  'ImmU64',
  'Scale_Imm',
  'Shift_Imm',
]


class X86_Operand(ir.Operand):
  def __init__(self, name: str, visibility: ir.Operand_Visibility):
    self._name = name
    self._visibility = visibility

  @property
  @abc.override
  def name(self) -> str:
    return self._name

  @property
  @abc.override
  def visibility(self) -> str:
    return self._visibility


################################################################################
##### REGISTERS

class X86_Register_Operand(X86_Operand, ir.Register_Operand):
  def __init__(self, name: str, visibility: ir.Operand_Visibility, use_def: ir.Use_Def,
               reg_class: ir.Register_Class, reg: X86_Register):
    super().__init__(name, visibility)
    self._use_def   = use_def
    self._reg_class = reg_class
    self._reg       = reg

  @property
  @abc.override
  def short_name(self) -> str:
    if self._reg:
      return self._reg.name
    else:
      return self._reg_class.name

  @property
  @abc.override
  def register_class(self) -> ir.Register_Class:
    return self._reg_class

  @property
  @abc.override
  def register(self) -> ty.Optional[X86_Register]:
    return self._reg

  @abc.override
  def with_register(self, reg: ir.Register) -> 'X86_Register_Operand':
    assert reg is not None

    if reg not in self.register_class:
      raise TypeError(f'Register {reg.name} is not a member of {self.register_class.name}')

    return X86_Register_Operand(self.name, self.visibility, self.use_def, self.register_class, reg)

  @property
  @abc.override
  def use_def(self) -> ir.Use_Def:
    return self._use_def


class X86_Flags_Operand(X86_Operand, ir.Flags_Operand):
  def __init__(self, name: str, visibility: ir.Operand_Visibility,
               reg: X86_Register, flags_read: X86_Flags, flags_written: X86_Flags):
    assert isinstance(flags_read, X86_Flags), flags_read
    assert isinstance(flags_written, X86_Flags), flags_written

    super().__init__(name, visibility)
    self._reg = reg
    self._flags_read = flags_read
    self._flags_written = flags_written

  @property
  @abc.override
  def is_virtual(self):
    return False

  @property
  @abc.override
  def flags_read(self) -> X86_Flags:
    return self._flags_read

  @property
  @abc.override
  def flags_written(self) -> X86_Flags:
    return self._flags_written

  @property
  @abc.override
  def register(self) -> X86_Register:
    return self._reg

  @property
  @abc.override
  def short_name(self) -> str:
    return self._reg.name

  @property
  @abc.override
  def use_def(self) -> ir.Use_Def:
    if self._flags_read and self._flags_written:
      return ir.Use_Def.USE_DEF
    if self._flags_read:
      return ir.Use_Def.USE
    if self._flags_written:
      return ir.Use_Def.DEF

    ## FIXME: should never happen
    # assert False, [self.name, self._reg, self._flags_read, self._flags_written]
    return ir.Use_Def.USE

  def __repr__(self):
    txt = self.name + ":"

    rw    = self.flags_read | self.flags_written
    flags = [f for f in X86_Flags if f & rw]

    if flags:
      for f in flags:
        txt += f.name

        if f & self.flags_read:
          txt += '?'
        if f & self.flags_written:
          txt += '!'
    else:
      txt += self._reg.name

    return txt


################################################################################
##### IMMEDIATES

class X86_Immediate_Operand(X86_Operand, ir.Immediate_Operand):
  def __init__(self, name: str, visibility: ir.Operand_Visibility, value: int = None):
    super().__init__(name, visibility)

    assert value is None or isinstance(value, (int, ir.Label)), f'want int or Label, have {value!r}'

    self._value = value

  @property
  @abc.override
  def short_name(self) -> str:
    return type(self).__name__.upper()

  @property
  @abc.override
  def value(self) -> ty.Union[int, float, None]:
    return self._value

  @abc.override
  def with_value(self, value):
    clss = type(self)

    if not self._is_valid_value(value):
      raise TypeError(f'{value} is not a valid value for immediates of type {clss.__name__}')

    return clss(self.name, self.visibility, value)

  @classmethod
  @abc.abstractmethod
  def _is_valid_value(clss, value) -> bool:
    """
      Check if value is a valid value for immediates of this type.
    """

  @classmethod
  @abc.abstractmethod
  def _arbitrary(clss, random) -> ty.Union[int, float]:
    """
      Generate a random int/float that can fit in an immediate of this type.
    """


def _signed_min_max(num_bits: int):
  min = - 2 ** (num_bits - 1)
  max = + 2 ** (num_bits - 1) - 1
  return min, max


def _unsigned_min_max(num_bits: int):
  min = 0
  max = + 2 ** (num_bits - 1)
  return min, max


class Imm8(X86_Immediate_Operand):
  """
    class of 8 bit signed immediates
  """

  _NUM_BITS = 8
  MIN, MAX  = _signed_min_max(_NUM_BITS)

  @property
  @abc.override
  def num_bits(self):
    return self._NUM_BITS

  @classmethod
  @abc.override
  def _is_valid_value(clss, value):
    return type(value) is int and clss.MIN <= value <= clss.MAX

  @classmethod
  @abc.override
  def _arbitrary(clss, random):
    return clss.MIN
    # return random.randint(clss.MIN, clss.MAX)


class ImmU8(X86_Immediate_Operand):
  """
    class of 8 bit unsigned immediates
  """

  _NUM_BITS = 8
  MIN, MAX  = _unsigned_min_max(_NUM_BITS)

  @property
  @abc.override
  def num_bits(self):
    return self._NUM_BITS

  @classmethod
  @abc.override
  def _is_valid_value(clss, value):
    return type(value) is int and clss.MIN <= value <= clss.MAX

  @classmethod
  @abc.override
  def _arbitrary(clss, random):
    return clss.MAX
    # return random.randint(clss.MIN, clss.MAX)


class Imm16(X86_Immediate_Operand):
  """
    class of 16 bit signed immediates
  """

  _NUM_BITS = 16
  MIN, MAX  = _signed_min_max(_NUM_BITS)

  @property
  @abc.override
  def num_bits(self):
    return self._NUM_BITS

  @classmethod
  @abc.override
  def _is_valid_value(clss, value):
    if type(value) is ir.Label:
      return True

    if type(value) is int:
      return clss.MIN <= value <= clss.MAX

    return False

  @classmethod
  @abc.override
  def _arbitrary(clss, random):
    return clss.MIN
    # return random.randint(clss.MIN, clss.MAX)


class ImmU16(X86_Immediate_Operand):
  """
    class of 16 bit unsigned immediates
  """

  _NUM_BITS = 16
  MIN, MAX  = _unsigned_min_max(_NUM_BITS)

  @property
  @abc.override
  def num_bits(self):
    return self._NUM_BITS

  @classmethod
  @abc.override
  def _is_valid_value(clss, value):
    if type(value) is ir.Label:
      return True

    if type(value) is int:
      return clss.MIN <= value <= clss.MAX

    return False

  @classmethod
  @abc.override
  def _arbitrary(clss, random):
    return clss.MAX
    # return random.randint(clss.MIN, clss.MAX)


class Imm32(X86_Immediate_Operand):
  """
    class of 32 bit signed immediates
  """

  _NUM_BITS = 32
  MIN, MAX  = _signed_min_max(_NUM_BITS)

  @property
  @abc.override
  def num_bits(self):
    return self._NUM_BITS

  @classmethod
  @abc.override
  def _is_valid_value(clss, value):
    if type(value) is ir.Label:
      return True

    if type(value) is int:
      return clss.MIN <= value <= clss.MAX

    return False

  @classmethod
  @abc.override
  def _arbitrary(clss, random):
    return clss.MIN
    # return random.randint(clss.MIN, clss.MAX)


class ImmU32(X86_Immediate_Operand):
  """
    class of 32 bit unsigned immediates
  """

  _NUM_BITS = 32
  MIN, MAX  = _unsigned_min_max(_NUM_BITS)

  @property
  @abc.override
  def num_bits(self):
    return self._NUM_BITS

  @classmethod
  @abc.override
  def _is_valid_value(clss, value):
    if type(value) is ir.Label:
      return True

    if type(value) is int:
      return clss.MIN <= value <= clss.MAX

    return False

  @classmethod
  @abc.override
  def _arbitrary(clss, random):
    return clss.MAX
    # return random.randint(clss.MIN, clss.MAX)


class Imm64(X86_Immediate_Operand):
  """
    class of 64 bit signed immediates
  """

  _NUM_BITS = 64
  MIN, MAX  = _signed_min_max(_NUM_BITS)

  @property
  @abc.override
  def num_bits(self):
    return self._NUM_BITS

  @classmethod
  @abc.override
  def _is_valid_value(clss, value):
    if type(value) is ir.Label:
      return True

    if type(value) is int:
      return clss.MIN <= value <= clss.MAX

    return False

  @classmethod
  @abc.override
  def _arbitrary(clss, random):
    return clss.MIN
    # return random.randint(clss.MIN, clss.MAX)


class ImmU64(X86_Immediate_Operand):
  """
    class of 64 bit unsigned immediates
  """

  _NUM_BITS = 64
  MIN, MAX  = _unsigned_min_max(_NUM_BITS)

  @property
  @abc.override
  def num_bits(self):
    return self._NUM_BITS

  @classmethod
  @abc.override
  def _is_valid_value(clss, value):
    if type(value) is ir.Label:
      return True

    if type(value) is int:
      return clss.MIN <= value <= clss.MAX

    return False

  @classmethod
  @abc.override
  def _arbitrary(clss, random):
    return clss.MAX
    # return random.randint(clss.MIN, clss.MAX)


class Scale_Imm(X86_Immediate_Operand):
  """
    Immediate for `scale` operand of a memory access in X86.
  """

  @property
  @abc.override
  def short_name(self) -> str:
    return 'SCALE'

  @classmethod
  @abc.override
  def _is_valid_value(clss, value):
    return type(value) is int and value in (1, 2, 4, 8)

  @classmethod
  @abc.override
  def _arbitrary(clss, random):
    return 1
    # return 2 ** random.randint(0, 3)


class Shift_Imm(X86_Immediate_Operand):
  """
    Immediate operand for a shit instruction.
  """

  MIN =  0
  MAX = 31

  @property
  @abc.override
  def short_name(self) -> str:
    return 'SHIFT'

  @classmethod
  @abc.override
  def _is_valid_value(clss, value):
    return type(value) is int and clss.MIN <= value <= clss.MAX

  @classmethod
  @abc.override
  def _arbitrary(clss, random):
    return 3
    # return random.randint(clss.MIN, clss.MAX)


################################################################################
##### MEMORY

class X86_Memory_Or_Address_Operand(X86_Operand, ir.Composite_Operand):
  """
    Mixin for defining memory & address operands
  """

  def __init__(self,
               name: str,
               visibility: ir.Operand_Visibility,
               address_width: int,
               ):
    super().__init__(name, visibility)

    assert type(address_width) is int
    assert address_width in (16, 32, 64)

    # how many bits are in address (16/32/64)
    self._address_width = address_width

  @property
  def address_width(self) -> int:
    return self._address_width

  @property
  @abc.override
  def is_virtual(self) -> bool:
    return any(op.is_virtual for op in self.sub_operands)

  @abc.override
  def update_sub_operand(self, idx_or_name: ty.Union[int, str],
                         fn: ty.Callable[['Operand'], 'Operand']):
    if type(idx_or_name) is int:
      name = self.get_operand_name(idx_or_name)
    else:
      name = idx_or_name

    new = copy.copy(self)
    sub = fn(getattr(new, name))
    assert isinstance(sub, ir.Operand)
    setattr(new, name, sub)
    return new


class X86_Memory_Operand(X86_Memory_Or_Address_Operand, ir.Memory_Operand):
  def __init__(self,
               name: str,
               visibility: ir.Operand_Visibility,
               use_def: ir.Use_Def,
               address_width: int, memory_width: int,
               ):
    super().__init__(name, visibility, address_width)

    assert type(use_def) is ir.Use_Def
    assert type(memory_width) is int

    # how many bits are loaded/stored
    self._memory_width = memory_width

    self._use_def = use_def

  @property
  @abc.override
  def memory_width(self) -> int:
    return self._memory_width

  @property
  @abc.override
  def use_def(self) -> ir.Use_Def:
    return self._use_def


class X86_Address_Operand(X86_Memory_Or_Address_Operand, ir.Address_Operand):
  @property
  @abc.override
  def use_def(self) -> ir.Use_Def:
    return ir.Use_Def.USE


class X86_Base_Displacement_Operand(ir.Base_Displacement_Operand):
  """
    Mixin for defining base/displacement operands
  """

  def __init__(self,
               base: ty.Optional[X86_Register_Operand],
               displacement: ty.Optional[Imm32],
               ):
    assert isinstance(displacement, X86_Immediate_Operand)
    assert isinstance(base, X86_Register_Operand), base

    self._displacement = displacement
    self._base = base

  @property
  def base(self) -> ir.Register_Operand:
    return self._base

  @property
  def displacement(self) -> ir.Immediate_Operand:
    return self._displacement

  def with_base(self, base_reg: ir.Register) -> 'X86_Base_Displacement_Operand':
    new = copy.copy(self)
    new._base = new._base.with_register(base_reg)
    return new

  def with_displacement(self, disp: int) -> 'X86_Base_Displacement_Operand':
    new = copy.copy(self)
    new._displacement = new._displacement.with_value(disp)
    return new

  @abc.abstractproperty
  def _short_short_name(self) -> str:
    pass

  @property
  @abc.override
  def short_name(self) -> str:
    return f'{self._short_short_name}BD{self.address_width}_{self.memory_width}'

  @property
  @abc.override
  def sub_operands(self):
    yield self.base
    yield self.displacement

  def get_operand_name(self, idx: int) -> str:
    return {
      0: 'base',
      1: 'displacement',
    }[idx]

  def __repr__(self):
    tmp = [
      self.name, ':',
      self._short_short_name, 'BD',
      str(self.address_width), '/',
    ]
    if hasattr(self, 'memory_width'):
      tmp += [
        str(self.memory_width), '/',
      ]
    tmp += [
      '(', repr(self.base), ', ', repr(self.displacement), ')'
    ]

    return ''.join(tmp)


class X86_Base_Displacement_Memory_Operand(X86_Base_Displacement_Operand, X86_Memory_Operand,
                                           ir.Base_Displacement_Memory_Operand):
  def __init__(self, name: str, visibility: ir.Operand_Visibility,
               use_def: ir.Use_Def, address_width: int, memory_width: int,
               base: ty.Optional[X86_Register_Operand],
               displacement: ty.Optional[Imm32],
               ):
    X86_Memory_Operand.__init__(self, name, visibility, use_def, address_width, memory_width)
    X86_Base_Displacement_Operand.__init__(self, base, displacement)

  @property
  @abc.override
  def _short_short_name(self) -> str:
    return 'mem'


class X86_Base_Displacement_Address_Operand(X86_Base_Displacement_Operand, X86_Address_Operand,
                                            ir.Base_Displacement_Address_Operand):
  def __init__(self, name: str, visibility: ir.Operand_Visibility,
               address_width: int,
               base: ty.Optional[X86_Register_Operand],
               displacement: ty.Optional[Imm32],
               ):
    X86_Address_Operand.__init__(self, name, visibility, address_width)
    X86_Base_Displacement_Operand.__init__(self, base, displacement)

  @property
  @abc.override
  def _short_short_name(self) -> str:
    return 'addr'
