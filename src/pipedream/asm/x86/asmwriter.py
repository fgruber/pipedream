from pipedream.utils import abc
from pipedream.asm.ir import *
from pipedream.asm.asmwriter import *
from pipedream.asm.x86.registers import *
from pipedream.asm.x86.operands import *

import functools
import math
import typing as ty


__all__ = [
    "X86_ASM_Writer",
]


class X86_ASM_Writer(ASM_Writer):
    def __init__(self, dialect: "X86_ASM_Dialect", file: ty.IO[str]):
        super().__init__(file)
        self.dialect = dialect

    @abc.override
    def begin_file(self, name):
        self.print(1, '.file 1 "' + name + '"')
        self.print(1, ".text")

    @abc.override
    def end_file(self, name):
        self.print(1, '.ident "pipe-dream"')

    @abc.override
    def begin_function(self, function_name: str):
        self.print(1, ".globl " + function_name)
        self.print(1, ".type  " + function_name + ", @function")
        self.align()
        self.print(0, function_name + ":")
        self.print(1, ".cfi_startproc")

    @abc.override
    def end_function(self, function_name: str):
        self.print(1, ".cfi_endproc")
        self.print(1, ".size " + function_name + ", .-" + function_name)

    @abc.override
    def emit_label(self, label):
        self.print(0, label.name + ":")

    @abc.override
    def align(self):
        self.print(1, ".align 16, 0x90")

    @abc.override
    def insts(self, insts: ty.Sequence[Instruction]):
        for inst in insts:
            assert isinstance(inst, Instruction)
            assert not any(o.is_virtual for o in inst.operands), [
                inst,
                [o for o in inst.operands if o.is_virtual],
            ]

            try:
                mnem = self.dialect._mnemonic(inst)
                ops = reversed(
                    [
                        op
                        for op in inst.operands
                        if op.visibility is not Operand_Visibility.SUPPRESSED
                    ]
                )

                if ops:
                    self.print(
                        1, mnem.ljust(12), ", ".join(self.emit_operand(o) for o in ops)
                    )
                else:
                    self.print(1, mnem)

            except AssertionError:
                print("#", inst)
                raise

    @abc.override
    def global_byte_array(self, name: str, size: int, alignment: int):
        ## this is just copy/adapted from ASM emitted by GCC

        assert type(name) is str
        assert type(size) is int
        assert type(alignment) is int

        assert name and name.isprintable()
        assert size >= 0
        assert alignment >= 0
        assert math.log2(alignment).is_integer()

        ## assume we are in text section
        # self.print(1, '.text')
        ## switch to BSS & emit array
        self.print(1, ".bss")
        self.print(1, ".global", name)
        self.print(1, ".align", alignment)
        self.print(1, ".type", name + ",", "@object")
        self.print(1, ".size", name + ",", size)
        self.print(0, name + ":")
        self.print(1, ".zero", size)
        ## switch back to text section
        self.print(1, ".text")
        self.print(0, "")

    @abc.override
    def comment(self, *args):
        self.print(1, "#", *args)

    @abc.override
    def newline(self):
        self.print(0)

    @abc.override
    def emit_operand(self, op) -> str:
        return renderX86_arg(op)


@functools.singledispatch
def renderX86_arg(arg):
    raise ValueError(
        "Cannot render argument " + repr(arg) + " of type " + type(arg).__name__
    )


@renderX86_arg.register(Label)
def _(arg):
    return arg.name


@renderX86_arg.register(X86_Register_Operand)
def _(arg):
    return arg.register.att_asm_name


@renderX86_arg.register(X86_Immediate_Operand)
def _(arg):
    if type(arg.value) is Label:
        return arg.value.name
    else:
        return "$" + str(arg.value)


@renderX86_arg.register(str)
def _(arg):
    # TODO: introduce symbol type
    return arg


@renderX86_arg.register(X86_Base_Displacement_Operand)
def _(arg):
    return renderX86_mem_arg(arg.displacement) + "(" + renderX86_mem_arg(arg.base) + ")"


# @renderX86_arg.register(X86_Memory_Operand)
# def _(arg):
#   mode = arg.address_mode()

#   assert mode in X86_Memory_Operand._VALID_ADDRESS_MODES

#   if mode == 'B':
#     return '(' + renderX86_mem_arg(arg.base) + ')'
#   if mode == 'D':
#     return '(' + renderX86_mem_arg(arg.displacement) + ')'
#   if mode == 'BD':
#     return renderX86_mem_arg(arg.displacement) + '(' + renderX86_mem_arg(arg.base) + ')'
#   if mode == 'BIS':
#     return ''.join((
#       '(',
#       renderX86_mem_arg(arg.base),
#       ',',
#       renderX86_mem_arg(arg.index),
#       ',',
#       renderX86_mem_arg(arg.scale),
#       ')',
#     ))
#   if mode == 'ISD':
#     return ''.join((
#       renderX86_mem_arg(arg.displacement),
#       '(',
#       ',',
#       renderX86_mem_arg(arg.index),
#       ',',
#       renderX86_mem_arg(arg.scale),
#       ')',
#     ))
#   if mode == 'BISD':
#     return ''.join((
#       renderX86_mem_arg(arg.displacement),
#       '(',
#       renderX86_mem_arg(arg.base),
#       ',',
#       renderX86_mem_arg(arg.index),
#       ',',
#       renderX86_mem_arg(arg.scale),
#       ')',
#     ))

#   raise ValueError('invalid memory operand', arg)


@functools.singledispatch
def renderX86_mem_arg(arg):
    raise ValueError(
        "Cannot render memory argument " + repr(arg) + " of type " + type(arg).__name__
    )


@renderX86_mem_arg.register(X86_Immediate_Operand)
def _(arg) -> str:
    assert arg.value is not None

    if arg.value == 0:
        return ""

    if type(arg.value) is Label:
        return arg.value.name

    assert type(arg.value) is int
    return str(arg.value)


@renderX86_mem_arg.register(X86_Register_Operand)
def _(arg) -> str:
    assert arg.register is not None
    return arg.register.att_asm_name
