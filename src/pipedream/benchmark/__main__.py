
__all__ = ['main']

import sys

from pipedream.benchmark import throughput_benchmark
from pipedream.benchmark import latency_benchmark
from pipedream.benchmark import port_usage_benchmark
from pipedream.benchmark import show_stats


def main(argv):
  if not argv:
    argv = ['run']

  cmd  = argv[0]
  argv = argv[1:]

  if cmd in ['throughput-benchmark', 'throughput']:
    return throughput_benchmark.main(argv)

  if cmd in ['latency-benchmark', 'latency']:
    return latency_benchmark.main(argv)

  if cmd in ['port-usage-benchmark', 'port']:
    return port_usage_benchmark.main(argv)

  if cmd in ['stats', 'show-stats']:
    return show_stats.main(argv)

  print(
    'error: unknown command', repr(cmd), 'try "throughput", "latency", "port", or "stats"',
    file=sys.stderr,
  )


if __name__ == '__main__':
  try:
    main(sys.argv[1:])
  except KeyboardInterrupt:
    exit(1)
  except BrokenPipeError:
    exit(1)
