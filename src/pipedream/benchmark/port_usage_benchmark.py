"""
  Generate and run benchmarks to measure execution port usage
"""

import pipedream.benchmark.common as common
from pipedream.benchmark.types import *


def main(argv=None):
  common.main(
    argv=argv,
    make_perf_counters=common.Perf_Counter_Spec.make_port_counters,
    benchmark_kind=Benchmark_Kind.THROUGHPUT,
  )


if __name__ == '__main__':
  try:
    main()
  except (KeyboardInterrupt, BrokenPipeError):
    exit(1)
