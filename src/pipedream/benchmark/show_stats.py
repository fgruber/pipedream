"""
  parses yaml output from running benchmarks and pretty-prints results.
"""

import argparse
import fractions
import numbers
import sys

from pipedream.benchmark.types import *
from pipedream.utils import terminal


class Pretty_Printer:
  C = terminal.Colors

  PARAMETER_COLOR = C.yellow
  RESULT_COLOR    = C.cyan | C.yellow

  def write(self, indent_lvl, *args, end='\n'):
    print(' ' * indent_lvl, *args, sep='', end=end, flush=True)

  def header1(self, *name):
    self.write(0, (self.C.bold | self.C.underline)(*name))

  def header2(self, *name):
    self.write(2, (self.C.bold | self.C.yellow)(*name))

  def header3(self, *name):
    self.write(4, (self.C.yellow)(*name))

  def one_column(self, label, col, color = C.blue, ljust=10, indent: int = 2):
    self.write(2 + indent, color(str(label), ljust=ljust), col)

  def two_columns(self, label1, col1, label2, col2, color = C.blue, ljust=10, indent: int = 2):
    lvl = 2 + indent

    if terminal.get_terminal_size().columns <= 80:
      self.write(lvl, color(label1 + ':', ljust=ljust), col1)
      self.write(lvl, color(label2 + ':', ljust=ljust), col2)
    else:
      self.write(
        lvl,
        (color(label1 + ':', ljust=ljust) + col1).ljust(40),
        color(label2 + ':', ljust=ljust) + col2,
      )

  def write_value(self, name, param, color = C.yellow):
    name  = name.replace('_', '-')

    if isinstance(param, enum.Enum):
      param = param.name.rjust(10)
    elif isinstance(param, numbers.Number):
      param = f'{param:10_}'
    elif isinstance(param, datetime.timedelta):
      param = ('%.5fs' % param.total_seconds()).rjust(10)
    elif isinstance(param, list) and all(isinstance(i, ir.Instruction) for i in param):
      param = ' '.join(i.name for i in param)

    self.write(2, color(name + ':', ljust=20), param)

  def write_parameter_one_column(self, name, val):
    self.one_column(
      name, val,
      color=self.PARAMETER_COLOR,
      indent=0,
      ljust=20,
    )

  def write_parameter_two_columns(self, name1, val1, name2, val2):
    self.two_columns(
      name1, str(val1).ljust(15),
      name2, str(val2).ljust(15),
      color=self.PARAMETER_COLOR,
      indent=0,
      ljust=20,
    )

  def round_result(self, value: float) -> str:
    if math.isnan(value):
      return str(value)

    value = fractions.Fraction(value)

    rounded = value
    found = False

    for i in range(100, 5, -1):
      rounded = value.limit_denominator(i)

      if rounded.numerator == 1:
        found = True
        break

    if not found:
      fracs = [value.limit_denominator(i) for i in range(5, 1, -1)]

      rounded = min(fracs, key=lambda f: abs(f - value))

    if round(value, 2) != round(value, 1):
      precise = ' (%.3f)' % value
    else:
      precise = ''

    return str(rounded).rjust(10) + precise

  def write_result(self, name: str, value: float):
    self.write(2, self.RESULT_COLOR(name + ':', ljust=20), self.round_result(value))

  def write_statistic(self, name, s: Statistics):
    self.header2(name)

    self.two_columns(
      "MEAN",   f'{s.mean:15_.3f}',
      "MEDIAN", f'{s.p50:15_.3f}',
    )

    def good_bad(val, div, threshold):
      if div == 0:
        return ''

      pct = (val / div)

      if pct < threshold:
        style = self.C.green
      else:
        style = self.C.red

      return style(f'({pct * 100:_.3f}%)'.rjust(12))

    def two_cols(lbl1, val1, txt1, lbl2, val2, txt2):
      if not math.isnan(val1) and not math.isnan(val2):
        self.two_columns(lbl1, txt1, lbl2, txt2)
      elif not math.isnan(val1):
        self.one_column(lbl1, txt1)
      elif not math.isnan(val2):
        self.one_column(lbl2, txt2)

    two_cols(
      "STDDEV", s.stddev, f'{s.stddev:15_.3f} ' + good_bad(s.stddev, s.mean, 0.1),
      'MAD',    s.MAD,    f'{s.MAD:15_.3f}'     + good_bad(s.MAD, s.mean, 0.05),
    )

    if s.percentiles:
      two_cols(
        "P10", s.p10, f'{s.p10:15_.3f}',
        'P25', s.p25, f'{s.p25:15_.3f}',
      )

      two_cols(
        "P75", s.p75, f'{s.p75:15_.3f}',
        'P90', s.p90, f'{s.p90:15_.3f}',
      )
      two_cols(
        "MIN", s.min, f'{s.min:15_.3f}',
        'MAX', s.max, f'{s.max:15_.3f}',
      )
      two_cols(
        "MAX-MIN", s.max - s.min, f'{s.max-s.min:15_.3f}',
        "IQR",     s.IQR,         f'{s.IQR :15_.3f}',
      )

      if not math.isnan(s.p50) and not math.isnan(s.min) and not math.isnan(s.max):
        self.write(6, self.C.blue("BOX-PLOT", ljust=10), s.whisker_plot(101))

    if s.histogram:
      self.write(6, self.C.blue('HISTOGRAM', ljust=10), s.histogram_plot(102))

    if not math.isnan(s.num_samples):
      self.write(4, self.C.blue("#SAMPLES", ljust=10), f'{s.num_samples:15_.0f} ')

  def pretty_print_benchmark_run(self, run: Benchmark_Run):
    print()
    benchmark = run.benchmark
    self.header1(benchmark.name, ':')

    self.write_parameter_one_column('timestamp', run.timestamp)
    self.write_parameter_one_column('runtime',   run.runtime)

    instructions = Benchmark_Spec.name_from_instructions(benchmark.instructions)
    if instructions != benchmark.name:
      self.write_parameter_one_column('instructions', instructions)

    self.write_parameter_one_column('kind', benchmark.kind.name)
    self.write_parameter_two_columns(
      'unroll-factor',     '%10d' % benchmark.unroll_factor,
      'kernel-iterations', '%10d' % benchmark.kernel_iterations,
    )
    # self.write_parameter_two_columns(
    #   'IPC (max)', self.round_result(run.clean_IPC.max),
    #   'MPC (max)', self.round_result(run.clean_MPC.max),
    # )
    # self.write_parameter_two_columns(
    #   'IPC (p90)', self.round_result(run.clean_IPC.p90),
    #   'MPC (p90)', self.round_result(run.clean_MPC.p90),
    # )

    i  = run.instructions.max  - benchmark.loop_overhead.instructions
    fm = run.fused_muops.max   - benchmark.loop_overhead.muops
    um = run.unfused_muops.max - benchmark.loop_overhead.muops

    def round_nan(num):
      if math.isnan(num):
        return num
      else:
        return round(num)

    self.write_parameter_two_columns(
      'fused muops',   '%10s' % round_nan(fm / i),
      'unfused muops', '%10s' % round_nan(um / i),
    )

    self.write_statistic('clean-IPC',     run.clean_IPC)
    self.write_statistic('clean-fMPC',    run.clean_fMPC)
    self.write_statistic('clean-uMPC',    run.clean_uMPC)
    self.write_statistic('IPC',           run.IPC)
    self.write_statistic('fMPC',          run.fMPC)
    self.write_statistic('uMPC',          run.uMPC)
    self.write_statistic('cycles',        run.cycles)
    self.write_statistic('instructions',  run.instructions)
    self.write_statistic('fused muops',   run.fused_muops)
    self.write_statistic('unfused muops', run.unfused_muops)

    for m in run.event_sets:
      for port, stat in m.port_muops.items():
        # ignore ports that take <.1% of the instructions in the bench
        if stat.mean < (run.unfused_muops.mean / 1000):
          continue

        self.write_statistic('port-' + str(port), stat)

      for name, stat in m.misc.items():
        self.write_statistic(name, stat)

    print()


def main(argv):
  # arch  = ir.Architecture.for_name('x86')
  # insts = arch.instruction_set()

  # regs = {}

  # for inst in insts:
  #   for op in inst.operands:
  #     if not isinstance(op, ir.Register_Operand):
  #       continue

  #     if not op.is_def:
  #       continue

  #     if len(op.register_class) != 1:
  #       continue

  #     regs.setdefault(op.register_class[0], set()).add(inst)

  # for r in sorted(regs, key=lambda r: r.name):
  #   print(f'{r.name:20}', *sorted([i.name for i in regs[r]]))

  # exit(42)

  parser = argparse.ArgumentParser()

  parser.add_argument(
    'FILE',
    default=['/dev/stdin'],
    nargs='*',
  )
  mut = parser.add_mutually_exclusive_group()
  mut.add_argument(
    '--yaml',
    dest='format', action='store_const', const='yaml',
    default='yaml',
    help='read benchmark data in YAML format (default)'
  )
  mut.add_argument(
    '--json',
    dest='format', action='store_const', const='json',
    help='read benchmark data in JSON format'
  )
  parser.add_argument('--short', action='store_true', default=False)

  args = parser.parse_args(argv)

  p = Pretty_Printer()

  if args.format == 'yaml':
    for input_file in args.FILE:
      with open(input_file) as fd:
        try:
          for run in yaml.load_all(
            Benchmark_Run.yaml_serializer(),
            fd,
          ):
            p.pretty_print_benchmark_run(run)
        except yaml.YAMLError as e:
          print(Pretty_Printer.C.red('YAML syntax error:'), input_file + ':', str(e))

  elif args.format == 'json':
    for input_file in args.FILE:
      with open(input_file) as fd:
        try:
          for lineno, line in enumerate(fd, 1):
            run = Benchmark_Run.from_json(line)

            if args.short:
              # if len(run.kernel) > 1:
              #   continue

              ports = run.ports_used(0.001)

              # if not math.isinf(run.mpc.mean) and round(run.mpc.mean) == len(ports):
              #   continue

              try:
                num_fused_muops = round(run.fused_muops.mean / run.instructions.mean)
              except ZeroDivisionError:
                num_fused_muops = math.nan

              try:
                num_unfused_muops = round(run.unfused_muops.mean / run.instructions.mean)
              except ZeroDivisionError:
                num_unfused_muops = math.nan

              print(
                run.name.ljust(100),
                f'IPC={round(run.clean_IPC.mean,1)}~{round(run.clean_IPC.stddev,1)}',
                f'fMPC={round(run.clean_fMPC.mean,1)}~{round(run.clean_fMPC.stddev,1)}',
                f'uMPC={round(run.clean_uMPC.mean,1)}~{round(run.clean_uMPC.stddev,1)}',
                '-',
                '%3s FUSED MUOPS' % num_fused_muops,
                '%3s UNFUSED MUOPS' % num_unfused_muops,
                '-',
                ' '.join(f'P{p}' for p in sorted(ports)),
              )
            else:
              p.pretty_print_benchmark_run(run)
        except json.JSONDecodeError as e:
          print(Pretty_Printer.C.red('JSON syntax error:'), input_file + f':{lineno}:', e)


if __name__ == '__main__':
  try:
    main(sys.argv[1:])
  except KeyboardInterrupt:
    exit(1)
  except BrokenPipeError:
    exit(1)
