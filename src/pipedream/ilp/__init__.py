#!/usr/bin/env python3

from __future__ import annotations

import abc
import collections
import dataclasses
import datetime
import itertools
import numbers
import operator
import subprocess
import inspect
import sys
import io
import typing as ty

import pygraphviz
import numpy
import gurobipy

from pipedream.benchmark.types import Benchmark_Spec
from pipedream.ilp.types import *
from pipedream.utils.statistics import Statistics
from pipedream.utils.terminal import Info_Line

GRB = gurobipy.GRB

Gurobi_Value = ty.Union[bool, int, float]


class ILP_Error(Exception):
  pass


class CPU_Model:
  def __init__(self):
    self._insts     = ()
    self._muops     = ()
    self._ports     = ()
    self._throttles = ()

    self._i2m = set()
    self._m2p = set()
    self._i2t = set()

  def add_inst(self, inst):
    assert type(inst) is Inst
    if inst not in self._insts:
      self._insts += (inst,)

  def add_muop(self, muop):
    assert type(muop) is Muop
    if muop not in self._muops:
      self._muops += (muop,)

  def add_port(self, port):
    assert type(port) is Port
    if port not in self._ports:
      self._ports += (port,)

  def add_throttle(self, throttle):
    assert type(throttle) is Port
    if throttle not in self._throttles:
      self._throttles += (throttle,)

  def add_im_edge(self, inst, muop):
    self.add_inst(inst)
    self.add_muop(muop)
    self._i2m.add((inst, muop))

  def add_mp_edge(self, muop, port):
    self.add_muop(muop)
    self.add_port(port)
    self._m2p.add((muop, port))

  def add_it_edge(self, inst, throttle):
    self.add_inst(inst)
    self.add_throttle(throttle)
    self._i2t.add((inst, throttle))

  def insts(self) -> ty.Iterable[Inst]:
    return iter(self._insts)

  def muops(self) -> ty.Iterable[Muop]:
    return iter(self._muops)

  def ports(self) -> ty.Iterable[Port]:
    return iter(self._ports)

  def throttles(self) -> ty.Iterable[Port]:
    return iter(self._throttles)

  def delta_im(self, inst, muop) -> bool:
    """
      Is there an edge between a given inst & muop?
    """
    return (inst, muop) in self._i2m

  def delta_mp(self, muop, port) -> bool:
    """
      Is there an edge between a given muop & port?
    """
    return (muop, port) in self._m2p

  def delta_it(self, inst, throttle) -> bool:
    """
      Is there an edge between a given inst & throttle-port?
    """
    return (inst, throttle) in self._i2t

  def muops_for_inst(self, inst: Inst) -> ty.FrozenSet[Muop]:
    return frozenset([m for m in self.muops() if self.delta_im(inst, m)])

  def ports_for_muop(self, muop: Muop) -> ty.FrozenSet[Port]:
    return frozenset([p for p in self.ports() if self.delta_mp(muop, p)])

  def merge_muops(self, muops = None) -> ty.List[ty.FrozenSet[Port]]:
    if muops is None:
      muops = self.muops()

    port_sets = []

    for M in muops:
      ports = set()

      for P in self.ports():
        if self.delta_mp(M, P):
          ports.add(P)

      if ports:
        port_sets.append(frozenset(ports))

    return port_sets

  def merged_muop_str(self, muops = None) -> str:
    port_sets = self.merge_muops(muops)
    return self.pretty_muop_str(port_sets)

  def used_ports(self) -> ty.FrozenSet[Port]:
    return set(P for P in self.ports() for M in self.muops() if self.delta_mp(M, P))

  @staticmethod
  def pretty_muop_str(port_sets: ty.Sequence[Port]) -> str:
    port_sets = [tuple(sorted(p.name for p in ports)) for ports in port_sets]
    port_sets = collections.Counter(port_sets)

    def longestCommonPrefix(strs):
      if not strs:
        return ""
      if len(strs) == 1:
        return strs[0]
      shortest_str = min(strs, key=len)
      for i, char in enumerate(shortest_str):
        for other in strs:
          if other[i] != char:
            return shortest_str[:i]
      return shortest_str

    txt = []

    for port_set in sorted(port_sets, key=lambda ps: [len(ps), ps]):
      if not port_set:
        txt += ['-']
      else:
        N      = port_sets[port_set]
        prefix = longestCommonPrefix(port_set)
        name   = prefix + ''.join([p[len(prefix):] for p in port_set])

        txt += [f'{N}{name}']

    return ' + '.join(txt)


class ILP_Output:
  def __init__(self, cpu: CPU_Model, kernel: Kernel, rho_opt: float, umu_opt: float, fmu_opt: float, has_error: bool):
    self.cpu        = cpu
    self.kernel     = kernel
    self.rho_opt    = rho_opt
    self.fmu_opt    = fmu_opt
    self.umu_opt    = umu_opt
    self.has_error  = has_error
    self._rho_i     = collections.defaultdict(float)
    self._mu_i      = collections.defaultdict(float)
    self._mu_m      = collections.defaultdict(float)
    self._mu_im     = collections.defaultdict(float)
    self._mu_mp     = collections.defaultdict(float)
    self._mu_it     = collections.defaultdict(float)
    self._saturated = set()

  @property
  def ipc_error(self) -> float:
    return self._errf(self.kernel.ipc, self.rho_opt)

  @property
  def umpc_error(self) -> float:
    return self._errf(self.kernel.umpc, self.umu_opt)

  @property
  def fmpc_error(self) -> float:
    return self._errf(self.kernel.fmpc, self.fmu_opt)

  @staticmethod
  def _errf(ref: float, est: float) -> float:
    """
      ref ... reference throughput
      est ... estimated throughput from ILP
    """

    # return (ref - est) ** 2
    ## absolute error
    # return abs(ref - est)
    ## relative error
    return abs(ref - est) / ref

  @property
  def error(self) -> float:
    return self.ipc_error + self.fmpc_error + self.umpc_error

  def rho_i(self, inst) -> float:
    return self._rho_i[inst]

  def mu_i(self, inst) -> float:
    return self._mu_i[inst]

  def mu_m(self, muop) -> float:
    return self._mu_m[muop]

  def mu_im(self, inst, muop) -> float:
    return self._mu_im[inst, muop]

  def mu_mp(self, muop, port) -> float:
    return self._mu_mp[muop, port]

  def mu_it(self, inst, throttle) -> float:
    return self._mu_it[inst, throttle]

  def used_muops(self, min_throughput: float = 0.001) -> ty.List[Muop]:
    muops = []

    for I in self.kernel:
      for M in self.cpu.muops():
        mu = self.mu_im(I, M)
        assert round(mu, 10) >= 0, [I, M, mu]

        if mu < min_throughput:
          continue

        assert self.cpu.delta_im(I, M), [I, M]

        muops.append(M)

    return muops

  def used_ports(self, min_throughput: float = 0.001) -> ty.List[Port]:
    ports = []

    for M in self.used_muops(min_throughput):
      for P in self.cpu.ports():
        mu = self.mu_mp(M, P)
        assert mu >= 0

        if mu < min_throughput:
          continue

        assert self.cpu.delta_mp(M, P), [M, P]

        ports.append(P)

    return ports

  def merged_muop_str(self) -> str:
    return self.cpu.merged_muop_str(self.used_muops())

  def saturated(self, muop) -> bool:
    return muop in self._saturated

  def _set_rho_i(self, inst, rho: float):
    self._rho_i[inst] = rho

  def _set_mu_i(self, inst, mu: float):
    self._mu_i[inst] = mu

  def _set_mu_m(self, muop, mu: float):
    self._mu_m[muop] = mu

  def _set_mu_im(self, inst, muop, rho: float):
    self._mu_im[inst, muop] = rho

  def _set_mu_mp(self, muop, port, rho: float):
    self._mu_mp[muop, port] = rho

  def _set_mu_it(self, inst, throttle, rho: float):
    self._mu_it[inst, throttle] = rho

  def _set_saturated(self, muop):
    # print(id(self), self.kernel, muop)
    self._saturated.add(muop)

  def to_dot(self) -> pygraphviz.AGraph:
    """
      Convert this graph to a DOT graph for pretty printing
    """

    dot = pygraphviz.AGraph(direct=True, rankdir='TB')
    self.add_to_dot(dot)
    return dot

  def add_to_dot(self, dot: pygraphviz.AGraph, id_prefix: str = None):
    """
      Add nodes & edges of this solution to DOT graph.
      All emitted node IDs are prefixed with :id_prefix:.
      By default :id_prefix: is the name of the kernel.
    """

    K = self.kernel

    if id_prefix is None:
      id_prefix = f'{K}_'

    def id(node, suffix = ''):
      return c_str(id_prefix + str(node) + str(suffix))

    color = '#ff0000' if self.has_error else '#000000'

    dot.add_node(id(K), label=Benchmark_Spec.name_from_instruction_names(I.name for I in K), shape='box')
    dot.add_node(id(K, 'IPC'), label=f'IPCopt = {round(self.rho_opt, 2)} IPC = {round(K.ipc, 2)}',
                 shape='ellipse', color=color)
    dot.add_node(id(K, 'uMPC'), label=f'uMPC-opt = {round(self.umu_opt, 2)} uMPC = {round(K.umpc, 2)}',
                 shape='ellipse', color=color)
    dot.add_node(id(K, 'fMPC'), label=f'fMPC-opt = {round(self.fmu_opt, 2)} fMPC = {round(K.fmpc, 2)}',
                 shape='ellipse', color=color)

    dot.add_edge(id(K, 'IPC'), id(K))
    dot.add_edge(id(K, 'fMPC'), id(K))

    muops     = set()
    ports     = set()
    throttles = set()
    IM = {}
    MP = {}
    IT = {}

    K_INSTS = sorted(set(K), key=lambda I: I.name)

    # import builtins
    # assert self._saturated, [builtins.id(self), K]

    for I in K_INSTS:
      # dot.add_node(id(I), label=I.name, shape='box')
      dot.add_node(id(I), label=f'{I.name} ρ={self.rho_i(I):.2f} μ={self.mu_i(I):.2f}', shape='box')

      dot.add_edge(id(K), id(I), label=f'{self.rho_i(I):.2f}=={round(K.ipc / len(K), 2)}x{K.count(I)}')
      # dot.add_edge(id(K), id(I), label=f'{round(K.ipc / len(K), 2)}x{K.count(I)}')

      for T in self.cpu.throttles():
        if not self.cpu.delta_it(I, T):
          assert round(self.mu_it(I, T), 10) == 0, [K, I, T, self.mu_it(I, T)]
          continue

        throttles.add(T)

        if abs(self.mu_it(I, T)) > 0.001:
          IT[I, T] = round(self.mu_it(I, T), 2)
        else:
          IT[I, T] = '-'

      for M in self.cpu.muops():
        if not self.cpu.delta_im(I, M):
          assert round(self.mu_im(I, M), 10) == 0, [K, I, M, self.mu_im(I, M)]
          continue

        muops.add(M)

        if abs(self.mu_im(I, M)) > 0.001:
          IM[I, M] = round(self.mu_im(I, M), 2)
        else:
          IM[I, M] = '-'

    for M in sorted(muops, key=lambda m: m.name):
      color = '#ff0000' if self.saturated(M) else '#000000'
      dot.add_node(id(M), label=f'{M.name} μ={self.mu_m(M):.2f}', shape='box', color=color)

      for P in self.cpu.ports():
        if abs(self.mu_mp(M, P)) > 0.001:
          MP[M, P] = round(self.mu_mp(M, P), 2)
          ports.add(P)
        elif self.cpu.delta_mp(M, P):
          MP[M, P] = '-'
          ports.add(P)

    for P in sorted(ports, key=lambda p: p.name):
      dot.add_node(id(P), label=P.name, shape='box')

      dot.add_edge(id(P), id(K, 'uMPC'), label=round(sum(self.mu_mp(M, P) for M in muops), 2))

    for T in sorted(throttles, key=lambda t: t.name):
      color = '#ff0000' if self.saturated(M) else '#000000'

      mu = sum([self.mu_it(I, T) for I in K_INSTS], 0)

      dot.add_node(id(T), label=f'{T.name} μ={round(mu, 2)}/{round(T.max_throughput,2)}',
                   color=color)

    for I, M in IM:
      dot.add_edge(id(I), id(M), label=IM[I, M])

    for M, P in MP:
      dot.add_edge(id(M), id(P), label=MP[M, P])

    for I, T in IT:
      dot.add_edge(id(I), id(T), label=IT[I, T])

  def to_ascii(self) -> str:
    dot = self.to_dot()
    dot.layout()
    dot_str = dot.string()

    cmd = ['graph-easy', '--from=graphviz', '--as=ascii']

    ret = subprocess.run(cmd, input=dot_str, universal_newlines=True, stdout=subprocess.PIPE, check=True)

    if ret.returncode != 0:
      exit(ret.returncode)

    return ret.stdout


class Model_Builder:
  def __init__(self):
    self._kernels = []
    self._insts   = set()
    self._muops   = set()
    self._ports   = set()

    self._k2i = collections.Counter()
    self._i2m = collections.Counter()
    self._m2p = collections.Counter()
    self._sat = set()

    self._names = set()

  def build(self):
    cpu = CPU_Model()

    for i in self.insts():
      cpu.add_inst(i)
    for m in self.muops():
      cpu.add_muop(m)
    for p in self.ports():
      cpu.add_port(p)

    for i, m in self._i2m:
      cpu.add_im_edge(i, m)
    for m, p in self._m2p:
      cpu.add_mp_edge(m, p)

    return tuple(self._kernels), cpu

  def make_kernel(self, *insts, ipc: float, mpc: float) -> 'Kernel':
    k = Kernel(ipc, mpc, insts)
    self.add_kernel(k)
    for i in insts:
      self.add_ki_edge(k, i)
    return k

  def add_kernel(self, kernel):
    if kernel not in self._kernels:
      self._kernels.append(kernel)

  def make_port(self, name) -> 'Port':
    p = Port(name)
    self.add_port(p)
    return p

  def add_port(self, port):
    self._ports.add(port)

  def make_muop(self, *ports, suffix = "") -> 'Muop':
    name = ''
    for p in ports:
      name += p.name
    name += suffix

    m = Muop(name)
    assert m not in self._muops
    self.add_muop(m)
    for p in ports:
      assert p in self._ports
      assert (m, p) not in self._m2p

      self.add_mp_edge(m, p)
    return m

  def add_muop(self, muop):
    self._muops.add(muop)

  def make_inst(self, name, *muops) -> 'Inst':
    i = Inst(name, len(muops))
    assert i not in self._insts
    self.add_inst(i)

    cnt = collections.Counter()
    for m in muops:
      cnt[i, m] += 1

    for m in muops:
      assert m in self._muops, m
      assert (i, m) not in self._i2m, (i, m)

      if cnt[i, m]:
        self.add_im_edge(i, m)
    return i

  def add_inst(self, inst):
    self._insts.add(inst)

  def add_ki_edge(self, kernel, inst, weight = 1):
    assert kernel in self._kernels
    self.add_inst(inst)
    self._k2i[kernel, inst] = weight

  def add_im_edge(self, inst, muop, weight = 1):
    self.add_inst(inst)
    self.add_muop(muop)
    self._i2m[inst, muop] = weight

  def add_mp_edge(self, muop, port, weight = 1):
    self.add_muop(muop)
    self.add_port(port)
    self._m2p[muop, port] = weight

  def kernels(self):
    return self._kernels

  def insts(self):
    return tuple(sorted(self._insts, key=lambda obj: obj.name))

  def inst_2_muop(self, inst, muop) -> bool:
    return bool(self._i2m[inst, muop])

  def muop_2_port(self, muop, port) -> bool:
    return bool(self._m2p[muop, port])

  def muops(self):
    return tuple(sorted(self._muops, key=lambda obj: obj.name))

  def ports(self):
    return tuple(sorted(self._ports, key=lambda obj: obj.name))


DEBUG_EXPR = False
# DEBUG_EXPR = True


class ILP_Model_Base:
  def __init__(self):
    self.built = False
    self._m = gurobipy.Model()
    self._constraint_2_expr = {}

  @property
  def m(self) -> gurobipy.Model:
    return self._m

  if DEBUG_EXPR:
    def make_var(self, name: str, vtype: str, lb: float = 0.0) -> 'Var':
      assert vtype in (GRB.BINARY, GRB.INTEGER, GRB.CONTINUOUS), vtype

      assert not self.built, 'cannot add variable to fully built model'

      return Var(self.m.addVar(name=name, vtype=vtype, lb=lb))

    @staticmethod
    def make_const(name: str, vtype: str, const: Gurobi_Value) -> 'Const':
      return Const.from_value(name, vtype, const)

    @staticmethod
    def make_const_bool(const: bool) -> 'Const':
      return Const.from_bool(str(int(const)), const)

    def set_objective(self, model_sense, main_objective, *sub_objectives):
      assert type(main_objective) is Expr, main_objective

      self.m.setObjective(main_objective.gurobi_obj, sense=model_sense)

      for i, obj in enumerate(sub_objectives, 1):
        assert type(obj) is Expr, obj

        self.m.setObjectiveN(obj.gurobi_obj, index=i, priority=len(sub_objectives) + 1 - i)

    def constraint(self, name, expr):
      # expr = Operations.const_promote(expr)

      if type(expr) is Const:
        assert expr, f'Unsatisfiable const false constraint: {name}: {expr}'
        return

      assert type(expr) is Expr, type(expr)

      name = name + ' ' + str(expr)
      while len(name.encode()) > 255:
        name = name[:-1]

      c = self.m.addConstr(expr.gurobi_obj, name=name)
      self._constraint_2_expr[c] = expr

    @classmethod
    def Σ(clss, seq: ty.Sequence[Operations]):
      seq = list(seq)

      txt = 'Σ(' + ', '.join([str(e) for e in seq]) + ')'

      if not seq:
        return clss.make_const(txt, GRB.CONTINUOUS, 0.0)

      objs = []

      for op in seq:
        op = Operations.const_promote(op)

        top = type(op)

        assert top in (Const, Var, Expr)

        if top is Const:
          objs.append(op.const)
        else:
          objs.append(op.gurobi_obj)

      return Expr(gurobipy.quicksum(objs), txt)

    @staticmethod
    def abs_(expr: Operations) -> Expr:
      if type(expr) is Const:
        g_obj = expr.const
      elif type(expr) in (Var, Expr):
        g_obj = expr.gurobi_obj
      else:
        assert False, [expr, type(expr)]

      if DEBUG_EXPR:
        txt = f'abs({expr})'
      else:
        txt = ''

      return Expr(gurobipy.abs_(g_obj), txt)

    @classmethod
    def and_(clss, args: ty.Iterable[Operations]) -> Expr:
      vars = []

      for arg in args:
        if type(arg) is Var:
          vars.append(arg)
        else:
          assert type(arg) is Const, arg
          assert type(arg.const) is bool, arg

          if arg.const:
            # X and True == X
            continue
          else:
            # X and False == False
            return clss.make_const('and()', GRB.BINARY, False)

      txt = 'and(' + ', '.join(map(str, vars)) + ')'

      return Expr(gurobipy.and_(*[v.gurobi_obj for v in vars]), txt)

    @classmethod
    def if_then(clss, a, b) -> Expr:
      return a >> b

    @classmethod
    def get_python_value(clss, obj: 'Operations', default: Gurobi_Value = None) -> Gurobi_Value:
      return obj.python_value(default)
  else:
    def make_var(self, name: str, vtype: str, lb: float = 0.0) -> 'Var':
      assert not self.built, 'cannot add variable to fully built model'

      return self.m.addVar(name=name, vtype=vtype, lb=lb)

    @staticmethod
    def make_const(name: str, vtype: str, const: Gurobi_Value) -> 'Const':
      return const

    @staticmethod
    def make_const_bool(const: bool) -> 'Const':
      return const

    def set_objective(self, model_sense, main_objective, *sub_objectives):
      self.m.setObjective(main_objective, sense=model_sense)

      for i, obj in enumerate(sub_objectives, 1):
        self.m.setObjectiveN(obj, index=i, priority=len(sub_objectives) + 1 - i)

    def constraint(self, name, expr):
      if type(expr) in (int, float, bool):
        assert expr, f'Unsatisfiable const false constraint: {name}: {expr}'
        return

      c = self.m.addConstr(expr, name=name)
      return c

    @classmethod
    def if_then(clss, a, b) -> 'Constraint':
      """
        Encode logical implication.
          (expr1) >> (expr2)
        means a constraint like
          if (expr1) then (expr2) else True
      """

      if type(a) is bool:
        if a:
          return b
        else:
          return True
      elif type(b) is bool:
        if b:
          return True
        else:
          raise ValueError('Unsatisfiable boolean constraint')
      else:
        assert type(a) not in (int, float)
        assert type(b) not in (int, float)

        return a >> b

    Σ    = gurobipy.quicksum
    abs_ = gurobipy.abs_
    and_ = gurobipy.and_

    @classmethod
    def get_python_value(clss, obj: 'Operations', default: Gurobi_Value) -> Gurobi_Value:
      if type(obj) is gurobipy.Var:
        return Var.get_python_value(obj, default)
      return obj

  def make_relation(self, long_name: str, pretty_name: str, vtype: str, *domain, constants=None):
    constants = constants or {}

    relation = Relation(pretty_name, vtype, domain=domain,
                        make_var=self.make_var,
                        make_const=self.make_const,
                        constants=constants)

    assert not hasattr(self, long_name)

    setattr(self, long_name, relation)
    return relation

  def optimize(self, print_iis: bool = True):
    self.built = True
    self.m.optimize()

    if self.m.status == GRB.UNBOUNDED:
      raise ILP_Error("could not solve model, solution is UNBOUNDED")

    if self.m.status == GRB.INFEASIBLE:
      if print_iis:
        print('The model is infeasible; computing IIS')
        self.m.computeIIS()
        print('\nThe following constraint(s) cannot be satisfied:')
        iis_constrs = [c for c in self.m.getConstrs() if c.IISConstr]
        # iis_constrs = iis_constrs[:100]
        for c in iis_constrs:
          expr = self._constraint_2_expr.get(c)
          if expr and DEBUG_EXPR:
            print(' ', c.constrName.split()[0] + ':', expr)
          else:
            print(' ', c.constrName)
      raise ILP_Error("could not solve model, it is INFEASIBLE")

    if self.m.status == GRB.INF_OR_UNBD:
      raise ILP_Error("could not solve model, it is INFEASIBLE or solution is UNBOUNDED")

    if self.m.status == GRB.INTERRUPTED:
      print(self.m.ObjVal)
      raise ILP_Error(f"solver was INTERRUPTED (status={self.m.status})")

    if self.m.status != GRB.OPTIMAL:
      raise ILP_Error(f"no optmial solution found (status={self.m.status})")

    return self.m.ObjVal


Σ = ILP_Model_Base.Σ
if_then = ILP_Model_Base.if_then


class ILP_Model(ILP_Model_Base):
  CONST_MU_KP_MARGIN_DEFAULT: float = 0.1

  def __init__(self,
               KERNELS: Domain[Kernel],
               INSTS: Domain[Inst],
               MUOPS: Domain[Muop],
               PORTS: Domain[Port],
               THROTTLES: Domain[Port],
               *,
               const_delta_im: ty.Dict[ty.Tuple[Inst, Muop], bool] = None,
               const_delta_mp: ty.Dict[ty.Tuple[Muop, Port], bool] = None,
               const_delta_ip: ty.Dict[ty.Tuple[Inst, Port], bool] = None,
               const_delta_it: ty.Dict[ty.Tuple[Inst, Port], bool] = None,
               const_mu_KP: ty.Dict[ty.Tuple[Kernel, Port], float] = None,
               # minimum number of ports a muop can go to
               min_num_ports_for_muop: ty.Dict[Muop, int] = None,
               # kernels containing at least one saturated/bottleneck muop
               kernels_with_bottleneck: ty.Set[Kernel] = (),
               # the throughput of a saturatd port is at least (1 * saturation_margin)
               saturation_margin: float = 0.995,
               # a muop that has an MPC>0 must have at least this much MPC
               min_throughput: float = 0.001,
               # maximum number of fused muops that can execute/retire per cycle
               max_fused_mpc: float = 4,
               # error margin for hardcoded port usage passed with :const_mu_KP:
               const_mu_KP_margin: float = CONST_MU_KP_MARGIN_DEFAULT,
               # log to console, etc.
               verbose: bool = False,
               ):
    const_delta_im = const_delta_im or {}
    const_delta_mp = const_delta_mp or {}
    const_delta_ip = const_delta_ip or {}
    const_delta_it = const_delta_it or {}
    const_mu_KP    = const_mu_KP or {}
    min_num_ports_for_muop =  min_num_ports_for_muop or {}

    super().__init__()
    m = self._m
    # m.Params.LogFile      = 'gurobi.log'
    m.Params.LogToConsole = verbose

    assert 0 <= saturation_margin < 1
    assert 0 < min_throughput <= 1
    assert 0 <= const_mu_KP_margin <= 1

    self.saturation_margin = saturation_margin

    self.INSTS     = INSTS
    self.MUOPS     = MUOPS
    self.PORTS     = PORTS
    self.THROTTLES = THROTTLES
    self.KERNELS   = KERNELS

    rho_opt = self.make_relation('rho_opt', 'ρ_opt', GRB.CONTINUOUS, KERNELS)
    umu_opt = self.make_relation('umu_opt', 'μ_opt', GRB.CONTINUOUS, KERNELS)
    fmu_opt = self.make_relation('fmu_opt', 'μ_fused', GRB.CONTINUOUS, KERNELS)

    rho_KI = self.make_relation('rho_KI', 'ρ', GRB.CONTINUOUS, KERNELS, INSTS,
                                constants={(K, I): 0 for K, I in KERNELS * INSTS if I not in K})
    mu_KI = self.make_relation('mu_KI', 'μ', GRB.CONTINUOUS, KERNELS, INSTS,
                               constants={(K, I): 0 for K, I in KERNELS * INSTS if I not in K})

    mu_KIM = self.make_relation('mu_KIM', 'μ', GRB.CONTINUOUS, KERNELS, INSTS, MUOPS,
                                constants={(K, I, M): 0 for K, I, M in KERNELS * INSTS * MUOPS
                                           if const_delta_im.get((I, M)) == 0})
    mu_KM  = self.make_relation('mu_KM', 'μ', GRB.CONTINUOUS, KERNELS, MUOPS)
    mu_KMP = self.make_relation('mu_KMP', 'μ', GRB.CONTINUOUS, KERNELS, MUOPS, PORTS,
                                constants={(K, M, P): 0 for K, M, P in KERNELS * MUOPS * PORTS
                                           if const_delta_mp.get((M, P)) == 0})
    mu_KP = self.make_relation('mu_KP', 'μ', GRB.CONTINUOUS, KERNELS, PORTS,
                               # constants=const_mu_KP)
                               # constants={(K, P): 0 for K, P in KERNELS * PORTS
                               #            if const_mu_KP.get((K, P)) == 0})
                               )
    mu_KIT = self.make_relation('mu_KIT', 'μ', GRB.CONTINUOUS, KERNELS, INSTS, THROTTLES)

    mu_fused_KI = self.make_relation('mu_fused_KI', 'μ_fused', GRB.CONTINUOUS, KERNELS, INSTS)

    # delta_IM = self.make_relation('delta_IM', 'δ', GRB.BINARY, INSTS, MUOPS, constants=const_delta_im)
    self.delta_IM = delta_IM = {(I, M): self.make_const_bool(const_delta_im[I, M]) for I, M in INSTS * MUOPS}
    self.delta_IT = delta_IT = {(I, T): self.make_const_bool(const_delta_it[I, T]) for I, T in INSTS * THROTTLES}
    self.delta_MP = delta_MP = self.make_relation('delta_MP', 'δ', GRB.BINARY, MUOPS, PORTS, constants=const_delta_mp)

    saturated_KM      = self.make_relation('saturated_KM', 'sat', GRB.BINARY, KERNELS, MUOPS)
    saturated_KT      = self.make_relation('saturated_KT', 'sat', GRB.BINARY, KERNELS, THROTTLES)
    saturated_KMP     = self.make_relation('saturated_KMP', 'sat', GRB.BINARY, KERNELS, MUOPS, PORTS)
    saturated_KP      = self.make_relation('saturated_KP', 'sat', GRB.BINARY, KERNELS, PORTS)
    saturated_K_issue = self.make_relation('saturated_K_issue', 'sat', GRB.BINARY, KERNELS)

    if DEBUG_EXPR:
      m.update()

    info = Info_Line()

    def constraint(name, expr):
      # MSG = info.timestamp() + '- KERNEL', f'{i}/{len(KERNELS)}' + '-', str(K)
      # info(MSG, '- BGN ', name)
      self.constraint(name, expr)
      # info(MSG, '- END ', name)

    # import datetime
    # prev = datetime.datetime.now()

    for i, K in enumerate(KERNELS, 1):
      # now = datetime.datetime.now()
      # info(info.timestamp(now),
      #      '- KERNEL', f'{i}/{len(KERNELS)}', '-', K, rprompt=f'[{round((now - prev).total_seconds(), 1)}s]')
      # prev = now

      K_INSTS = Domain(Inst, sorted(set(K)))

      # ## C1 -- we want the measured IPC
      # constraint('C1', rho_opt[K] == K.ipc)

      # ## C2 -- we want the measured MPC
      # constraint('C2', umu_opt[K] == K.mpc)

      ## C3 -- (ρ_opt / ρ_emp) == (μ_opt / μ_emp)
      # constraint('C3', (rho_opt[K] * mu_emp[K]) == (umu_opt[K] * rho_emp[K]))

      ## C4 -- total IPC is sum of IPCs of instructions
      constraint('C4', rho_opt[K] == Σ(rho_KI[K, I] for I in K_INSTS))

      # C5 -- IPC of kernel is split evenly among all instructions
      for I1, I2 in K_INSTS * K_INSTS:
        assert K.count(I1) and K.count(I2)
        constraint('C5', K.count(I2) * rho_KI[K, I1] == K.count(I1) * rho_KI[K, I2])

      # # C5R -- redundant alternative formulation of C5
      # for I in INSTS:
      #   constraint('C5R', rho_KI[K, I] * len(K) == rho_opt[K] * K.count(I))

      ## C6 -- IPC instructions not in kernel is zero
      for I in INSTS:
        if K.count(I) == 0:
          constraint('C6', rho_KI[K, I] == 0)

      ## C7 -- IPC * num-muops == MPC (translate IPC to MPC)
      for I in K_INSTS:
        constraint('C7', I.num_unfused_muops * rho_KI[K, I] == mu_KI[K, I])

      ## C8 -- MPC is split evenly among muops for an instruction
      for I, M in K_INSTS * MUOPS:
        constraint('C8', if_then(delta_IM[I, M] == 1, rho_KI[K, I] == mu_KIM[K, I, M]))

      # C9 -- total unfused MPC for an instruction is sum of MPC of its muops
      for I in K_INSTS:
        constraint('C9', mu_KI[K, I] == Σ(mu_KIM[K, I, M] for M in MUOPS))

      # C11 -- an instruction causes no MPC in a muop it is not connected to
      for I, M in K_INSTS * MUOPS:
        constraint('C11', if_then(delta_IM[I, M] == 0, mu_KIM[K, I, M] == 0))

      ## C12 -- total MPC for a muop is sum of MPC coming from all instructions it is connected to
      for M in MUOPS:
        constraint('C12', mu_KM[K, M] == Σ(mu_KIM[K, I, M] for I in K_INSTS))

      ## C13 -- total MPC for a muop is sum of MPC going to all ports it is connected to
      for M in MUOPS:
        constraint('C13', mu_KM[K, M] == Σ(mu_KMP[K, M, P] for P in PORTS))

      ## C14 -- total MPC for a port is sum of MPC coming to all muops it is connected to
      for P in PORTS:
        constraint('C14', mu_KP[K, P] == Σ(mu_KMP[K, M, P] for M in MUOPS))

      ## C16 -- a muop causes no MPC in a port it is not connected to
      for M, P in MUOPS * PORTS:
        constraint('C16', mu_KMP[K, M, P] <= delta_MP[M, P])

      ## C17 -- a port can only execute one muop per cycle
      for P in PORTS:
        constraint('C17', mu_KP[K, P] <= 1)

      ## C18 -- total kernel MPC is sum of MPC across all ports
      constraint('C18', umu_opt[K] == Σ(mu_KP[K, P] for P in PORTS if P.is_real))

      # C19 -- if a muop and a port are not connected no MPC flows between them
      for M, P in MUOPS * PORTS:
        constraint('C19', if_then(delta_MP[M, P] == 0, mu_KMP[K, M, P] == 0))

      ## Kudos to Fabian Ritter for this

      ## C21 -- if muop M is saturated all ports reachable from it are saturated (have MPC==1)
      for M, P in MUOPS * PORTS:
        assert P.max_throughput == 1, P

        # constraint('C21.1', saturated_KMP[K, M, P] == and_([saturated_KM[K, M], delta_MP[M, P]]))

        saturated_throughput = max(P.max_throughput * min_throughput, P.max_throughput * saturation_margin)

        # constraint('C21.2', (saturated_KMP[K, M, P] == 1) >> (mu_KP[K, P] >= saturated_throughput))

        constraint('C21', (saturated_KM[K, M] + delta_MP[M, P] - (2 - saturated_throughput)) <= mu_KP[K, P])

      ## C22 -- if a muop is saturated it produces some MPC
      ##        (this just forbids the solver from marking unused muops as saturated)
      for M in MUOPS:
        constraint('C22', if_then(saturated_KM[K, M] == 1, mu_KM[K, M] >= min_throughput))

      for T in THROTTLES:
        mu_KT = Σ(mu_KIT[K, I, T] for I in K_INSTS)

        constraint('C22.2', if_then(saturated_KT[K, T] == 1, mu_KT >= T.max_throughput * saturation_margin))

      ## C22.3 -- translate unfused to fused MPC
      for I in K_INSTS:
        constraint('C22.3', mu_fused_KI[K, I] == (I.num_fused_muops / I.num_unfused_muops) * mu_KI[K, I])

      constraint('C22.4', fmu_opt[K] == Σ(mu_fused_KI[K, I] for I in K_INSTS))
      constraint('C22.5', fmu_opt[K] <= max_fused_mpc)

      if K in kernels_with_bottleneck:
        ## C23 -- there is always at least one saturated muop
        constraint('C23', Σ(saturated_KM[K, M] for M in MUOPS) +
                          Σ(saturated_KT[K, T] for T in THROTTLES) #+
                          #saturated_K_issue[K]
                          >= 1)
      # else:
      #   constraint('C24', Σ(saturated_KM[K, M] for M in MUOPS) == 0)

      for P in PORTS:
        constraint('C21.3', if_then(saturated_KP[K, P] == 1, mu_KP[K, P] >= saturated_throughput))

      for P in PORTS:
        constraint('CmuKP.1', mu_KP[K, P] <= P.max_throughput)

      if const_mu_KP:
        for P in PORTS:
          mu = const_mu_KP.get((K, P))

          if mu is not None:
            mu = min(mu, P.max_throughput)
            mu = max(mu, 0)

            # print(f'{mu - 0.25} <= mu_KP[{K}, {P}] <= {mu + 0.25}')

            constraint('CmuKP.hi', mu_KP[K, P] <= mu + const_mu_KP_margin)
            constraint('CmuKP.lo', mu_KP[K, P] >= mu - const_mu_KP_margin)

            # # constraint('CmuKP.hi', mu_KP[K, P] <= mu + 0.2)
            # # constraint('CmuKP.lo', mu_KP[K, P] >= mu - 0.2)

            # if mu <= 0.05:
            #   constraint('CmuKP', mu_KP[K, P] <= 0.05)
            # elif mu < 0.6:
            #   constraint('CmuKP.2', mu_KP[K, P] <= 0.6)
            #   for M in MUOPS:
            #     constraint('CmuKP.3', saturated_KMP[K, M, P] == 0)

            # if mu >= P.max_throughput * saturation_margin:
            #   constraint('CmuKP.4', mu_KP[K, P] >= P.max_throughput * saturation_margin)
            # # # constraint('CmuKP', mu_KP[K, P] >= min_throughput)

      for I, T in K_INSTS * THROTTLES:
        assert (I, T) in delta_IT

        constraint('CmuIT', if_then(delta_IT[I, T] == 0, mu_KIT[K, I, T] == 0))
        constraint('CmuIT', if_then(delta_IT[I, T] == 1, mu_KIT[K, I, T] == mu_KI[K, I]))

      for T in THROTTLES:
        constraint('CmuIT', Σ(mu_KIT[K, I, T] for I in INSTS) <= T.max_throughput)

    ## C10 -- number of out-edges of an instruction is same as number of muops
    for I in INSTS:
      constraint('C10', Σ(delta_IM[I, M] for M in MUOPS if not M.is_virtual) == I.num_unfused_muops)

    ## C20 -- if some instruction produces a MUOP
    ##        then that MUOP must be able to go to some port
    for I, M in INSTS * MUOPS:
      constraint('C20', if_then(delta_IM[I, M] == 1, Σ(delta_MP[M, P] for P in PORTS) >= 1))

    if const_delta_ip:
      for IP, δ in const_delta_ip.items():
        I, P = IP

        for M in MUOPS:
          ## Gurobi limitation: 'and' can only be used in constraints like:
          ##                       'VAR = and(OTHER_VARS)'
          ##                    The following does not work:
          ##                       'CONST == and(OTHER_VARS)'

          imp = delta_IMP[I, M, P]

          if type(imp) is Const:
            if imp.const:
              constraint('CδIMP', 2 == delta_IM[I, M] + delta_MP[M, P])
            else:
              constraint('CδIMP', 1 >= delta_IM[I, M] + delta_MP[M, P])
          else:
            assert type(imp) is Var
            constraint('CδIMP', imp == and_([delta_IM[I, M], delta_MP[M, P]]))

        if δ:
          constraint('CδIP', Σ(delta_IMP[I, M, P] for M in MUOPS) >= 1)
        else:
          constraint('CδIP', Σ(delta_IMP[I, M, P] for M in MUOPS) == 0)

    m.update()
    info.clear()


def solve_for_delta(KERNELS: Domain[Kernel],
                    INSTS: Domain[Inst],
                    MUOPS: Domain[Muop],
                    PORTS: Domain[Port],
                    THROTTLES: Domain[Port], *,
                    max_pos_error: float = 0,
                    max_neg_error: float = 0,
                    print_iis: bool = True,
                    allow_errors: bool = False,
                    verbose: bool = True,
                    **ilp_kwargs,
                    ) -> ty.List[ILP_Output]:
  ## reconstruct model from measured IPC

  def log(*msg):
    if verbose:
      print(*msg)

  assert max_pos_error >= 0
  assert max_neg_error >= 0

  log('--- BGN GEN MODEL', datetime.datetime.now(), '-------------------------')

  ilp = ILP_Model(
    KERNELS, INSTS, MUOPS, PORTS, THROTTLES,
    verbose = verbose,
    **ilp_kwargs,
  )

  if allow_errors:
    errors = {K: ilp.make_var(f'err_{K}', GRB.BINARY) if len(K) > 1 else ilp.make_const_bool(False) for K in KERNELS}
    ilp.m.update()

    ilp.constraint('XXX', Σ(errors.values()) <= 10)
  else:
    errors = {K: ilp.make_const_bool(False) for K in KERNELS}

  for K in KERNELS:
    error = errors[K]

    ## C1 -- we want the measured IPC
    # ilp.constraint('C1', ilp.rho_opt[K] >= K.ipc)
    # ilp.constraint('C1.L', ilp.rho_opt[K] <= K.ipc + max_error)
    # ilp.constraint('C1.H', ilp.rho_opt[K] >= K.ipc - max_error)
    ilp.constraint('C1.L', if_then(error == 0, ilp.rho_opt[K] <= K.ipc + max_pos_error))
    ilp.constraint('C1.H', if_then(error == 0, ilp.rho_opt[K] >= K.ipc - max_neg_error))

    ## C2 -- we want the measured unfused MPC
    # ilp.constraint('C1', ilp.umu_opt[K] >= K.mpc)
    # ilp.constraint('C2.L', ilp.umu_opt[K] <= K.mpc + max_error)
    # ilp.constraint('C2.H', ilp.umu_opt[K] >= K.mpc - max_error)
    ilp.constraint('C2.L', if_then(error == 0, ilp.umu_opt[K] <= K.umpc + max_pos_error))
    ilp.constraint('C2.H', if_then(error == 0, ilp.umu_opt[K] >= K.umpc - max_neg_error))

    ## C3 -- we want the measured fused MPC
    ilp.constraint('C3.L', if_then(error == 0, ilp.umu_opt[K] <= K.fmpc + max_pos_error))
    ilp.constraint('C3.H', if_then(error == 0, ilp.umu_opt[K] >= K.fmpc - max_neg_error))

    if len(K) != 1:
      continue

    ilp.constraint('Cno-err', error == 0)

    # single instruction kernel
    const_delta_im = ilp_kwargs.get('const_delta_im', {})
    inst  = K[0]
    edges = [(M, const_delta_im[inst, M]) for M in MUOPS if (inst, M) in const_delta_im]

    if len(edges) != len(MUOPS):
      continue

    # we know which muops every instruction produces

    muops = [M for M, b in edges if b]
    assert muops

    if len(muops) != 1:
      continue

    # all throughput of kernel goes through one muop
    muop = muops[0]

    min_num_ports = math.floor(K.umpc) or 1
    assert 0 < min_num_ports
    ilp.constraint('CX', Σ(ilp.delta_MP[muop, P] for P in PORTS) >= min_num_ports)

  objectives = []

  if allow_errors:
    objectives += [
      ## as few unexplainable kernels as possible
      Σ(errors.values()),
    ]

  ilp.set_objective(
    GRB.MINIMIZE,
    ## minimize number of edges between muops and ports (a saturated edge is considered 'free')
    Σ(ilp.delta_MP[M, P] for M, P in MUOPS * PORTS),
    # Σ(ilp.delta_MP[M, P] - ilp.mu_KMP[K, M, P] for K, M, P in KERNELS * MUOPS * PORTS),
    ## maximize pressure
    -Σ(ilp.saturated_KM[K, M] for K, M in KERNELS * MUOPS),
    # -Σ(ilp.mu_KP[K, P] for K, P in KERNELS * PORTS),
    # -Σ(ilp.rho_opt[K] for K in KERNELS),
    *objectives,
  )

  # ilp.set_objective(
  #   GRB.MINIMIZE,
  #   - Σ(ilp.rho_opt[K] + ilp.umu_opt[K] + ilp.umu_opt[K] for K in KERNELS) ,
  # )
  log('--- END GEN MODEL', datetime.datetime.now(), '-------------------------')

  log('--- BGN SOLVE', datetime.datetime.now(), '-----------------------------')
  try:
    ilp.optimize(print_iis=print_iis)
  except ILP_Error as e:
    raise e
  finally:
    log('--- END SOLVE', datetime.datetime.now(), '---------------------------')

  return model_to_outputs(ilp, errors)


def solve_for_error(CPU, kernels, *,
                    # max_error: float,
                    saturation_margin: float,
                    min_throughput: float,
                    kernels_with_bottleneck: set,
                    const_mu_KP: dict = None,
                    print_iis: bool = False,
                    verbose: bool = True,
                    ) -> ty.List[ILP_Output]:
  def log(*msg):
    if verbose:
      print(*msg)

  log('--- BGN GEN MODEL', datetime.datetime.now(), '-------------------------')

  KERNELS   = Domain(Kernel, kernels)
  INSTS     = Domain(Inst, CPU.insts())
  MUOPS     = Domain(Muop, CPU.muops())
  PORTS     = Domain(Port, CPU.ports())
  THROTTLES = Domain(Port, CPU.throttles())

  delta_im = {(I, M): CPU.delta_im(I, M) for I, M in INSTS * MUOPS}
  delta_it = {(I, T): CPU.delta_it(I, T) for I, T in INSTS * THROTTLES}
  delta_mp = {(M, P): CPU.delta_mp(M, P) for M, P in MUOPS * PORTS}

  ilp = ILP_Model(KERNELS, INSTS, MUOPS, PORTS, THROTTLES,
                  const_delta_im          = delta_im,
                  const_delta_it          = delta_it,
                  const_delta_mp          = delta_mp,
                  kernels_with_bottleneck = kernels_with_bottleneck,
                  saturation_margin       = saturation_margin,
                  min_throughput          = min_throughput,)

  ipc_diff  = {K: ilp.make_var(f'ipc_diff_{K}', GRB.CONTINUOUS) for K in KERNELS}
  fmpc_diff = {K: ilp.make_var(f'fmpc_diff_{K}', GRB.CONTINUOUS) for K in KERNELS}
  umpc_diff = {K: ilp.make_var(f'umpc_diff_{K}', GRB.CONTINUOUS) for K in KERNELS}

  ipc_error  = {K: ilp.make_var(f'ipc_err_{K}', GRB.CONTINUOUS) for K in KERNELS}
  fmpc_error = {K: ilp.make_var(f'fmpc_err_{K}', GRB.CONTINUOUS) for K in KERNELS}
  umpc_error = {K: ilp.make_var(f'umpc_err_{K}', GRB.CONTINUOUS) for K in KERNELS}

  ilp.m.update()

  for K in KERNELS:
    ilp.constraint('C-ipc-diff',  ipc_diff[K]  == K.ipc  - ilp.rho_opt[K])
    ilp.constraint('C-fmpc-diff', fmpc_diff[K] == K.fmpc - ilp.fmu_opt[K])
    ilp.constraint('C-umpc-diff', umpc_diff[K] == K.umpc - ilp.umu_opt[K])

    ilp.constraint('C-ipc-err',  ipc_error[K]  == abs_(ipc_diff[K]))
    ilp.constraint('C-fmpc-err', fmpc_error[K] == abs_(fmpc_diff[K]))
    ilp.constraint('C-umpc-err', umpc_error[K] == abs_(umpc_diff[K]))

  ilp.set_objective(
    GRB.MAXIMIZE,
    ## minimize error
    -Σ(ipc_error[K] + fmpc_error[K] + umpc_error[K] for K in KERNELS),
    ## maximize pressure
    # Σ(ilp.rho_opt[K] for K in KERNELS),
    Σ(ilp.saturated_KM[K, M] for K, M in KERNELS * MUOPS),
  )

  log('--- END GEN MODEL', datetime.datetime.now(), '-------------------------')

  log('--- BGN SOLVE', datetime.datetime.now(), '-----------------------------')
  try:
    ilp.optimize(print_iis=print_iis)
  except ILP_Error as e:
    raise e
  finally:
    log('--- END SOLVE', datetime.datetime.now(), '---------------------------')

  return model_to_outputs(ilp)


def model_to_outputs(ilp: ILP_Model, errors: dict = None) -> [ILP_Output]:
  errors = errors or {}

  cpu = CPU_Model()

  for I, M in ilp.INSTS * ilp.MUOPS:
    if ilp.get_python_value(ilp.delta_IM[I, M], False):
      cpu.add_im_edge(I, M)

  for M, P in ilp.MUOPS * ilp.PORTS:
    if ilp.get_python_value(ilp.delta_MP[M, P], False):
      cpu.add_mp_edge(M, P)

  for I, T in ilp.INSTS * ilp.THROTTLES:
    if ilp.get_python_value(ilp.delta_IT[I, T], False):
      cpu.add_it_edge(I, T)

  outputs = []

  BAD_RHO = BAD_MU = -0.0

  for K in ilp.KERNELS:
    out = ILP_Output(cpu, K,
                     rho_opt   = ilp.get_python_value(ilp.rho_opt[K], BAD_RHO),
                     umu_opt   = ilp.get_python_value(ilp.umu_opt[K], BAD_MU),
                     fmu_opt   = ilp.get_python_value(ilp.fmu_opt[K], BAD_MU),
                     has_error = errors[K].python_value(True) if K in errors else False)
    outputs.append(out)

    for M in ilp.MUOPS:
      if ilp.get_python_value(ilp.saturated_KM[K, M], False):
        out._set_saturated(M)

      out._set_mu_m(M, ilp.get_python_value(ilp.mu_KM[K, M], BAD_MU))

    for T in ilp.THROTTLES:
      if ilp.get_python_value(ilp.saturated_KT[K, T], False):
        out._set_saturated(T)

    for I in ilp.INSTS:
      out._set_rho_i(I, ilp.get_python_value(ilp.rho_KI[K, I], BAD_RHO))
      out._set_mu_i(I, ilp.get_python_value(ilp.mu_KI[K, I], BAD_MU))

    for I, M in ilp.INSTS * ilp.MUOPS:
      out._set_mu_im(I, M, ilp.get_python_value(ilp.mu_KIM[K, I, M], BAD_MU))

    for M, P in ilp.MUOPS * ilp.PORTS:
      out._set_mu_mp(M, P, ilp.get_python_value(ilp.mu_KMP[K, M, P], BAD_MU))

    for I, T in ilp.INSTS * ilp.THROTTLES:
      out._set_mu_it(I, T, ilp.get_python_value(ilp.mu_KIT[K, I, T], BAD_MU))

  return outputs


def solve_for_throughput(CPU, kernels,
                         saturation_margin: float,
                         min_throughput: float,
                         kernels_with_bottleneck: set,
                         const_mu_KP: Dict[Port, float] = None,
                         const_mu_KP_margin: float = ILP_Model.CONST_MU_KP_MARGIN_DEFAULT,
                         print_iis: bool = False,
                         verbose: bool = True,
                         ) -> ty.List[ILP_Output]:
  """ calculate IPC for given graph """

  def log(*msg):
    if verbose:
      print(*msg)

  log('--- BGN GEN MODEL', datetime.datetime.now(), '-------------------------')

  KERNELS   = Domain(Kernel, kernels)
  INSTS     = Domain(Inst, CPU.insts())
  MUOPS     = Domain(Muop, CPU.muops())
  PORTS     = Domain(Port, CPU.ports())
  THROTTLES = Domain(Port, CPU.throttles())

  delta_im = {(I, M): CPU.delta_im(I, M) for I, M in INSTS * MUOPS}
  delta_it = {(I, T): CPU.delta_it(I, T) for I, T in INSTS * THROTTLES}
  delta_mp = {(M, P): CPU.delta_mp(M, P) for M, P in MUOPS * PORTS}

  ilp = ILP_Model(KERNELS, INSTS, MUOPS, PORTS, THROTTLES,
                  const_delta_im          = delta_im,
                  const_delta_it          = delta_it,
                  const_delta_mp          = delta_mp,
                  const_mu_KP             = const_mu_KP,
                  const_mu_KP_margin      = const_mu_KP_margin,
                  kernels_with_bottleneck = kernels_with_bottleneck,
                  saturation_margin       = saturation_margin,
                  min_throughput          = min_throughput,)

  ilp.set_objective(
    GRB.MAXIMIZE,
    ## maximize throughput
    Σ(ilp.rho_opt[K] + ilp.umu_opt[K] + ilp.fmu_opt[K] for K in KERNELS),
    # Σ(ilp.saturated_KM[K, M] for K, M in KERNELS * MUOPS),
  )

  log('--- END GEN MODEL', datetime.datetime.now(), '-------------------------')

  log('--- BGN SOLVE', datetime.datetime.now(), '-----------------------------')
  try:
    ilp.optimize(print_iis=print_iis)
  except ILP_Error as e:
    raise e
  finally:
    log('--- END SOLVE', datetime.datetime.now(), '---------------------------')

  return model_to_outputs(ilp)


def main(argv=None):
  hardcoded_cpu()
  # measured_cpu()
  pass


def measured_cpu():
  μI1   = Inst(name='μI1', num_muops=1)  # ADCX_GPR32i32_GPR32i32 ADCX_GPR64i64_GPR64i64 ADC_GPR32i32_GPR32i32 ADC_GPR32i32_IMMi32 ADC_GPR64i64_GPR64i64 ADC_GPR64i64_IMMi32 SBB_GPR32i32_GPR32i32 SBB_GPR32i32_IMMi32 SBB_GPR64i64_GPR64i64 SBB_GPR64i64_IMMi32
  μI2   = Inst(name='μI2', num_muops=1)  # ADDPD_VR128f64x2_VR128f64x2 ADDPS_VR128f32x4_VR128f32x4 ADDSD_VR128f64_VR128f64 ADDSS_VR128f32_VR128f32 ADDSUBPD_VR128f64x2_VR128f64x2 ADDSUBPS_VR128f32x4_VR128f32x4 CMPPD_VR128f64x2_VR128f64x2_IMMu8 CMPPS_VR128f32x4_VR128f32x4_IMMu8 CMPSD_XMM_VR128f64_VR128f64_IMMu8 CMPSS_VR128f32_VR128f32_IMMu8 CVTDQ2PS_VR128f32x4_VR128i32x4 CVTPS2DQ_VR128i32x4_VR128f32x4 CVTTPS2DQ_VR128i32x4_VR128f32x4 MAXPD_VR128f64x2_VR128f64x2 MAXPS_VR128f32x4_VR128f32x4 MAXSD_VR128f64_VR128f64 MAXSS_VR128f32_VR128f32 MINPD_VR128f64x2_VR128f64x2 MINPS_VR128f32x4_VR128f32x4 MINSD_VR128f64_VR128f64 MINSS_VR128f32_VR128f32 MULPD_VR128f64x2_VR128f64x2 MULPS_VR128f32x4_VR128f32x4 MULSD_VR128f64_VR128f64 MULSS_VR128f32_VR128f32 PABSB_VR128i32x4_VR128i32x4_SSSE3 PABSB_VR128i32x4_VR128i32x4_SSSE3MMX PABSD_VR128i32x4_VR128i32x4_SSSE3 PABSD_VR128i32x4_VR128i32x4_SSSE3MMX PABSW_VR128i32x4_VR128i32x4_SSSE3 PABSW_VR128i32x4_VR128i32x4_SSSE3MMX PADDSB_VR128i32x4_VR128i32x4_PENTIUMMMX PADDSB_VR128i32x4_VR128i32x4_SSE2 PADDSW_VR128i32x4_VR128i32x4_PENTIUMMMX PADDSW_VR128i32x4_VR128i32x4_SSE2 PADDUSB_VR128i32x4_VR128i32x4_PENTIUMMMX PADDUSB_VR128i32x4_VR128i32x4_SSE2 PADDUSW_VR128i32x4_VR128i32x4_PENTIUMMMX PADDUSW_VR128i32x4_VR128i32x4_SSE2 PAVGB_VR128u8x16_VR128u8x16_PENTIUMMMX PAVGB_VR128u8x16_VR128u8x16_SSE2 PAVGW_VR128u16x8_VR128u16x8_PENTIUMMMX PAVGW_VR128u16x8_VR128u16x8_SSE2 PCMPEQB_VR128i8x16_VR128i8x16_PENTIUMMMX PCMPEQB_VR128i8x16_VR128i8x16_SSE2 PCMPEQD_VR128i32x4_VR128i32x4_PENTIUMMMX PCMPEQD_VR128i32x4_VR128i32x4_SSE2 PCMPEQQ_VR128i32x4_VR128i32x4 PCMPEQW_VR128i16x8_VR128i16x8_PENTIUMMMX PCMPEQW_VR128i16x8_VR128i16x8_SSE2 PCMPGTB_VR128i8x16_VR128i8x16_PENTIUMMMX PCMPGTB_VR128i8x16_VR128i8x16_SSE2 PCMPGTD_VR128i32x4_VR128i32x4_PENTIUMMMX PCMPGTD_VR128i32x4_VR128i32x4_SSE2 PCMPGTW_VR128i16x8_VR128i16x8_PENTIUMMMX PCMPGTW_VR128i16x8_VR128i16x8_SSE2 PMADDUBSW_VR128i8x16_VR128i8x16_SSSE3 PMADDUBSW_VR128i8x16_VR128i8x16_SSSE3MMX PMADDWD_VR128i16x8_VR128i16x8_PENTIUMMMX PMADDWD_VR128i16x8_VR128i16x8_SSE2 PMAXSB_VR128i32x4_VR128i32x4 PMAXSD_VR128i32x4_VR128i32x4 PMAXSW_VR128i32x4_VR128i32x4_PENTIUMMMX PMAXSW_VR128i32x4_VR128i32x4_SSE2 PMAXUB_VR128i32x4_VR128i32x4_PENTIUMMMX PMAXUB_VR128i32x4_VR128i32x4_SSE2 PMAXUD_VR128i32x4_VR128i32x4 PMAXUW_VR128i32x4_VR128i32x4 PMINSB_VR128i32x4_VR128i32x4 PMINSD_VR128i32x4_VR128i32x4 PMINSW_VR128i32x4_VR128i32x4_PENTIUMMMX PMINSW_VR128i32x4_VR128i32x4_SSE2 PMINUB_VR128i32x4_VR128i32x4_PENTIUMMMX PMINUB_VR128i32x4_VR128i32x4_SSE2 PMINUD_VR128i32x4_VR128i32x4 PMINUW_VR128i32x4_VR128i32x4 PMULDQ_VR128i32x4_VR128i32x4 PMULHRSW_VR128i32x4_VR128i32x4_SSSE3 PMULHRSW_VR128i32x4_VR128i32x4_SSSE3MMX PMULHUW_VR128u16x8_VR128u16x8_PENTIUMMMX PMULHUW_VR128u16x8_VR128u16x8_SSE2 PMULHW_VR128i16x8_VR128i16x8_PENTIUMMMX PMULHW_VR128i16x8_VR128i16x8_SSE2 PMULLW_VR128i32x4_VR128i32x4_PENTIUMMMX PMULLW_VR128i32x4_VR128i32x4_SSE2 PMULUDQ_VR128i32x4_VR128i32x4_SSE2 PMULUDQ_VR128i32x4_VR128i32x4_SSE2MMX PSIGNB_VR128i32x4_VR128i32x4_SSSE3 PSIGNB_VR128i32x4_VR128i32x4_SSSE3MMX PSIGND_VR128i32x4_VR128i32x4_SSSE3 PSIGND_VR128i32x4_VR128i32x4_SSSE3MMX PSIGNW_VR128i32x4_VR128i32x4_SSSE3 PSIGNW_VR128i32x4_VR128i32x4_SSSE3MMX PSLLD_VR128u32x4_IMMu8_PENTIUMMMX PSLLD_VR128u32x4_IMMu8_SSE2 PSLLQ_VR128u64x2_IMMu8_PENTIUMMMX PSLLQ_VR128u64x2_IMMu8_SSE2 PSLLW_VR128u16x8_IMMu8_PENTIUMMMX PSLLW_VR128u16x8_IMMu8_SSE2 PSRAD_VR128i32x4_IMMu8_PENTIUMMMX PSRAD_VR128i32x4_IMMu8_SSE2 PSRAW_VR128i16x8_IMMu8_PENTIUMMMX PSRAW_VR128i16x8_IMMu8_SSE2 PSRLD_VR128u32x4_IMMu8_PENTIUMMMX PSRLD_VR128u32x4_IMMu8_SSE2 PSRLQ_VR128u64x2_IMMu8_PENTIUMMMX PSRLQ_VR128u64x2_IMMu8_SSE2 PSRLW_VR128u16x8_IMMu8_PENTIUMMMX PSRLW_VR128u16x8_IMMu8_SSE2 PSUBSB_VR128i32x4_VR128i32x4_PENTIUMMMX PSUBSB_VR128i32x4_VR128i32x4_SSE2 PSUBSW_VR128i32x4_VR128i32x4_PENTIUMMMX PSUBSW_VR128i32x4_VR128i32x4_SSE2 PSUBUSB_VR128i32x4_VR128i32x4_PENTIUMMMX PSUBUSB_VR128i32x4_VR128i32x4_SSE2 PSUBUSW_VR128i32x4_VR128i32x4_PENTIUMMMX PSUBUSW_VR128i32x4_VR128i32x4_SSE2 SUBPD_VR128f64x2_VR128f64x2 SUBPS_VR128f32x4_VR128f32x4 SUBSD_VR128f64_VR128f64 SUBSS_VR128f32_VR128f32
  μI3   = Inst(name='μI3', num_muops=1)  # ADD_GPR32i32_GPR32i32 ADD_GPR32i32_IMMi32 ADD_GPR64i64_GPR64i64 ADD_GPR64i64_IMMi32 AND_GPR32i32_GPR32i32 AND_GPR32i32_IMMi32 AND_GPR64i64_GPR64i64 AND_GPR64i64_IMMi32 CMP_EAXi32_IMMi32 CMP_GPR32i32_GPR32i32 CMP_GPR32i32_IMMi32 CMP_GPR64i64_GPR64i64 CMP_GPR64i64_IMMi32 CMP_RAXi64_IMMi32 DEC_GPR32i32 DEC_GPR64i64 INC_GPR32i32 INC_GPR64i64 MOVSXD_GPR64i64_GPR32i32 MOV_GPR32i32_IMMi32 MOV_GPR64i64_IMMi32 NEG_GPR32i32 NEG_GPR64i64 NOT_GPR32i32 NOT_GPR64i64 OR_GPR32i32_GPR32i32 OR_GPR32i32_IMMi32 OR_GPR64i64_GPR64i64 OR_GPR64i64_IMMi32 SUB_GPR32i32_GPR32i32 SUB_GPR32i32_IMMi32 SUB_GPR64i64_GPR64i64 SUB_GPR64i64_IMMi32 TEST_EAXi32_IMMi32 TEST_GPR32i32_GPR32i32 TEST_GPR32i32_IMMi32 TEST_GPR64i64_GPR64i64 TEST_GPR64i64_IMMi32 TEST_RAXi64_IMMi32 XOR_GPR32i32_GPR32i32 XOR_GPR32i32_IMMi32 XOR_GPR64i64_GPR64i64 XOR_GPR64i64_IMMi32
  μI4   = Inst(name='μI4', num_muops=1)  # ADOX_GPR32i32_GPR32i32 ADOX_GPR64i64_GPR64i64
  μI5   = Inst(name='μI5', num_muops=1)  # AESDECLAST_VR128i32x4_VR128i32x4 AESDEC_VR128i32x4_VR128i32x4 AESENCLAST_VR128i32x4_VR128i32x4 AESENC_VR128i32x4_VR128i32x4 COMISD_VR128f64_VR128f64 COMISS_VR128f32_VR128f32 MOVD_GPR32i32_VR128i32 MOVMSKPD_GPR32i32_VR128f64x2 MOVMSKPS_GPR32i32_VR128f32x4 MOVQ_GPR64i64_VR128i64 PHMINPOSUW_VR128i32x4_VR128i32x4 PMOVMSKB_GPR32i32_VR128i8x16_SSE PMOVMSKB_GPR32i32_VR128i8x16_SSE2 RCPPS_VR128f32x4_VR128f32x4 RCPSS_VR128f32_VR128f32 RSQRTPS_VR128f32x4_VR128f32x4 RSQRTSS_VR128f32_VR128f32 UCOMISD_VR128f64_VR128f64 UCOMISS_VR128f32_VR128f32
  μI6   = Inst(name='μI6', num_muops=1)  # ANDNPD_VR128u64x2_VR128u64x2 ANDNPS_VR128u32x4_VR128u32x4 ANDPD_VR128u64x2_VR128u64x2 ANDPS_VR128u32x4_VR128u32x4 BLENDPD_VR128f64x2_VR128f64x2_IMMu8 BLENDPS_VR128f32x4_VR128f32x4_IMMu8 MOVQ_VR128i32x4_VR128i64 ORPD_VR128u64x2_VR128u64x2 ORPS_VR128u32x4_VR128u32x4 PADDB_VR128i32x4_VR128i32x4_PENTIUMMMX PADDB_VR128i32x4_VR128i32x4_SSE2 PADDD_VR128i32x4_VR128i32x4_PENTIUMMMX PADDD_VR128i32x4_VR128i32x4_SSE2 PADDQ_VR128i32x4_VR128i32x4_SSE2 PADDQ_VR128i32x4_VR128i32x4_SSE2MMX PADDW_VR128i16x8_VR128i16x8_PENTIUMMMX PADDW_VR128i16x8_VR128i16x8_SSE2 PANDN_VR128i32x4_VR128i32x4_PENTIUMMMX PANDN_VR128i32x4_VR128i32x4_SSE2 PAND_VR128i32x4_VR128i32x4_PENTIUMMMX PAND_VR128i32x4_VR128i32x4_SSE2 POR_VR128i32x4_VR128i32x4_PENTIUMMMX POR_VR128i32x4_VR128i32x4_SSE2 PSUBB_VR128i32x4_VR128i32x4_PENTIUMMMX PSUBB_VR128i32x4_VR128i32x4_SSE2 PSUBD_VR128i32x4_VR128i32x4_PENTIUMMMX PSUBD_VR128i32x4_VR128i32x4_SSE2 PSUBQ_VR128i32x4_VR128i32x4_SSE2 PSUBQ_VR128i32x4_VR128i32x4_SSE2MMX PSUBW_VR128i32x4_VR128i32x4_PENTIUMMMX PSUBW_VR128i32x4_VR128i32x4_SSE2 PXOR_VR128i32x4_VR128i32x4_PENTIUMMMX PXOR_VR128i32x4_VR128i32x4_SSE2 XORPD_VR128u64x2_VR128u64x2 XORPS_VR128u32x4_VR128u32x4
  μI7   = Inst(name='μI7', num_muops=1)  # ANDN_GPR32i32_GPR32i32_GPR32i32 ANDN_GPR64i64_GPR64i64_GPR64i64 BLSI_GPR32i32_GPR32i32 BLSI_GPR64i64_GPR64i64 BLSMSK_GPR32i32_GPR32i32 BLSMSK_GPR64i64_GPR64i64 BLSR_GPR32i32_GPR32i32 BLSR_GPR64i64_GPR64i64 BSWAP_GPR32i32 BZHI_GPR32i32_GPR32i32_GPR32i32 BZHI_GPR64i64_GPR64i64_GPR64i64 LEA_GPR32i32_ADDR64i64 LEA_GPR64i64_ADDR64i64
  μI8   = Inst(name='μI8', num_muops=1)  # BLENDVPD_VR128f64x2_VR128f64x2 BLENDVPS_VR128f32x4_VR128f32x4 PBLENDVB_VR128i32x4_VR128i32x4
  μI9   = Inst(name='μI9', num_muops=1)  # BSF_GPR32i32_GPR32i32 BSF_GPR64i64_GPR64i64 BSR_GPR32i32_GPR32i32 BSR_GPR64i64_GPR64i64 CRC32_GPR32i32_GPR32i32 CRC32_GPR64i64_GPR64i64 IMUL_GPR32i32_GPR32i32 IMUL_GPR32i32_GPR32i32_IMMi32 IMUL_GPR64i64_GPR64i64 IMUL_GPR64i64_GPR64i64_IMMi32 LZCNT_GPR32i32_GPR32i32 LZCNT_GPR64i64_GPR64i64 PDEP_GPR32i32_GPR32i32_GPR32i32 PDEP_GPR64i64_GPR64i64_GPR64i64 PEXT_GPR32i32_GPR32i32_GPR32i32 PEXT_GPR64i64_GPR64i64_GPR64i64 POPCNT_GPR32i32_GPR32i32 POPCNT_GPR64i64_GPR64i64 SHLD_GPR32i32_GPR32i32_IMMu8 SHLD_GPR64i64_GPR64i64_IMMu8 SHRD_GPR32i32_GPR32i32_IMMu8 SHRD_GPR64i64_GPR64i64_IMMu8 TZCNT_GPR32i32_GPR32i32 TZCNT_GPR64i64_GPR64i64
  μI10  = Inst(name='μI10', num_muops=1)  # BTC_GPR32i32_GPR32i32 BTC_GPR32i32_IMMu8 BTC_GPR64i64_GPR64i64 BTC_GPR64i64_IMMu8 BTR_GPR32i32_GPR32i32 BTR_GPR32i32_IMMu8 BTR_GPR64i64_GPR64i64 BTR_GPR64i64_IMMu8 BTS_GPR32i32_GPR32i32 BTS_GPR32i32_IMMu8 BTS_GPR64i64_GPR64i64 BTS_GPR64i64_IMMu8 BT_GPR32i32_GPR32i32 BT_GPR32i32_IMMu8 BT_GPR64i64_GPR64i64 BT_GPR64i64_IMMu8 CMOVB_GPR32i32_GPR32i32 CMOVB_GPR64i64_GPR64i64 CMOVLE_GPR32i32_GPR32i32 CMOVLE_GPR64i64_GPR64i64 CMOVL_GPR32i32_GPR32i32 CMOVL_GPR64i64_GPR64i64 CMOVNB_GPR32i32_GPR32i32 CMOVNB_GPR64i64_GPR64i64 CMOVNLE_GPR32i32_GPR32i32 CMOVNLE_GPR64i64_GPR64i64 CMOVNL_GPR32i32_GPR32i32 CMOVNL_GPR64i64_GPR64i64 CMOVNO_GPR32i32_GPR32i32 CMOVNO_GPR64i64_GPR64i64 CMOVNP_GPR32i32_GPR32i32 CMOVNP_GPR64i64_GPR64i64 CMOVNS_GPR32i32_GPR32i32 CMOVNS_GPR64i64_GPR64i64 CMOVNZ_GPR32i32_GPR32i32 CMOVNZ_GPR64i64_GPR64i64 CMOVO_GPR32i32_GPR32i32 CMOVO_GPR64i64_GPR64i64 CMOVP_GPR32i32_GPR32i32 CMOVP_GPR64i64_GPR64i64 CMOVS_GPR32i32_GPR32i32 CMOVS_GPR64i64_GPR64i64 CMOVZ_GPR32i32_GPR32i32 CMOVZ_GPR64i64_GPR64i64 ROL_GPR32i32_IMMu8_FLAGS_I186 ROL_GPR32i32_IMMu8_FLAGS_I86 ROL_GPR64i64_IMMu8_FLAGS_I186 ROL_GPR64i64_IMMu8_FLAGS_I86 RORX_GPR32i32_GPR32i32_IMMu8 RORX_GPR64i64_GPR64i64_IMMu8 ROR_GPR32i32_IMMu8_FLAGS_I186 ROR_GPR32i32_IMMu8_FLAGS_I86 ROR_GPR64i64_IMMu8_FLAGS_I186 ROR_GPR64i64_IMMu8_FLAGS_I86 SARX_GPR32i32_GPR32i32_GPR32i32 SARX_GPR64i64_GPR64i64_GPR64i64 SAR_GPR32i32_IMMu8_FLAGS_I186 SAR_GPR32i32_IMMu8_FLAGS_I86 SAR_GPR64i64_IMMu8_FLAGS_I186 SAR_GPR64i64_IMMu8_FLAGS_I86 SHLX_GPR32i32_GPR32i32_GPR32i32 SHLX_GPR64i64_GPR64i64_GPR64i64 SHL_GPR32i32_IMMu8_FLAGS_I186 SHL_GPR32i32_IMMu8_FLAGS_I86 SHL_GPR64i64_IMMu8_FLAGS_I186 SHL_GPR64i64_IMMu8_FLAGS_I86 SHRX_GPR32i32_GPR32i32_GPR32i32 SHRX_GPR64i64_GPR64i64_GPR64i64 SHR_GPR32i32_IMMu8_FLAGS_I186 SHR_GPR32i32_IMMu8_FLAGS_I86 SHR_GPR64i64_IMMu8_FLAGS_I186 SHR_GPR64i64_IMMu8_FLAGS_I86
  μI11  = Inst(name='μI11', num_muops=1)  # DIVPD_VR128f64x2_VR128f64x2 DIVPS_VR128f32x4_VR128f32x4 DIVSD_VR128f64_VR128f64 DIVSS_VR128f32_VR128f32 SQRTPD_VR128f64x2_VR128f64x2 SQRTPS_VR128f32x4_VR128f32x4 SQRTSD_VR128f64_VR128f64 SQRTSS_VR128f32_VR128f32
  μI12  = Inst(name='μI12', num_muops=1)  # INSERTPS_VR128f32x4_VR128f32x4_IMMu8 MOVDDUP_VR128i32x4_VR128i64 MOVD_VR128i32x4_GPR32i32 MOVHLPS_VR128f32x2_VR128f32x2 MOVLHPS_VR128f32x2_VR128f32x2 MOVQ_VR128i32x4_GPR64i64 MOVSD_XMM_VR128f64_VR128f64 MOVSHDUP_VR128f32x4_VR128f32x4 MOVSLDUP_VR128f32x4_VR128f32x4 MOVSS_VR128f32_VR128f32 PACKSSDW_VR128i32x4_VR128i32x4_PENTIUMMMX PACKSSDW_VR128i32x4_VR128i32x4_SSE2 PACKSSWB_VR128i16x8_VR128i16x8_PENTIUMMMX PACKSSWB_VR128i16x8_VR128i16x8_SSE2 PACKUSDW_VR128i32x4_VR128i32x4 PACKUSWB_VR128i16x8_VR128i16x8_PENTIUMMMX PACKUSWB_VR128i16x8_VR128i16x8_SSE2 PALIGNR_VR128i32x4_VR128i32x4_IMMu8_SSSE3 PALIGNR_VR128i32x4_VR128i32x4_IMMu8_SSSE3MMX PBLENDW_VR128i32x4_VR128i32x4_IMMu8 PCLMULQDQ_VR128i32x4_VR128i32x4_IMMu8 PCMPGTQ_VR128i32x4_VR128i32x4 PMOVSXBD_VR128i32x4_VR128i8x4 PMOVSXBQ_VR128i64x2_VR128i8x2 PMOVSXBW_VR128i16x8_VR128i8x8 PMOVSXDQ_VR128i64x2_VR128i32x2 PMOVSXWD_VR128i32x4_VR128i16x4 PMOVSXWQ_VR128i64x2_VR128i16x2 PMOVZXBD_VR128u32x4_VR128u8x4 PMOVZXBQ_VR128u64x2_VR128u8x2 PMOVZXBW_VR128u16x8_VR128u8x8 PMOVZXDQ_VR128u64x2_VR128u32x2 PMOVZXWD_VR128u32x4_VR128u16x4 PMOVZXWQ_VR128u64x2_VR128u16x2 PSADBW_VR128i32x4_VR128i32x4_PENTIUMMMX PSADBW_VR128i32x4_VR128i32x4_SSE2 PSHUFB_VR128i32x4_VR128i32x4_SSSE3 PSHUFB_VR128i32x4_VR128i32x4_SSSE3MMX PSHUFD_VR128u32x4_VR128u32x4_IMMu8 PSHUFHW_VR128u16x8_VR128u16x8_IMMu8 PSHUFLW_VR128u16x8_VR128u16x8_IMMu8 PSLLDQ_VR128u128_IMMu8 PSRLDQ_VR128u128_IMMu8 PUNPCKHBW_VR128i32x4_VR128i64_PENTIUMMMX PUNPCKHBW_VR128i32x4_VR128i64_SSE2 PUNPCKHDQ_VR128i32x4_VR128i64_PENTIUMMMX PUNPCKHDQ_VR128i32x4_VR128i64_SSE2 PUNPCKHQDQ_VR128i32x4_VR128i64 PUNPCKHWD_VR128i32x4_VR128i64_PENTIUMMMX PUNPCKHWD_VR128i32x4_VR128i64_SSE2 PUNPCKLBW_VR128i32x4_VR128i64_PENTIUMMMX PUNPCKLBW_VR128i32x4_VR128i64_SSE2 PUNPCKLDQ_VR128i32x4_VR128i64_PENTIUMMMX PUNPCKLDQ_VR128i32x4_VR128i64_SSE2 PUNPCKLQDQ_VR128i32x4_VR128i64 PUNPCKLWD_VR128i32x4_VR128i64_PENTIUMMMX PUNPCKLWD_VR128i32x4_VR128i64_SSE2 SHUFPD_VR128f64x2_VR128f64x2_IMMu8 SHUFPS_VR128f32x4_VR128f32x4_IMMu8 UNPCKHPD_VR128f64x2_VR128i64 UNPCKHPS_VR128f32x4_VR128i32x4 UNPCKLPD_VR128f64x2_VR128i64 UNPCKLPS_VR128f32x4_VR128i64
  μI13  = Inst(name='μI13', num_muops=1)  # MOVAPD_VR128f64x2_VR128f64x2 MOVAPS_VR128f32x4_VR128f32x4 MOVDQA_VR128i32x4_VR128i32x4 MOVDQU_VR128i32x4_VR128i32x4 MOVUPD_VR128f64x2_VR128f64x2 MOVUPS_VR128f32x4_VR128f32x4
  μI14  = Inst(name='μI14', num_muops=1)  # MOV_GPR32i32_GPR32i32 MOV_GPR64i64_GPR64i64
  μI15  = Inst(name='μI15', num_muops=1)  # MOV_GPR64i64_IMMi64

  μ1 = Muop("μ1")
  μ2 = Muop("μ2")
  μ3 = Muop("μ3")
  μ4 = Muop("μ4")
  μ5 = Muop("μ5")
  μ6 = Muop("μ6")
  μ7 = Muop("μ7")
  μ8 = Muop("μ8")
  μ9 = Muop("μ9")
  μ10 = Muop("μ10")
  μ11 = Muop("μ11")
  μ12 = Muop("μ12")
  μ13 = Muop("μ13")
  μ14 = Muop("μ14")
  μ15 = Muop("μ15")

  INSTS = Domain(Inst, [
    μI1,
    μI2,
    μI3,
    μI4,
    μI5,
    μI6,
    μI7,
    μI8,
    μI9,
    μI10,
    μI11,
    μI12,
    μI13,
    μI14,
    μI15,
  ])

  MUOPS = Domain(Muop, [
    μ1,
    μ2,
    μ3,
    μ4,
    μ5,
    μ6,
    μ7,
    μ8,
    μ9,
    μ10,
    μ11,
    μ12,
    μ13,
    μ14,
    μ15,
  ])

  K_μI1 = Kernel(ipc=1.011073617896248, mpc=1.011073617896248, insts=[μI1])
  K_μI2 = Kernel(ipc=1.9992073557594234, mpc=1.9992073557594234, insts=[μI2])
  K_μI3 = Kernel(ipc=3.9782494170884446, mpc=3.9782494170884446, insts=[μI3])
  K_μI4 = Kernel(ipc=1.014971937992284, mpc=1.014971937992284, insts=[μI4])
  K_μI5 = Kernel(ipc=0.9993059390381804, mpc=0.9993059390381804, insts=[μI5])
  K_μI6 = Kernel(ipc=2.999282849787905, mpc=2.999282849787905, insts=[μI6])
  K_μI7 = Kernel(ipc=1.9992354017423286, mpc=1.9992354017423286, insts=[μI7])
  # K_μI8 = Kernel(ipc=2.452286805482214, mpc=2.452286805482214, insts=[μI8])
  K_μI8 = Kernel(ipc=2, mpc=2, insts=[μI8])
  K_μI9 = Kernel(ipc=0.9994715477019017, mpc=0.9994715477019017, insts=[μI9])
  K_μI10 = Kernel(ipc=1.982963018842018, mpc=1.982963018842018, insts=[μI10])
  K_μI11 = Kernel(ipc=0.2844692374683093, mpc=0.2844692374683093, insts=[μI11])
  K_μI12 = Kernel(ipc=0.999382576036973, mpc=0.999382576036973, insts=[μI12])
  K_μI13 = Kernel(ipc=3.9786944181396158, mpc=3.9786944181396158, insts=[μI13])
  K_μI14 = Kernel(ipc=3.9784012052960405, mpc=3.9784012052960405, insts=[μI14])
  K_μI15 = Kernel(ipc=0.999579199375336, mpc=0.999579199375336, insts=[μI15])

  K_μI1_μI2 = Kernel(insts=[μI1, μI2], ipc=2.0581827407184576, mpc=2.0581827407184576)
# warning: μI1 μI3 stddev of aggregate IPC to high: 0.7725682838883692
# warning: μI1 μI3 stddev of aggregate IPC to high: 0.7725682838883692
  K_μI1_μI3 = Kernel(insts=[μI1, μI3], ipc=3.330756241776384, mpc=3.330756241776384)
# warning: μI1 μI4 stddev of aggregate IPC to high: 0.12158207101168632
# warning: μI1 μI4 stddev of aggregate IPC to high: 0.12158207101168632
  K_μI1_μI4 = Kernel(insts=[μI1, μI4], ipc=1.8801462530776403, mpc=1.8801462530776403)
# warning: μI1 μI5 stddev of aggregate IPC to high: 0.3037726677367651
# warning: μI1 μI5 stddev of aggregate IPC to high: 0.3037726677367651
  K_μI1_μI5 = Kernel(insts=[μI1, μI5], ipc=1.6423664823317412, mpc=1.6423664823317412)
  K_μI1_μI6 = Kernel(insts=[μI1, μI6], ipc=2.0580113035305874, mpc=2.0580113035305874)
# warning: μI1 μI7 stddev of aggregate IPC to high: 0.7552775902395003
# warning: μI1 μI7 stddev of aggregate IPC to high: 0.7552775902395003
  K_μI1_μI7 = Kernel(insts=[μI1, μI7], ipc=3.454032457650719, mpc=3.454032457650719)
  K_μI1_μI8 = Kernel(insts=[μI1, μI8], ipc=2.0521382952602507, mpc=2.0521382952602507)
  K_μI1_μI9 = Kernel(insts=[μI1, μI9], ipc=1.9919351038069022, mpc=1.9919351038069022)
  K_μI1_μI10 = Kernel(insts=[μI1, μI10], ipc=1.92548637049766, mpc=1.92548637049766)
  K_μI1_μI11 = Kernel(insts=[μI1, μI11], ipc=0.5720108984783295, mpc=0.5720108984783295)
  K_μI1_μI12 = Kernel(insts=[μI1, μI12], ipc=1.9960398171363534, mpc=1.9960398171363534)
  K_μI1_μI13 = Kernel(insts=[μI1, μI13], ipc=2.0526001493215285, mpc=2.0526001493215285)
  K_μI1_μI14 = Kernel(insts=[μI1, μI14], ipc=2.0696178143486343, mpc=2.0696178143486343)
  K_μI1_μI15 = Kernel(insts=[μI1, μI15], ipc=1.9860429316863004, mpc=1.9860429316863004)
  K_μI2_μI3 = Kernel(insts=[μI2, μI3], ipc=3.978474179339727, mpc=3.978474179339727)
  K_μI2_μI4 = Kernel(insts=[μI2, μI4], ipc=2.0794792988306905, mpc=2.0794792988306905)
  K_μI2_μI5 = Kernel(insts=[μI2, μI5], ipc=1.9866314209375033, mpc=1.9866314209375033)
  K_μI2_μI6 = Kernel(insts=[μI2, μI6], ipc=2.919268536851742, mpc=2.919268536851742)
  K_μI2_μI7 = Kernel(insts=[μI2, μI7], ipc=2.9968680289831524, mpc=2.9968680289831524)
# warning: μI2 μI8 stddev of aggregate IPC to high: 0.16357794653007013
# warning: μI2 μI8 stddev of aggregate IPC to high: 0.16357794653007013
  K_μI2_μI8 = Kernel(insts=[μI2, μI8], ipc=2.535285911200978, mpc=2.535285911200978)
  K_μI2_μI9 = Kernel(insts=[μI2, μI9], ipc=1.9959163196812968, mpc=1.9959163196812968)
  K_μI3_μI4 = Kernel(insts=[μI3, μI4], ipc=3.843341865813188, mpc=3.843341865813188)
  K_μI3_μI5 = Kernel(insts=[μI3, μI5], ipc=1.995876735028046, mpc=1.995876735028046)
  K_μI3_μI6 = Kernel(insts=[μI3, μI6], ipc=3.97848896449242, mpc=3.97848896449242)
  K_μI3_μI7 = Kernel(insts=[μI3, μI7], ipc=3.9030435939990324, mpc=3.9030435939990324)
  K_μI3_μI8 = Kernel(insts=[μI3, μI8], ipc=3.8183902333352555, mpc=3.8183902333352555)
  K_μI3_μI9 = Kernel(insts=[μI3, μI9], ipc=1.9956926315118815, mpc=1.9956926315118815)
# warning: μI4 μI5 stddev of aggregate IPC to high: 0.3095068443468208
# warning: μI4 μI5 stddev of aggregate IPC to high: 0.3095068443468208
  K_μI4_μI5 = Kernel(insts=[μI4, μI5], ipc=1.639826133776254, mpc=1.639826133776254)
  K_μI4_μI6 = Kernel(insts=[μI4, μI6], ipc=2.079578827625724, mpc=2.079578827625724)
# warning: μI4 μI7 stddev of aggregate IPC to high: 0.8046061593752912
# warning: μI4 μI7 stddev of aggregate IPC to high: 0.8046061593752912
  K_μI4_μI7 = Kernel(insts=[μI4, μI7], ipc=3.510168360024271, mpc=3.510168360024271)
  K_μI4_μI8 = Kernel(insts=[μI4, μI8], ipc=2.080786632721921, mpc=2.080786632721921)
  K_μI4_μI9 = Kernel(insts=[μI4, μI9], ipc=1.9956666045436162, mpc=1.9956666045436162)
  K_μI5_μI6 = Kernel(insts=[μI5, μI6], ipc=1.9962132571799378, mpc=1.9962132571799378)
  K_μI5_μI7 = Kernel(insts=[μI5, μI7], ipc=1.9964434879025883, mpc=1.9964434879025883)
  K_μI5_μI8 = Kernel(insts=[μI5, μI8], ipc=1.996960442775948, mpc=1.996960442775948)
  K_μI5_μI9 = Kernel(insts=[μI5, μI9], ipc=1.9958388231651596, mpc=1.9958388231651596)
  K_μI6_μI7 = Kernel(insts=[μI6, μI7], ipc=2.9971923829651943, mpc=2.9971923829651943)
  K_μI6_μI8 = Kernel(insts=[μI6, μI8], ipc=2.944507520010041, mpc=2.944507520010041)
  K_μI6_μI9 = Kernel(insts=[μI6, μI9], ipc=1.995977115059086, mpc=1.995977115059086)
  K_μI7_μI8 = Kernel(insts=[μI7, μI8], ipc=2.8625826586876926, mpc=2.8625826586876926)
  K_μI7_μI9 = Kernel(insts=[μI7, μI9], ipc=1.964219172432416, mpc=1.964219172432416)
  K_μI8_μI9 = Kernel(insts=[μI8, μI9], ipc=1.9958779554583268, mpc=1.9958779554583268)
  K_μI10_μI2 = Kernel(insts=[μI10, μI2], ipc=2.9808799454930934, mpc=2.9808799454930934)
# warning: μI10 μI3 stddev of aggregate IPC to high: 0.22925884243589323
# warning: μI10 μI3 stddev of aggregate IPC to high: 0.22925884243589323
  K_μI10_μI3 = Kernel(insts=[μI10, μI3], ipc=3.8173910792456835, mpc=3.8173910792456835)
# warning: μI10 μI5 stddev of aggregate IPC to high: 0.18208974584710153
# warning: μI10 μI5 stddev of aggregate IPC to high: 0.18208974584710153
  K_μI10_μI5 = Kernel(insts=[μI10, μI5], ipc=1.7410779082754027, mpc=1.7410779082754027)
  K_μI10_μI6 = Kernel(insts=[μI10, μI6], ipc=3.9396573692759786, mpc=3.9396573692759786)
  K_μI10_μI7 = Kernel(insts=[μI10, μI7], ipc=3.9308986432722026, mpc=3.9308986432722026)
  K_μI10_μI8 = Kernel(insts=[μI10, μI8], ipc=3.714967155702065, mpc=3.714967155702065)
  K_μI10_μI9 = Kernel(insts=[μI10, μI9], ipc=1.9950948365143644, mpc=1.9950948365143644)
  K_μI10_μI11 = Kernel(insts=[μI10, μI11], ipc=0.5786138692299531, mpc=0.5786138692299531)
  K_μI10_μI12 = Kernel(insts=[μI10, μI12], ipc=1.9957203481007348, mpc=1.9957203481007348)
  K_μI10_μI13 = Kernel(insts=[μI10, μI13], ipc=3.940548812107195, mpc=3.940548812107195)
  K_μI10_μI14 = Kernel(insts=[μI10, μI14], ipc=3.9269339498132, mpc=3.9269339498132)
  K_μI10_μI15 = Kernel(insts=[μI10, μI15], ipc=1.9866946902209328, mpc=1.9866946902209328)
  K_μI11_μI2 = Kernel(insts=[μI11, μI2], ipc=0.577028810743341, mpc=0.577028810743341)
  K_μI11_μI3 = Kernel(insts=[μI11, μI3], ipc=0.5674829380838081, mpc=0.5674829380838081)
  K_μI11_μI5 = Kernel(insts=[μI11, μI5], ipc=0.5707133576033036, mpc=0.5707133576033036)
  K_μI11_μI6 = Kernel(insts=[μI11, μI6], ipc=0.5806918887628683, mpc=0.5806918887628683)
  K_μI11_μI7 = Kernel(insts=[μI11, μI7], ipc=0.582215583391209, mpc=0.582215583391209)
  K_μI11_μI8 = Kernel(insts=[μI11, μI8], ipc=0.5821101316552815, mpc=0.5821101316552815)
  K_μI11_μI9 = Kernel(insts=[μI11, μI9], ipc=0.5734403703271604, mpc=0.5734403703271604)
# warning: μI11 μI12 stddev of aggregate IPC to high: 0.1268989278230408
# warning: μI11 μI12 stddev of aggregate IPC to high: 0.1268989278230408
  K_μI11_μI12 = Kernel(insts=[μI11, μI12], ipc=0.5588767582368053, mpc=0.5588767582368053)
  K_μI11_μI13 = Kernel(insts=[μI11, μI13], ipc=0.5823029971950232, mpc=0.5823029971950232)
  K_μI11_μI14 = Kernel(insts=[μI11, μI14], ipc=0.5821634626529743, mpc=0.5821634626529743)
  K_μI11_μI15 = Kernel(insts=[μI11, μI15], ipc=0.5824353358512515, mpc=0.5824353358512515)
  K_μI12_μI2 = Kernel(insts=[μI12, μI2], ipc=1.9959891020058487, mpc=1.9959891020058487)
  K_μI12_μI3 = Kernel(insts=[μI12, μI3], ipc=1.9961718148816094, mpc=1.9961718148816094)
  K_μI12_μI5 = Kernel(insts=[μI12, μI5], ipc=1.9958326673980622, mpc=1.9958326673980622)
  K_μI12_μI6 = Kernel(insts=[μI12, μI6], ipc=1.99609888270255, mpc=1.99609888270255)
  K_μI12_μI7 = Kernel(insts=[μI12, μI7], ipc=1.997543627090355, mpc=1.997543627090355)
  K_μI12_μI8 = Kernel(insts=[μI12, μI8], ipc=1.9957743285432223, mpc=1.9957743285432223)
  K_μI12_μI9 = Kernel(insts=[μI12, μI9], ipc=1.9954347802634982, mpc=1.9954347802634982)
  K_μI12_μI13 = Kernel(insts=[μI12, μI13], ipc=1.9964707601338103, mpc=1.9964707601338103)
  K_μI12_μI14 = Kernel(insts=[μI12, μI14], ipc=1.9961256986943332, mpc=1.9961256986943332)
  K_μI12_μI15 = Kernel(insts=[μI12, μI15], ipc=1.9875118980107436, mpc=1.9875118980107436)
  K_μI13_μI2 = Kernel(insts=[μI13, μI2], ipc=3.9770958560788072, mpc=3.9770958560788072)
  K_μI13_μI3 = Kernel(insts=[μI13, μI3], ipc=3.9784616626398033, mpc=3.9784616626398033)
  K_μI13_μI5 = Kernel(insts=[μI13, μI5], ipc=1.996266540286566, mpc=1.996266540286566)
  K_μI13_μI6 = Kernel(insts=[μI13, μI6], ipc=3.9784904174414875, mpc=3.9784904174414875)
  K_μI13_μI8 = Kernel(insts=[μI13, μI8], ipc=3.974255724512168, mpc=3.974255724512168)
  K_μI13_μI9 = Kernel(insts=[μI13, μI9], ipc=1.9958626591795472, mpc=1.9958626591795472)
  K_μI13_μI14 = Kernel(insts=[μI13, μI14], ipc=3.9786811500850097, mpc=3.9786811500850097)
  K_μI13_μI15 = Kernel(insts=[μI13, μI15], ipc=1.99072171574608, mpc=1.99072171574608)
  K_μI14_μI2 = Kernel(insts=[μI14, μI2], ipc=3.9774665930218607, mpc=3.9774665930218607)
  K_μI14_μI3 = Kernel(insts=[μI14, μI3], ipc=3.9783126486893807, mpc=3.9783126486893807)
  K_μI14_μI5 = Kernel(insts=[μI14, μI5], ipc=1.9961383263424703, mpc=1.9961383263424703)
  K_μI14_μI6 = Kernel(insts=[μI14, μI6], ipc=3.978715605319928, mpc=3.978715605319928)
  K_μI14_μI8 = Kernel(insts=[μI14, μI8], ipc=3.9787600920959854, mpc=3.9787600920959854)
  K_μI14_μI9 = Kernel(insts=[μI14, μI9], ipc=1.9958250438013883, mpc=1.9958250438013883)
  K_μI14_μI15 = Kernel(insts=[μI14, μI15], ipc=1.9979923292606698, mpc=1.9979923292606698)
  K_μI15_μI2 = Kernel(insts=[μI15, μI2], ipc=1.9863930085496933, mpc=1.9863930085496933)
  K_μI15_μI3 = Kernel(insts=[μI15, μI3], ipc=1.9888097044573423, mpc=1.9888097044573423)
  K_μI15_μI5 = Kernel(insts=[μI15, μI5], ipc=1.9875016344872765, mpc=1.9875016344872765)
  K_μI15_μI6 = Kernel(insts=[μI15, μI6], ipc=1.9870649417937247, mpc=1.9870649417937247)
  K_μI15_μI8 = Kernel(insts=[μI15, μI8], ipc=1.972165708917816, mpc=1.972165708917816)
  K_μI15_μI9 = Kernel(insts=[μI15, μI9], ipc=1.982244807989735, mpc=1.982244807989735)

  KERNELS = Domain(Kernel, [
    K_μI1,
    K_μI2,
    K_μI3,
    K_μI4,
    K_μI5,
    K_μI6,
    K_μI7,
    K_μI8,
    K_μI9,
    K_μI10,
    K_μI11,
    K_μI12,
    K_μI13,
    K_μI14,
    K_μI15,

    K_μI1_μI2,
    K_μI1_μI3,
    K_μI1_μI4,
    K_μI1_μI5,
    K_μI1_μI6,
    K_μI1_μI7,
    K_μI1_μI8,
    K_μI1_μI9,
    K_μI1_μI10,
    K_μI1_μI11,
    K_μI1_μI12,
    K_μI1_μI13,
    K_μI1_μI14,
    K_μI1_μI15,
    K_μI2_μI3,
    K_μI2_μI4,
    K_μI2_μI5,
    K_μI2_μI6,
    K_μI2_μI7,
    K_μI2_μI8,
    K_μI2_μI9,
    K_μI3_μI4,
    K_μI3_μI5,
    K_μI3_μI6,
    K_μI3_μI7,
    K_μI3_μI8,
    K_μI3_μI9,
    K_μI4_μI5,
    K_μI4_μI6,
    K_μI4_μI7,
    K_μI4_μI8,
    K_μI4_μI9,
    K_μI5_μI6,
    K_μI5_μI7,
    K_μI5_μI8,
    K_μI5_μI9,
    K_μI6_μI7,
    K_μI6_μI8,
    K_μI6_μI9,
    K_μI7_μI8,
    K_μI7_μI9,
    K_μI8_μI9,
    K_μI10_μI2,
    K_μI10_μI3,
    # K_μI10_μI4,
    K_μI10_μI5,
    K_μI10_μI6,
    K_μI10_μI7,
    K_μI10_μI8,
    K_μI10_μI9,
    K_μI10_μI11,
    K_μI10_μI12,
    K_μI10_μI13,
    K_μI10_μI14,
    K_μI10_μI15,
    K_μI11_μI2,
    K_μI11_μI3,
    # K_μI11_μI4,
    K_μI11_μI5,
    K_μI11_μI6,
    K_μI11_μI7,
    K_μI11_μI8,
    K_μI11_μI9,
    K_μI11_μI12,
    K_μI11_μI13,
    K_μI11_μI14,
    K_μI11_μI15,
    K_μI12_μI2,
    K_μI12_μI3,
    # K_μI12_μI4,
    K_μI12_μI5,
    K_μI12_μI6,
    K_μI12_μI7,
    K_μI12_μI8,
    K_μI12_μI9,
    K_μI12_μI13,
    K_μI12_μI14,
    K_μI12_μI15,
    K_μI13_μI2,
    K_μI13_μI3,
    # K_μI13_μI4,
    K_μI13_μI5,
    K_μI13_μI6,
    # K_μI13_μI7,
    K_μI13_μI8,
    K_μI13_μI9,
    K_μI13_μI14,
    K_μI13_μI15,
    K_μI14_μI2,
    K_μI14_μI3,
    # K_μI14_μI4,
    K_μI14_μI5,
    K_μI14_μI6,
    # K_μI14_μI7,
    K_μI14_μI8,
    K_μI14_μI9,
    K_μI14_μI15,
    K_μI15_μI2,
    K_μI15_μI3,
    # K_μI15_μI4,
    K_μI15_μI5,
    K_μI15_μI6,
    # K_μI15_μI7,
    K_μI15_μI8,
    K_μI15_μI9,
  ])

  delta_im = {
    (μI1, μ1): True,
    (μI1, μ2): False,
    (μI1, μ3): False,
    (μI1, μ4): False,
    (μI1, μ5): False,
    (μI1, μ6): False,
    (μI1, μ7): False,
    (μI1, μ8): False,
    (μI1, μ9): False,
    (μI1, μ10): False,
    (μI1, μ11): False,
    (μI1, μ12): False,
    (μI1, μ13): False,
    (μI1, μ14): False,
    (μI1, μ15): False,
    (μI2, μ1): False,
    (μI2, μ2): True,
    (μI2, μ3): False,
    (μI2, μ4): False,
    (μI2, μ5): False,
    (μI2, μ6): False,
    (μI2, μ7): False,
    (μI2, μ8): False,
    (μI2, μ9): False,
    (μI2, μ10): False,
    (μI2, μ11): False,
    (μI2, μ12): False,
    (μI2, μ13): False,
    (μI2, μ14): False,
    (μI2, μ15): False,
    (μI3, μ1): False,
    (μI3, μ2): False,
    (μI3, μ3): True,
    (μI3, μ4): False,
    (μI3, μ5): False,
    (μI3, μ6): False,
    (μI3, μ7): False,
    (μI3, μ8): False,
    (μI3, μ9): False,
    (μI3, μ10): False,
    (μI3, μ11): False,
    (μI3, μ12): False,
    (μI3, μ13): False,
    (μI3, μ14): False,
    (μI3, μ15): False,
    (μI4, μ1): False,
    (μI4, μ2): False,
    (μI4, μ3): False,
    (μI4, μ4): True,
    (μI4, μ5): False,
    (μI4, μ6): False,
    (μI4, μ7): False,
    (μI4, μ8): False,
    (μI4, μ9): False,
    (μI4, μ10): False,
    (μI4, μ11): False,
    (μI4, μ12): False,
    (μI4, μ13): False,
    (μI4, μ14): False,
    (μI4, μ15): False,
    (μI5, μ1): False,
    (μI5, μ2): False,
    (μI5, μ3): False,
    (μI5, μ4): False,
    (μI5, μ5): True,
    (μI5, μ6): False,
    (μI5, μ7): False,
    (μI5, μ8): False,
    (μI5, μ9): False,
    (μI5, μ10): False,
    (μI5, μ11): False,
    (μI5, μ12): False,
    (μI5, μ13): False,
    (μI5, μ14): False,
    (μI5, μ15): False,
    (μI6, μ1): False,
    (μI6, μ2): False,
    (μI6, μ3): False,
    (μI6, μ4): False,
    (μI6, μ5): False,
    (μI6, μ6): True,
    (μI6, μ7): False,
    (μI6, μ8): False,
    (μI6, μ9): False,
    (μI6, μ10): False,
    (μI6, μ11): False,
    (μI6, μ12): False,
    (μI6, μ13): False,
    (μI6, μ14): False,
    (μI6, μ15): False,
    (μI7, μ1): False,
    (μI7, μ2): False,
    (μI7, μ3): False,
    (μI7, μ4): False,
    (μI7, μ5): False,
    (μI7, μ6): False,
    (μI7, μ7): True,
    (μI7, μ8): False,
    (μI7, μ9): False,
    (μI7, μ10): False,
    (μI7, μ11): False,
    (μI7, μ12): False,
    (μI7, μ13): False,
    (μI7, μ14): False,
    (μI7, μ15): False,
    (μI8, μ1): False,
    (μI8, μ2): False,
    (μI8, μ3): False,
    (μI8, μ4): False,
    (μI8, μ5): False,
    (μI8, μ6): False,
    (μI8, μ7): False,
    (μI8, μ8): True,
    (μI8, μ9): False,
    (μI8, μ10): False,
    (μI8, μ11): False,
    (μI8, μ12): False,
    (μI8, μ13): False,
    (μI8, μ14): False,
    (μI8, μ15): False,
    (μI9, μ1): False,
    (μI9, μ2): False,
    (μI9, μ3): False,
    (μI9, μ4): False,
    (μI9, μ5): False,
    (μI9, μ6): False,
    (μI9, μ7): False,
    (μI9, μ8): False,
    (μI9, μ9): True,
    (μI9, μ10): False,
    (μI9, μ11): False,
    (μI9, μ12): False,
    (μI9, μ13): False,
    (μI9, μ14): False,
    (μI9, μ15): False,
    (μI10, μ1): False,
    (μI10, μ2): False,
    (μI10, μ3): False,
    (μI10, μ4): False,
    (μI10, μ5): False,
    (μI10, μ6): False,
    (μI10, μ7): False,
    (μI10, μ8): False,
    (μI10, μ9): False,
    (μI10, μ10): True,
    (μI10, μ11): False,
    (μI10, μ12): False,
    (μI10, μ13): False,
    (μI10, μ14): False,
    (μI10, μ15): False,
    (μI11, μ1): False,
    (μI11, μ2): False,
    (μI11, μ3): False,
    (μI11, μ4): False,
    (μI11, μ5): False,
    (μI11, μ6): False,
    (μI11, μ7): False,
    (μI11, μ8): False,
    (μI11, μ9): False,
    (μI11, μ10): False,
    (μI11, μ11): True,
    (μI11, μ12): False,
    (μI11, μ13): False,
    (μI11, μ14): False,
    (μI11, μ15): False,
    (μI12, μ1): False,
    (μI12, μ2): False,
    (μI12, μ3): False,
    (μI12, μ4): False,
    (μI12, μ5): False,
    (μI12, μ6): False,
    (μI12, μ7): False,
    (μI12, μ8): False,
    (μI12, μ9): False,
    (μI12, μ10): False,
    (μI12, μ11): False,
    (μI12, μ12): True,
    (μI12, μ13): False,
    (μI12, μ14): False,
    (μI12, μ15): False,
    (μI13, μ1): False,
    (μI13, μ2): False,
    (μI13, μ3): False,
    (μI13, μ4): False,
    (μI13, μ5): False,
    (μI13, μ6): False,
    (μI13, μ7): False,
    (μI13, μ8): False,
    (μI13, μ9): False,
    (μI13, μ10): False,
    (μI13, μ11): False,
    (μI13, μ12): False,
    (μI13, μ13): True,
    (μI13, μ14): False,
    (μI13, μ15): False,
    (μI14, μ1): False,
    (μI14, μ2): False,
    (μI14, μ3): False,
    (μI14, μ4): False,
    (μI14, μ5): False,
    (μI14, μ6): False,
    (μI14, μ7): False,
    (μI14, μ8): False,
    (μI14, μ9): False,
    (μI14, μ10): False,
    (μI14, μ11): False,
    (μI14, μ12): False,
    (μI14, μ13): False,
    (μI14, μ14): True,
    (μI14, μ15): False,
    (μI15, μ1): False,
    (μI15, μ2): False,
    (μI15, μ3): False,
    (μI15, μ4): False,
    (μI15, μ5): False,
    (μI15, μ6): False,
    (μI15, μ7): False,
    (μI15, μ8): False,
    (μI15, μ9): False,
    (μI15, μ10): False,
    (μI15, μ11): False,
    (μI15, μ12): False,
    (μI15, μ13): False,
    (μI15, μ14): False,
    (μI15, μ15): True,
  }

  PORTS = Domain(Port, [Port(f'P{i}') for i in range(8)])
  THROTTLES = Domain(Port, [])

  solve_for_delta_and_print(
    KERNELS, INSTS, MUOPS, PORTS, THROTTLES,
    delta_im          = delta_im,
    kernels_with_bottleneck = {
      k for k in KERNELS if k.fmpc >= 0.95
    },
    # rho_kp = {

    # },
    # saturation_margin = 0.975,
    # max_error         = 0.025,
    saturation_margin = 0.9,
    max_error         = 0.1,
  )


def hardcoded_cpu():
  CPU = Model_Builder()

  P0, P1, P2, P3, P4, P5, P6, P7 = map(CPU.make_port, [f'P{i}' for i in range(0, 8)])
  PDIV = CPU.make_port('Pdiv')

  # M0    = CPU.make_muop(P0)
  # M1    = CPU.make_muop(P1)
  # M5_1  = CPU.make_muop(P5, suffix='.1')
  # M5_2  = CPU.make_muop(P5, suffix='.2')
  # M01   = CPU.make_muop(P0, P1)
  # M15   = CPU.make_muop(P1, P5)
  # M015  = CPU.make_muop(P0, P1, P5)
  # M06_1 = CPU.make_muop(P0, P6, suffix='.1')
  # M06_2 = CPU.make_muop(P0, P6, suffix='.2')
  # M0156 = CPU.make_muop(P0, P1, P5, P6)
  # M23   = CPU.make_muop(P2, P3)

  # M5  = M5_1
  # M06 = M06_1

  # ADCX_GPR32i32_GPR32i32 = CPU.make_inst('ADCX_GPR32i32_GPR32i32', M06)
  # ADC_GPR32i32_IMMi32 = CPU.make_inst('ADC_GPR32i32_IMMi32', M06)
  # ADDPD_VR128f64x2_VR128f64x2 = CPU.make_inst('ADDPD_VR128f64x2_VR128f64x2', M01)
  # ADD_GPR32i32_GPR32i32 = CPU.make_inst('ADD_GPR32i32_GPR32i32', M0156)
  # ADOX_GPR32i32_GPR32i32 = CPU.make_inst('ADOX_GPR32i32_GPR32i32', M06)
  # AESDECLAST_VR128i32x4_VR128i32x4 = CPU.make_inst('AESDECLAST_VR128i32x4_VR128i32x4', M0)
  # ANDNPD_VR128u64x2_VR128u64x2 = CPU.make_inst('ANDNPD_VR128u64x2_VR128u64x2', M015)
  # ANDN_GPR32i32_GPR32i32_GPR32i32 = CPU.make_inst('ANDN_GPR32i32_GPR32i32_GPR32i32', M15)
  # BLENDVPD_VR128f64x2_VR128f64x2 = CPU.make_inst('BLENDVPD_VR128f64x2_VR128f64x2', M015)
  # BSF_GPR32i32_GPR32i32 = CPU.make_inst('BSF_GPR32i32_GPR32i32', M1)
  # BTC_GPR32i32_GPR32i32 = CPU.make_inst('BTC_GPR32i32_GPR32i32', M06)
  # CMOVNZ_GPR32i32_GPR32i32 = CPU.make_inst('CMOVNZ_GPR32i32_GPR32i32', M06)
  # DIVPD_VR128f64x2_VR128f64x2 = CPU.make_inst('DIVPD_VR128f64x2_VR128f64x2', M0)
  # DIVPS_VR128f32x4_VR128f32x4 = CPU.make_inst('DIVPS_VR128f32x4_VR128f32x4', M0)
  # INSERTPS_VR128f32x4_VR128f32x4_IMMu8 = CPU.make_inst('INSERTPS_VR128f32x4_VR128f32x4_IMMu8', M5)
  # LDDQU_VR128f64x2_MEM64i32x4 = CPU.make_inst('LDDQU_VR128f64x2_MEM64i32x4', M23)
  # MOVAPD_VR128f64x2_VR128f64x2 = CPU.make_inst('MOVAPD_VR128f64x2_VR128f64x2', M015)
  # MOV_GPR64i64_IMMi64 = CPU.make_inst('MOV_GPR64i64_IMMi64', M0156)
  # SQRTPD_VR128f64x2_VR128f64x2 = CPU.make_inst('SQRTPD_VR128f64x2_VR128f64x2', M0)
  # XOR_GPR32i32_IMMi32 = CPU.make_inst('XOR_GPR32i32_IMMi32', M0156)

  # CPU.make_kernel(ADCX_GPR32i32_GPR32i32, ipc=1.01, mpc=1.01)
  # CPU.make_kernel(ADC_GPR32i32_IMMi32, ipc= 1.0, mpc=1.08)
  # CPU.make_kernel(ADDPD_VR128f64x2_VR128f64x2, ipc= 2.0, mpc= 2.0)
  # CPU.make_kernel(ADD_GPR32i32_GPR32i32, ipc=3.97, mpc=3.97)
  # CPU.make_kernel(ADOX_GPR32i32_GPR32i32, ipc=1.01, mpc=1.01)
  # CPU.make_kernel(AESDECLAST_VR128i32x4_VR128i32x4, ipc=0.998, mpc=0.999)
  # CPU.make_kernel(ANDNPD_VR128u64x2_VR128u64x2, ipc=2.99, mpc=2.99)
  # CPU.make_kernel(ANDN_GPR32i32_GPR32i32_GPR32i32, ipc= 2.0, mpc= 2.0)
  # CPU.make_kernel(BLENDVPD_VR128f64x2_VR128f64x2, ipc=2.45, mpc=2.45)
  # CPU.make_kernel(BSF_GPR32i32_GPR32i32, ipc=0.998, mpc=0.998)
  # CPU.make_kernel(BTC_GPR32i32_GPR32i32, ipc=1.98, mpc=1.98)
  # CPU.make_kernel(CMOVNZ_GPR32i32_GPR32i32, ipc=1.97, mpc=1.97)
  # CPU.make_kernel(DIVPD_VR128f64x2_VR128f64x2, ipc=0.249, mpc=0.25)
  # CPU.make_kernel(DIVPS_VR128f32x4_VR128f32x4, ipc=0.333, mpc=0.333)
  # CPU.make_kernel(INSERTPS_VR128f32x4_VR128f32x4_IMMu8, ipc=0.998, mpc=0.998)
  # CPU.make_kernel(LDDQU_VR128f64x2_MEM64i32x4, ipc=1.99, mpc=1.99)
  # CPU.make_kernel(MOVAPD_VR128f64x2_VR128f64x2, ipc=3.97, mpc=3.97)
  # CPU.make_kernel(MOV_GPR64i64_IMMi64, ipc=0.998, mpc=0.999)
  # CPU.make_kernel(SQRTPD_VR128f64x2_VR128f64x2, ipc=0.222, mpc=0.222)
  # CPU.make_kernel(XOR_GPR32i32_IMMi32, ipc=3.96, mpc=3.96)

  # MOVMSKPD     = CPU.make_inst('movmskpd', M0)
  # PADDQ        = CPU.make_inst('paddq', M015)
  # CVTSI2SS_R32 = CPU.make_inst('cvtsi2ss-r32', M01, M5_01)
  # CVTSI2SS_R64 = CPU.make_inst('cvtsi2ss-r64', M01, M5_01, M5_02)
  # CVTSI2SD     = CPU.make_inst('cvtsi2sd', M01, M5_01)
  # ADDPS        = CPU.make_inst('addps', M01)
  # CMOVBE       = CPU.make_inst('cmovbe', M06_1, M06_2)

  # BEXTR        = CPU.make_inst('bextr', M06, M15)

  # CPU.make_kernel(TZCNT, ipc=1, mpc=1),
  # CPU.make_kernel(CMOVBE, ipc=1, mpc=2),
  # CPU.make_kernel(PADDQ, ipc=3, mpc=3),
  # CPU.make_kernel(MOVMSKPD, ipc=1, mpc=1),
  # CPU.make_kernel(CVTSI2SS_R32, ipc=1, mpc=2),
  # CPU.make_kernel(CVTSI2SS_R64, ipc=0.5, mpc=1.5),
  # CPU.make_kernel(CVTSI2SD, ipc=1, mpc=2),
  # CPU.make_kernel(ADDPS, ipc=2, mpc=2),
  # CPU.make_kernel(SUB_R16_I16, ipc=0.5, mpc=0.5),

  # CPU.make_kernel(TZCNT, CMOVBE, ipc=2, mpc=3),
  # CPU.make_kernel(TZCNT, TZCNT, CMOVBE, ipc=1.5, mpc=2),
  # CPU.make_kernel(TZCNT, CMOVBE, CMOVBE, ipc=1.5, mpc=2.5),
  # CPU.make_kernel(TZCNT, TZCNT, TZCNT, CMOVBE, ipc=1.333, mpc=1.666),
  # CPU.make_kernel(TZCNT, CMOVBE, CMOVBE, CMOVBE, ipc=1.333, mpc=2.333),

  # CPU.make_kernel(TZCNT, PADDQ, ipc=2, mpc=2),
  # CPU.make_kernel(TZCNT, TZCNT, PADDQ, ipc=1.5, mpc=1.5),
  # CPU.make_kernel(TZCNT, PADDQ, PADDQ, ipc=3, mpc=3),

  # CPU.make_kernel(CMOVBE, PADDQ, ipc=2, mpc=3),
  # CPU.make_kernel(CMOVBE, CMOVBE, PADDQ, ipc=1.5, mpc=2.5),
  # CPU.make_kernel(CMOVBE, PADDQ, PADDQ, ipc=3, mpc=4),

  # CPU.make_kernel(CVTSI2SD, CVTSI2SS_R32, ipc=1, mpc=2),
  # CPU.make_kernel(CVTSI2SD, CVTSI2SS_R32, CVTSI2SS_R32, ipc=1, mpc=2),

  MA = CPU.make_muop(P0, P1, P5, P6, suffix='.A')  # ADD_GPR64i64_IMMi8
  MB = CPU.make_muop(P0, P1, P5, P6, suffix='.B')  # ADD_GPR8NOREXi8_GPR8NOREXi8
  MC = CPU.make_muop(P0, P6)          # ADC_GPR8i8_GPR8i8

  A = CPU.make_inst('A', MA)
  B = CPU.make_inst('B', MB)
  C = CPU.make_inst('C', MC)

  CPU.make_kernel(A,    ipc=4,   mpc=4)
  CPU.make_kernel(B,    ipc=1.5, mpc=1.5)
  CPU.make_kernel(C,    ipc=1,   mpc=1)

  delta_IM = {
    (A, MA): True,
    (B, MB): True,
    (C, MC): True,
  }

  CPU.make_kernel(A, B, ipc=2, mpc=2)
  CPU.make_kernel(A, C, ipc=3.85, mpc=3.85)
  CPU.make_kernel(B, C, ipc=2.6, mpc=2.6)

  kernels, CPU = CPU.build()

  # solve_for_throughput_and_print(CPU, kernels)
  solve_for_throughput_and_delta_and_print(CPU, kernels, const_delta_im=delta_IM)


def solve_for_throughput_and_delta_and_print(CPU: CPU_Model, kernels: ty.Sequence[Kernel], **delta_kwargs):
  KERNELS   = Domain(Kernel, kernels)
  INSTS     = Domain(Inst, CPU.insts())
  MUOPS     = Domain(Muop, CPU.muops())
  PORTS     = Domain(Port, CPU.ports())
  THROTTLES = Domain(Port, [])

  solve_for_throughput_and_print(CPU, kernels)

  solve_for_delta_and_print(
    KERNELS, INSTS, MUOPS, PORTS, THROTTLES, **delta_kwargs
  )


def solve_for_throughput_and_print(CPU, KERNELS):
  print('----- BGN: SOLVE FOR THROUGHPUT ----------------------------------------')

  ipc, fmpc, umpc = solve_for_throughput(CPU, KERNELS)

  print('----- END: SOLVE FOR THROUGHPUT ----------------------------------------')
  print()

  for K in KERNELS:
    print(K)
    print('     ', f'{"WANT":>10}', f'{"HAVE":>10}')
    print('  IPC', f'{K.ipc:10.5}', f'{ipc[K]:10.5}')
    print('  fMPC', f'{K.fmpc:10.5}', f'{fmpc[K]:10.5}')
    print('  uMPC', f'{K.umpc:10.5}', f'{umpc[K]:10.5}')
    # assert round(K.ipc, 2) == round(ipc[K], 2), f'{K}: {round(K.ipc, 2)} != {round(ipc[K], 2)}'
    # assert round(K.mpc, 2) == round(mpc[K], 2), f'{K}: {round(K.mpc, 2)} != {round(mpc[K], 2)}'


def solve_for_delta_and_print(KERNELS, INSTS, MUOPS, PORTS, THROTTLES, print_iis: bool = True, **kwargs):
  print('----- BGN: SOLVE FOR DELTA ---------------------------------------------')

  outputs = solve_for_delta(KERNELS, INSTS, MUOPS, PORTS, THROTTLES, print_iis=print_iis, **kwargs)

  print('----- END: SOLVE FOR DELTA ---------------------------------------------')
  print()

  if not outputs:
    return

  print(len(outputs), 'OUTPUTS')

  for o in outputs:
    print(' ', Benchmark_Spec.name_from_instruction_names(i.name for i in o.kernel) + ':',
          o.used_muops(), o.cpu.merged_muop_str(o.used_muops()))
  print()
  print('ERRORS:', str(len([o for o in outputs if o.has_error])) + '/' + str(len(outputs)))
  return

  complex = [o for o in outputs if len(o.kernel) == 1 and o.kernel[0].num_unfused_muops != 1]

  # dot = pygraphviz.AGraph(direc=True, rankdir='TB')
  # render_dot(dot.add_subgraph(), singles)
  # render_dot(dot.add_subgraph(), complex)
  # render_dot(dot.add_subgraph(), pairs)

  # dot.layout(prog='dot')
  # dot.draw('test.pdf')

  # subprocess.run(
  #   ['xdg-open', 'test.pdf']
  # )

  import matplotlib.image as mpimg
  import matplotlib.pyplot as plt

  C = R = math.ceil(math.sqrt(len(complex)))
  while C * (R - 1) >= len(complex):
    R -= 1

  fig = plt.figure()

  for P, o in enumerate(complex, 1):
    fmt = 'png'
    dot = o.to_dot()
    # print(dot.draw().decode())
    dot.layout(prog='dot')
    raw = dot.draw(format=fmt)
    img = mpimg.imread(io.BytesIO(raw), format=fmt)

    fig.add_subplot(R, C, P).imshow(img)

  for ax in fig.axes:
    ax.use_sticky_edges = False

  xpos, ypos, zoom = 0, 0, 1

  XMIN, XMAX = plt.xlim()
  YMIN, YMAX = plt.ylim()

  ZOOM_SPEED = 0.05
  X_SPEED = Y_SPEED = 0.01

  def on_key_press(event):
    nonlocal zoom, xpos, ypos

    if event.key == 'escape':
      plt.close(fig)
      return
    if event.key in ['=', '+']:
      zoom += ZOOM_SPEED
      zoom = min(1, zoom)
    elif event.key in ['_', '-']:
      zoom -= ZOOM_SPEED
      zoom = max(0, zoom)
    elif event.key in ['w', 'up']:
      ypos += YMAX * Y_SPEED
    elif event.key in ['s', 'down']:
      ypos -= YMAX * Y_SPEED
    elif event.key in ['a', 'left']:
      xpos -= XMAX * X_SPEED
    elif event.key in ['d', 'right']:
      xpos += XMAX * X_SPEED
    else:
      # print(event.key)
      return

    plt.xlim(xmin=xpos + XMAX * (1 - zoom) + XMIN, xmax=xpos + XMAX * zoom)
    plt.ylim(ymin=ypos + YMAX * (1 - zoom) + YMIN, ymax=ypos + YMAX * zoom)
    fig.canvas.draw()
    xmin, xmax = plt.xlim()
    ymin, ymax = plt.ylim()
    # print(f'x={xmin}/{xmax} y={ymin}/{ymax} zoom={zoom}')

  fig.canvas.mpl_connect('key_press_event', on_key_press)

  try:
    plt.show()
  except KeyboardInterrupt:
    pass


@dataclasses.dataclass(frozen=True)
class multimethod:
  default: 'function'
  args: ty.Tuple[str, ...] = dataclasses.field(init=False)
  overloads: ty.Dict = dataclasses.field(init=False, default_factory=dict)

  @property
  def __name__(self):
    return self.default.__name__

  def __call__(self, *args):
    types = tuple(type(a) for a in args)

    try:
      fn = self.overloads[types]
    except KeyError:
      fn = self.default

    return fn(*args)

  def __post_init__(self):
    args = self._get_args(self.default)

    assert not hasattr(self, 'args')
    object.__setattr__(self, 'args', tuple(spec.args))

  def register(self, overload: 'fn') -> 'fn':
    args = self._get_args(overload)

    if args != self.args:
      raise ValueError('invalid args in overload')

    types = typing.get_type_hints(overload)

    arg_types = tuple(types[a] for a in args)

    if arg_types in overloads:
      raise ValueError(f'{self.__name__} overloaded with same types twice')

    overloads[arg_types] = overload

  @staticmethod
  def _get_args(fn) -> ty.Tuple[str, ...]:
    spec = inspect.getfullargspec(fn)
    assert spec.varargs is None
    assert spec.varkw is None
    assert not spec.kwonlyargs, spec.kwonlyargs

    return tuple(spec.args)


class Operations(abc.ABC):
  __slots__ = ()

  def __mul__(self, that):
    return Operations.mul(self, that)

  def __rmul__(self, that):
    return Operations.mul(that, self)

  def __add__(self, that):
    return Operations.arithmetic_op(operator.add, '+', self, that)

  def __radd__(self, that):
    return Operations.arithmetic_op(operator.add, '+', that, self)

  def __sub__(self, that):
    return Operations.arithmetic_op(operator.sub, '-', self, that)

  def __rsub__(self, that):
    return Operations.arithmetic_op(operator.sub, '-', that, self)

  def __eq__(self, that):
    def eq(a, b):
      assert type(a) not in (Const, Var, Expr), a
      assert type(b) not in (Const, Var, Expr), b

      try:
        return operator.eq(a, b)
      except gurobipy.GurobiError:
        print(a, b)
        raise

    return Operations.compare(eq, '==', self, that)
    # return Operations.compare(operator.eq, '==', self, that)

  def __le__(self, that):
    return Operations.compare(operator.le, '<=', self, that)

  def __ge__(self, that):
    return Operations.compare(operator.ge, '>=', self, that)

  @staticmethod
  def arithmetic_op(op, op_name, a, b) -> 'Operations':
    assert a is not None
    assert b is not None

    a = Operations.const_promote(a)
    b = Operations.const_promote(b)

    ta = type(a)
    tb = type(b)

    assert ta in (Const, Var, Expr), [a, ta]
    assert tb in (Const, Var, Expr), [b, tb]

    if ta is Const and tb is Const:
      # TODO: be more clever
      vtype = GRB.CONTINUOUS
      return Const(f'{a.name} {op_name} {b.name}', vtype, op(a.const, b.const))

    if DEBUG_EXPR:
      txt = f'{a} {op_name} {b}'
    else:
      txt = ''

    if ta is Const:
      return Expr(op(a.const, b.gurobi_obj), txt)

    if tb is Const:
      return Expr(op(a.gurobi_obj, b.const), txt)

    return Expr(op(a.gurobi_obj, b.gurobi_obj), txt)

  @staticmethod
  def mul(a, b) -> 'Operations':
    assert a is not None
    assert b is not None

    a = Operations.const_promote(a)
    b = Operations.const_promote(b)

    ta = type(a)
    tb = type(b)

    assert ta in (Const, Var, Expr), [a, ta]
    assert tb in (Const, Var, Expr), [b, tb]

    if ta is Var and tb is Var:
      assert a.model is b.model, [a, b]
      assert a.vtype == b.vtype, [a, b]
      raise TypeError(f'Cannot multiply variables {a} and {b}')
      return Expr(a.gurobi_obj * b.gurobi_obj, f'{a} * {b}')

    if ta is Const and tb is Const:
      # TODO: be more clever
      vtype = GRB.CONTINUOUS
      return Const(f'{a.name} * {b.name}', vtype, a.const * b.const)

    if DEBUG_EXPR:
      txt = f'{a} * {b}'
    else:
      txt = ''

    if ta is Const:
      return Expr(a.const * b.gurobi_obj, txt)

    if tb is Const:
      return Expr(a.gurobi_obj * b.const, txt)

    return Expr(a.gurobi_obj * b.gurobi_obj, txt)

  @staticmethod
  def compare(op, op_name, a, b):
    a = Operations.const_promote(a)
    b = Operations.const_promote(b)

    ta = type(a)
    tb = type(b)

    assert ta in (Const, Var, Expr), [a, ta]
    assert tb in (Const, Var, Expr), [b, tb]

    if ta is Const and tb is Const:
      return Const(f'{a.name} {op_name} {b.name}', GRB.BINARY, int(op(a.const, b.const)))

    if DEBUG_EXPR:
      txt = f'{a} {op_name} {b}'
    else:
      txt = ''

    if ta is Const:
      return Expr(op(a.const, b.gurobi_obj), txt)

    if tb is Const:
      return Expr(op(a.gurobi_obj, b.const), txt)

    return Expr(op(a.gurobi_obj, b.gurobi_obj), txt)

  @staticmethod
  def const_promote(a: ty.Union[numbers.Number, 'Operations']) -> 'Operations':
    ta = type(a)

    if ta is int:
      return Const(str(a), GRB.INTEGER, a)
    if ta is bool:
      return Const(str(a), GRB.BINARY, int(a))
    if ta in (float, numpy.float64):
      return Const(str(a), GRB.CONTINUOUS, a)

    assert ta in (Expr, Const, Var), [a, ta]

    return a


class Expr(Operations):
  __slots__ = ('_expr', '_txt')

  def __init__(self, expr: gurobipy.LinExpr, txt: str):
    assert isinstance(expr, (gurobipy.LinExpr, gurobipy.GenExpr, gurobipy.QuadExpr,
                             gurobipy.Constr, gurobipy.TempConstr)), expr

    self._expr = expr
    self._txt  = txt

  @property
  def gurobi_obj(self) -> gurobipy.Var:
    return self._expr

  def python_value(self, default: Gurobi_Value) -> gurobipy.Var:
    assert isinstance(self._expr, (gurobipy.LinExpr, gurobipy.GenExpr, gurobipy.QuadExpr)), self._expr

    return self.gurobi_obj.getValue()

  def __neg__(self):
    return Expr(-self._expr, f'-{self}')

  def __rshift__(self, that: Operations) -> Expr:
    """
      Encode logical implication.
        (expr1) >> (expr2)
      means a constraint like
        if (expr1) then (expr2)
    """

    assert type(self) is Expr

    if DEBUG_EXPR:
      txt = f'({self}) >> ({that})'
    else:
      txt = ''

    if type(that) is Expr:
      return Expr(self._expr >> that._expr, txt)
    if type(that) is Const:
      if that.const:
        return Const.from_bool(txt, True)
      else:
        raise ValueError(f'Unsatisfiable implication: {self} >> 0')

    raise TypeError(type(that))

  def __str__(self):
    return self._txt


class Var(Operations):
  __slots__ = ('_var',)

  def __init__(self, var: gurobipy.Var):
    self._var = var

  @property
  def gurobi_obj(self) -> gurobipy.Var:
    return self._var

  @property
  def name(self) -> str:
    return self._var.VarName

  @property
  def vtype(self) -> str:
    return self._var.VType

  def __rshift__(self, that: Operations) -> Expr:
    """
      Encode logical implication.
        (self == 1) >> (that)
      means a constraint like
        if (self == 1) then (that)
    """

    assert type(self) is Var

    if self.vtype is not GRB.BINARY:
      raise TypeError(f'Non-binary variable {self.name} cannot be used in indicator constraint ({self} >> {that})')

    return (self == 1) >> expr

  def python_value(self, default: Gurobi_Value) -> Gurobi_Value:
    return self.get_python_value(self._var, default)

  @staticmethod
  def get_python_value(var: gurobipy.Var, default: Gurobi_Value) -> Gurobi_Value:
    vtype = var.VType
    try:
      value = var.X
    except AttributeError:
      if default is not None:
        return default
      raise

    if vtype is GRB.BINARY:
      ## don't do this in general,
      ## here we know the possible values of the RHS
      eps = 0.00001

      if abs(value - 0.0) < eps:
        return False

      if abs(value - 1.0) < eps:
        return True

      raise ValueError(str(value) + ' not bool-ish')

    if vtype is GRB.INTEGER:
      if int(value) != value:
        raise ValueError(str(value) + ' not int-ish')

      return int(value)

    if vtype is GRB.CONTINUOUS:
      return value

    raise TypeError('Invalid Gurobi type ' + repr(vtype))

  def __str__(self):
    return self.name

  def __repr__(self):
    return repr(self._var)


class Const(Operations):
  __slots__ = ('_name', '_type', '_const')

  @property
  def vtype(self) -> str:
    return self._type

  @staticmethod
  def from_value(name: str, vtype, const: Gurobi_Value) -> 'Const':
    if vtype is GRB.BINARY:
      return Const.from_bool(name, const)

    if vtype is GRB.INTEGER:
      return Const.from_int(name, const)

    if vtype is GRB.CONTINUOUS:
      return Const.from_float(name, const)

    raise TypeError(f'Invalid vtype: {vtype}')

  @staticmethod
  def from_bool(name: str, const: bool) -> 'Const':
    assert type(const) is bool, f'name={name!r} const={const!r}'

    return Const(name, GRB.BINARY, const)

  @staticmethod
  def from_int(name: str, const: int) -> 'Const':
    assert type(const) is int

    return Const(name, GRB.INTEGER, const)

  @staticmethod
  def from_float(name: str, const: ty.Union[int, float]) -> 'Const':
    assert type(const) in (float, int)

    return Const(name, GRB.CONTINUOUS, const)

  def python_value(self, default: Gurobi_Value) -> Gurobi_Value:
    return self._const

  @property
  def const(self) -> numbers.Number:
    return self._const

  @property
  def name(self) -> str:
    return self._name

  def __rshift__(self, that: Expr) -> ty.Union[Expr, Const]:
    """
      Encode logical implication.
        (expr1) >> (expr2)
      means a constraint like
        if (expr1) then (expr2)
    """

    assert type(that) is Expr or type(that) is Const and that.vtype is GRB.BINARY
    assert self.const in [0, 1, True, False], f'bad const {self} with value {self.const}'

    if DEBUG_EXPR:
      txt = f'({self}) >> ({that})'
    else:
      txt = ''

    if self.const:
      return that
    else:
      return Const(txt, GRB.BINARY, True)

  def __str__(self):
    return repr(self.name) + '/' + str(self._const)
    # return self.name

  def __init__(self, name, type, const):
    self._name  = name
    self._type  = type
    self._const = const


E = ty.TypeVar('E')


class Domain(ty.Generic[E]):
  class Product:
    def __init__(self, *domains):
      self.domains = domains

    def __iter__(self):
      return itertools.product(*self.domains)

  def __init__(self, Type: ty.Type[E], values: ty.Sequence[E]):
    self._type   = Type
    self._values = tuple(values)

    assert all(type(v) is Type for v in values), f'{Type.__name__} vs {" ".join(map(str, values))}'

  @property
  def type(self) -> ty.Type[E]:
    return self._type

  def __mul__(self, that) -> Product:
    if type(that) is Domain:
      return Domain.Product(self, that)
    if type(that) is Domain.Product:
      return Domain.Product(self, *that.domains)
    return NotImplemented

  def __rmul__(self, that) -> Product:
    if type(that) is Domain:
      return Domain.Product(that, self)
    if type(that) is Domain.Product:
      return Domain.Product(*that.domains, self)
    return NotImplemented

  def __getitem__(self, key):
    return self._values[key]

  def __contains__(self, key):
    return key in self._values

  def __iter__(self):
    return iter(self._values)

  def __bool__(self):
    return bool(self._values)

  def __len__(self):
    return len(self._values)


class Relation(object):
  def __init__(self, name, vtype, domain: ty.Tuple[Domain, ...],
               make_var: ty.Callable[[str, str], Var],
               make_const: ty.Callable[[str, Gurobi_Val, str], Const],
               constants: ty.Optional[ty.Dict] = None):
    assert type(name) is str
    assert vtype in (GRB.BINARY, GRB.INTEGER, GRB.CONTINUOUS)

    assert type(domain) is tuple, type(domain)
    assert all(type(d) is Domain for d in domain), domain

    constants = constants or {}

    self._name   = name
    self._vtype  = vtype
    self._types  = tuple(d.type for d in domain)
    self._dict   = collections.OrderedDict()
    self._domain = domain

    assert domain

    for key in constants:
      self._check_key(key)

    for key in itertools.product(*domain):
      key = self._check_key(key)

      if key in self._dict:
        raise ValueError(f'Duplicate key: {key}')

      name = self._name + ''.join(f'_{k}' for k in key)

      if key in constants:
        obj = make_const(name, vtype, constants[key])
        constants.pop(key)
      else:
        obj = make_var(name, vtype)

      self._dict[key] = obj

  @property
  def name(self) -> str:
    return self._name

  def __getitem__(self, key):
    key = self._check_key(key)
    return self._dict[key]

  def __contains__(self, key):
    key = self._check_key(key)

    return key in self._dict

  def _name_from_key(self, key):
    return self._name + ''.join(f'_{k}' for k in key)

  def _check_key(self, key):
    if type(key) is not tuple:
      key = (key,)

    if len(key) != len(self._types):
      have = '(' + ', '.join(type(e).__name__ for e in key) + ')'
      want = '(' + ', '.join(e.__name__ for e in self._types) + ')'
      raise TypeError(f'{self.name}: have {have}, want {want}')

    for k, d in zip(key, self._domain):
      t = d.type

      if not isinstance(k, t):
        have = '(' + ', '.join(type(e).__name__ for e in key) + ')'
        want = '(' + ', '.join(e.__name__ for e in self._types) + ')'
        raise TypeError(f'{self.name}: have {have}, want {want}')

      if k not in d:
        have = '(' + ', '.join(type(e).__name__ for e in key) + ')'
        want = '(' + ', '.join(e.__name__ for e in self._types) + ')'
        raise TypeError(f'{self.name}: {have} not in domain {d.type.__name__}')

    return key

  def dump(self, out: ty.IO[str] = sys.stdout):
    vars = self._dict.values()

    just = max(len(v.name) for v in vars)

    print(self._name, file=out)

    for v in sorted(vars, key=lambda v: v.name):
      print(' ', v.name.ljust(just), v.python_value('???'), file=out)


C_STR_ESCAPE = {
  ord('"'): '\\"',
}


def c_str(txt: str) -> str:
  assert all(c.isprintable() for c in txt), txt

  return '"' + txt.translate(C_STR_ESCAPE) + '"'


def sliding_window(seq: ty.Sequence, n: int=2):
  """
    Returns a sliding window (of width n) over data from the iterable
    s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...
  """

  it = iter(seq)
  result = tuple(itertools.islice(it, n))
  if len(result) == n:
    yield result
  for elem in it:
    result = result[1:] + (elem,)
    yield result
