
import collections
import dataclasses
import enum
import itertools
import math
import typing as ty


class Kernel:
  def __init__(self, ipc: float, umpc: float, fmpc: float, insts: ty.Tuple['Inst', ...]):
    assert all(type(i) is Inst for i in insts), insts

    self._ipc       = float(ipc)
    self._umpc      = float(umpc)
    self._fmpc      = float(fmpc)
    self._insts     = insts

    counts = collections.Counter(insts)

    self._gcd = min(math.gcd(a, b) for a, b in itertools.product(counts.values(), repeat=2))

  @property
  def ipc(self) -> float:
    return self._ipc

  @property
  def fmpc(self) -> float:
    return self._fmpc

  @property
  def umpc(self) -> float:
    return self._umpc

  def count(self, inst) -> int:
    cnt = self._insts.count(inst) / self._gcd
    assert cnt.is_integer()
    return int(cnt)

  def __iter__(self):
    return iter(self._insts)

  def __len__(self):
    return len(self._insts)

  def __getitem__(self, idx):
    return self._insts[idx]

  def __contains__(self, inst: 'Inst'):
    return inst in self._insts

  def __lt__(self, that) -> str:
    return self._insts.items() < that._insts.items()

  def __repr__(self) -> str:
    return f'Kernel(ipc={self.ipc}, fmpc={self.fmpc}, umpc={self.umpc}, insts=[' + ', '.join(repr(i) for i in self._insts) + '])'

  def __str__(self) -> str:
    seq = []
    for i in self:
      if i not in seq:
        seq.append(i)

    def x(i: Inst):
      c = self.count(i)
      assert c > 0

      if c == 1:
        return i.name
      else:
        return i.name + '^' + str(c)

    return 'K{' + ' '.join(x(i) for i in seq) + '}'


class Inst:
  def __init__(self, name, num_fused_muops: int, num_unfused_muops: int):
    self._name              = name
    self._num_fused_muops   = num_fused_muops
    self._num_unfused_muops = num_unfused_muops

  @property
  def name(self) -> str:
    return self._name

  @property
  def num_fused_muops(self) -> int:
    return self._num_fused_muops

  @property
  def num_unfused_muops(self) -> int:
    return self._num_unfused_muops

  def __eq__(self, that):
    if type(self) is not type(that):
      return False

    return self.name == that.name

  def __hash__(self):
    return hash(self.name)

  def __lt__(self, that):
    if type(self) is not type(that):
      return NotImplemented

    return self.name < that.name

  def __str__(self) -> str:
    return f'I{{{self.name}}}'

  def __repr__(self) -> str:
    return f'{type(self).__name__}(name={self.name!r}, num_fused_muops={self._num_fused_muops!r}, num_unfused_muops={self._num_unfused_muops!r})'


class Muop:
  def __init__(self, name: str, is_virtual: bool = False):
    self._name       = name
    self._is_virtual = is_virtual

  @property
  def name(self) -> str:
    return self._name

  @property
  def is_virtual(self) -> bool:
    return self._is_virtual

  def __eq__(self, that):
    if type(self) is not type(that):
      return False

    return self.name == that.name

  def __hash__(self):
    return hash(self.name)

  def __lt__(self, that):
    if type(self) is not type(that):
      return NotImplemented

    return self.name < that.name

  def __str__(self) -> str:
    return f'M{{{self.name}}}'

  def __repr__(self) -> str:
    return f'{type(self).__name__}({self.name!r})'


class Port_Type(enum.Enum):
  REAL     = 0
  SLOWDOWN = 1
  SPEEDUP  = 2

  @property
  def is_virtual(self) -> bool:
    return bool(self.value)


@dataclasses.dataclass(frozen=True)
class Port:
  name: str
  type: Port_Type = Port_Type.REAL
  max_throughput: float = 1.0

  @property
  def is_real(self) -> bool:
    return not (self.is_virtual or self.is_slowdown)

  @property
  def is_virtual(self) -> bool:
    return self.type.is_virtual

  @property
  def is_slowdown(self) -> bool:
    return self.type is Port_Type.SLOWDOWN

  def __str__(self) -> str:
    return f'P{{{self.name}}}'

  def __key(self):
    return self.name, self.type, self.max_throughput

