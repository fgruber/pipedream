
"""
  Helpers for dealing with statistics.
"""

import pipedream.utils.yaml as yaml
import pipedream.utils.terminal as terminal

import fractions
import io
import math
import numpy
import typing as ty

import scipy.stats


__all__ = [
  'Statistics',
]


class Statistics(yaml.YAML_Struct):
  """
    Statistics for a set of measurements.
    Mean, standard deviation, some percentiles, ...
  """
  raw_data    = yaml.Slot(ty.List[float], list)
  num_samples = yaml.Slot(float, math.nan)
  ## arithmetic mean
  mean        = yaml.Slot(float, math.nan)
  ## sample standard deviation
  stddev      = yaml.Slot(float, math.nan)
  ## sample variance
  variance    = yaml.Slot(float, math.nan)
  ## median absolute deviation
  MAD         = yaml.Slot(float, math.nan)
  min         = yaml.Slot(float, math.nan)
  max         = yaml.Slot(float, math.nan)
  percentiles = yaml.Slot(ty.Dict[int, float], dict)
  histogram   = yaml.Slot(ty.List[int], list)

  def __init__(self, *,
               mean: float, stddev: float, variance: float = math.nan,
               num_samples: float = math.nan,
               MAD: float = math.nan,
               min: float = math.nan, max: float = math.nan,
               percentiles: ty.Dict[int, float] = None,
               histogram: ty.Sequence[float] = None,
               raw_data: ty.List[float] = None,
               ):
    self.raw_data    = raw_data if raw_data is not None else []
    self.num_samples = float(num_samples)
    self.mean        = float(mean)
    self.stddev      = float(stddev)
    self.variance    = self.stddev ** 2 if math.isnan(float(variance)) else float(variance)
    self.MAD         = float(MAD)
    self.min         = float(min)
    self.max         = float(max)
    self.percentiles = percentiles or {}
    self.histogram   = histogram or []

    assert not percentiles or 0 not in percentiles or percentiles[0] == min
    assert not percentiles or 100 not in percentiles or percentiles[100] == max

    assert not percentiles or 10 in percentiles
    assert not percentiles or 25 in percentiles
    assert not percentiles or 50 in percentiles
    assert not percentiles or 75 in percentiles
    assert not percentiles or 90 in percentiles

  def drop_details(self):
    """
      Throw away the histogram and all percentiles except 10,25,50,75,90.
      This is intended to reduce memory usage.
    """
    self.raw_data = []
    self.histogram = []
    self.percentiles = {
      10: self.percentiles[10],
      25: self.percentiles[25],
      50: self.percentiles[50],
      75: self.percentiles[75],
      90: self.percentiles[90],
    }

  def prediction_interval(self, probability: float):
    assert 0 <= probability <= 1

    return scipy.stats.norm.interval(probability, loc=self.mean, scale=self.stddev)

  @staticmethod
  def from_array(array: numpy.ndarray, keep_raw_data: bool = False):
    """
      compute values from a numpy array.
    """

    if len(array) == 0:
      raise ValueError('Cannot create Statistics from empty array')

    # https://en.wikipedia.org/wiki/Median_absolute_deviation
    median = numpy.median(array)
    MAD    = numpy.median([abs(x - median) for x in array])

    percentiles = numpy.percentile(array, range(1, 100))
    percentiles = {n + 1: percentiles[n] for n in range(len(percentiles))}

    if not math.isinf(median) and not math.isnan(median):
      hist, hist_bins = numpy.histogram(array, 100)

      assert sum(hist) == len(array), f'{sum(hist)} != {len(array)}'

      hist = [int(n) for n in hist]
    else:
      hist = None

    if keep_raw_data:
      return Statistics(
        raw_data    = array,
        num_samples = len(array),
        mean        = array.mean(),
        stddev      = array.std(ddof=1),
        variance    = array.var(ddof=1),
        MAD         = MAD,
        min         = array.min(),
        max         = array.max(),
        percentiles = percentiles,
        histogram   = hist,
      )

    else:
      return Statistics(
        num_samples = len(array),
        mean        = array.mean(),
        stddev      = array.std(ddof=1),
        variance    = array.var(ddof=1),
        MAD         = MAD,
        min         = array.min(),
        max         = array.max(),
        percentiles = percentiles,
        histogram   = hist,
      )

  @property
  def IQR(self):
    """
      Interquartile range (https://en.wikipedia.org/wiki/Interquartile_range)
    """
    return self.p75 - self.p25

  @property
  def p0(self):
    """
      0th percentile (minimum value)
    """
    return self.min

  @property
  def p100(self):
    """
      100th percentile (maximum value)
    """
    return self.max

  @property
  def p10(self):
    return self.percentile(10)

  @property
  def p25(self):
    return self.percentile(25)

  @property
  def p50(self):
    return self.percentile(50)

  @property
  def p75(self):
    return self.percentile(75)

  @property
  def p90(self):
    return self.percentile(90)

  def percentile(self, q: int) -> float:
    """
      Retrieve q-th percentile value.
      Returns NaN if that percentile is not available
    """

    return self.percentiles.get(q, math.nan)

  def scale(self, scalar: float) -> 'Statistics':
    """
      return new Statistics where mean and stddev are multiplied with scalar.
      All other values are dropped.

      I guess this is basically only safe for results of __truediv__
    """

    return Statistics(mean = self.mean * scalar, stddev = self.stddev * scalar)

  def __truediv__(self, that):
    if type(that) is not Statistics:
      return NotImplemented

    ## https://stats.stackexchange.com/questions/49399/standard-deviation-of-a-ratio-percentage-change
    ##
    ## comment by Eekhorn:
    ## ... if you want to normalize your data y ± Δy to z ± Δz
    ##    x = y / z
    ## you have to calculate the standard deviation Δx as follows:
    ##    Δx = x * sqrt( (Δy / y)^2 + (Δz / z)^2 )
    ## NOTE: I removed the scaling by 100 to get percentages
    ##       it appears in a linear factor in both equations so you can scale later
    ##       just by multiplying mean & stddev if desired.

    y_mean = self.mean
    y_dev  = self.stddev

    z_mean = that.mean
    z_dev  = that.stddev

    x_mean = y_mean / z_mean
    x_dev  = x_mean * math.sqrt(
      (y_dev / y_mean) ** 2 +
      (z_dev / z_mean) ** 2
    )

    return Statistics(mean=x_mean, stddev=x_dev)

  def __sub__(self, that):
    assert type(that) is Statistics

    assert False, "TODO: need covariance, and for that we need to store all values :("

  @classmethod
  def stddev_of_difference(clss, data1: numpy.ndarray, data2: numpy.ndarray):
    return math.sqrt(clss.variance_of_difference(data1, data2))

  @classmethod
  def variance_of_difference(clss, data1: numpy.ndarray, data2: numpy.ndarray):
    # https://en.wikipedia.org/wiki/Variance#Sum_of_correlated_variables
    # https://stats.stackexchange.com/questions/142745/what-is-the-demonstration-of-the-variance-of-the-difference-of-two-dependent-var
    # Var[X - Y] = Var[X] + Var[Y] - 2 * Cov[X, Y]

    cov = numpy.cov(numpy.vstack(data1, data2))

    return numpy.var(data1) + numpy.var(data2) - 2 * cov[0, 1]

  def __str__(self):
    out = io.StringIO()
    yaml.dump(self, out)
    return out.getvalue()

  def whisker_plot(self, width: int) -> str:
    assert width > 2
    width -= 2

    def frac(n, default = 0):
      if math.isnan(n):
        n = default
      return fractions.Fraction(n)

    min = fractions.Fraction(self.min)
    p10 = frac(self.p10, min)
    p25 = frac(self.p25, p10)
    p50 = fractions.Fraction(self.p50)
    p75 = frac(self.p75, p50)
    p90 = frac(self.p90, p75)
    max = fractions.Fraction(self.max)

    span = (max - min) or 1

    def w(hi, lo):
      # assert lo <= hi
      return round(((hi - lo) * width) / span)

    w10  = w(p10, min)
    w25  = w(p25, p10)
    w50  = w(p50, p25)
    w75  = w(p75, p50)
    w90  = w(p90, p75)
    w100 = w(max, p90)

    # print('%10s %10s' % (p10, w10))
    # print('%10s %10s' % (p25, w25))
    # print('%10s %10s' % (p50, w50))
    # print('%10s %10s' % (p75, w75))
    # print('%10s %10s' % (p90, w90))
    # print('%10s %10s' % (max, w100))

    txt = ''
    txt += '['
    txt += ' ' * round(w10)
    txt += ('|' + '-' * width)[:round(w25)]
    txt += '=' * round(w50)
    txt += '0'
    txt += '=' * round(w75)
    txt += ('|' + '-' * width)[:round(w90)][::-1]
    txt += ' ' * round(w100)
    txt += ']'

    # draw outliers (lower 10 and higher 10 percentlies)
    if self.percentiles:
      tmp = list(txt)

      def draw_outlier(pos: int):
        if pos >= len(tmp):
          return
        if tmp[pos] != ' ':
          return

        tmp[pos] = '.'

      for p in range(1, 10):
        if p in self.percentiles:
          draw_outlier(w(self.percentiles[p], self.min))

      for p in range(91, 99):
        if p in self.percentiles:
          draw_outlier(w(self.percentiles[p], self.min))

      txt = ''.join(tmp)

    return txt

  def histogram_plot(self, width: int) -> str:
    """
      Visualizes distribution of values using terminal colors.

      Produces a 'histogram' of width :width:
        blue   -> very many values
        red    -> many values
        yellow -> some values
        green  -> few values
    """

    assert width > 2
    width -= 2

    assert self.histogram

    BLACK  = (0, 0, 0)
    GRAY   = (55, 55, 55)
    WHITE  = (255, 255, 255)
    SOFT_GREEN = (171, 252, 133)
    GREEN  = (122, 244, 66)
    YELLOW = (247, 243, 32)
    RED    = (244, 19, 19)
    CYAN   = (133, 246, 252)
    BLUE   = (11, 19, 239)
    PURPLE = (150, 0, 170)

    N = max(self.histogram)

    COLORS   = (BLACK, GRAY, SOFT_GREEN, GREEN, YELLOW, RED, CYAN, BLUE, PURPLE, WHITE)
    STEPS    = (0, N * 0.025, N * 0.05, N * 0.10, N * 0.25, N * 0.50, N * 0.75, N * 0.9, N * 0.95, N)
    GRADIENT = tuple(tuple(C[i] for C in COLORS) for i in range(3))

    def color_gradient(n: float):
      return tuple(int(round(numpy.interp(n, STEPS, GRADIENT[i]))) for i in range(3))

    hist = ''
    hist += '['

    for n in self.histogram:
      rgb = color_gradient(n)
      hist += terminal.Colors.bg_rgb(*rgb)(' ')

    hist += ']'

    assert len(hist) == 102

    return hist

  def to_jsonish(self) -> dict:
    d = {}
    for slot in self._yaml_slots_:
      assert slot.yaml_name not in d

      val = getattr(self, slot.py_name)
      d[slot.yaml_name] = val
    return d

  @staticmethod
  def from_jsonish(src: dict) -> 'Statistics':
    out = {}

    for slot in Statistics._yaml_slots_:
      try:
        val = src[slot.yaml_name]

        if slot.type is ty.Dict[int, float]:
          val = {int(k): v for k, v in val.items()}
      except KeyError:
        val = slot.default

      out[slot.py_name] = val

    try:
      return Statistics(**out)
    except TypeError:
      print(src)
      raise
