#!/usr/bin/env python3

"""
  Round measurements in jsonl file to get "cleaner" values.
"""

import argparse
import sys
import collections
import json
import fractions
import itertools
from typing import *


def main():
  parser = argparse.ArgumentParser()

  parser.add_argument('FILE', nargs='*')
  parser.add_argument('-o', '--output', default=sys.stdout, type=argparse.FileType('w'))

  args = parser.parse_args()

  if not args.FILE:
    args.FILE = ['/dev/stdin']

  out = args.output

  kernels = set()
  IPC = collections.defaultdict(list)
  MPC = collections.defaultdict(list)
  instructions = {}
  muops = {}

  for F in args.FILE:
    with open(F) as fd:
      try:
        for line in fd:
          data = json.loads(line)

          kernel = frozenset(data['kernel'])

          new_n_insts = data['instructions']
          new_n_muops = data['muops']

          old_n_insts = instructions.get(kernel, 1)
          old_n_muops = muops.get(kernel, 0)

          old_num_muops = round(old_n_muops / old_n_insts)
          new_num_muops = round(new_n_muops / new_n_insts)

          assert new_num_muops > 0, f'{line} => num_muops = {new_num_muops}'

          if old_num_muops and old_num_muops != new_num_muops:
            print('error: kernel', *sorted(kernel),
                  'does not always produce the same number of muops:',
                  # f'{old_num_muops} != {new_num_muops}',
                  f'{instructions.get(kernel, 0) / muops.get(kernel, 1)} != {n_muops / n_insts}',
                  file=sys.stderr)
            exit(1)

          kernels.add(kernel)

          IPC[kernel].append(data['ipc'])
          MPC[kernel].append(data['mpc'])

          instructions[kernel] = round(new_n_insts)
          muops[kernel]        = round(new_n_muops)
      except json.JSONDecodeError:
        print('error: malformed file', repr(F) + ': line:', repr(line), file=sys.stderr)

  for kernel in sorted(kernels, key=''.join):
    insts = sorted('"' + i + '"' for i in kernel)
    insts = '[' + ', '.join(insts) + ']'

    try:
      ipc = round_measurements(IPC[kernel])
      mpc = round_measurements(MPC[kernel])
    except AssertionError:
      print(insts)
      raise

    print('{',
          f'"kernel":', insts + ',',
          f'"ipc": {ipc}/{IPC[kernel]},',
          f'"mpc": {mpc}/{MPC[kernel]},',
          f'"instructions": {instructions[kernel]},',
          f'"muops": {muops[kernel]}}}',
          file=out)


_NICE_FRACTIONS = [fractions.Fraction(1, 1)] + [fractions.Fraction(j, i) for i in range(7) for j in range(i)]
_UGLY_FRACTIONS = [fractions.Fraction(1, i) for i in range(7, 100)]
_ALL_FRACTIONS = _NICE_FRACTIONS + _UGLY_FRACTIONS


def round_measurements(ns: Sequence[float]) -> float:
  assert all(n > 0 for n in ns)

  if len(set(ns)) == 1 and ns[0].is_integer():
    return ns[0]

  ns = [max(ns)]

  def error(candidate):
    return sum(abs(n - candidate) for n in ns)

  Is = [int(n) for n in ns]

  best = min(
    [i + d for i, d in itertools.product(Is, _ALLOWED_FRACTIONS)],
    key=error
  )

  MAX_ERROR = 0.1

  assert error(best) < len(ns) * MAX_ERROR, [best, ns, error(best)]

  return float(best)


def round_measurements(ns: Sequence[float]) -> float:
  n = max(ns)

  error = lambda x: abs(n - x)

  if n < 1:
    best = min([int(n) + f for f in _ALL_FRACTIONS], key=error)

    MAX_ERROR = 0.1

    assert error(best) < MAX_ERROR, [best, n, error(best)]

    return float(best)
  else:
    best = min([int(n) + f for f in _NICE_FRACTIONS], key=error)

    MAX_ERROR = 0.1

    assert error(best) < MAX_ERROR, [best, n, error(best)]

    return float(best)


if __name__ == '__main__':
  main()
