/// helper classes for extracting the XED instruction dabase from its C API.

#pragma once

#include "config.hpp"

#include "XED_Info.hpp"
#include "Pipedream_Info.hpp"
#include "utils.hpp"

#include <memory> // for std::unique_ptr
#include <vector> // for std::vector

/// Iterator over XEDs list of all instructions.
struct xed_inst_t_iterator {
  /// iterator over all xed_inst_t in XEDs instruction database.
  static Iterator_Range<xed_inst_t_iterator> all() {
    assert(xed_inst_iclass(xed_inst_table_base()) == XED_ICLASS_INVALID);
    assert(xed_inst_iclass(xed_inst_table_base() + 1) != XED_ICLASS_INVALID);

    return Iterator_Range<xed_inst_t_iterator>{
      xed_inst_table_base() + 1,
      xed_inst_table_base() + XED_MAX_INST_TABLE_NODES,
    };
  }

  xed_inst_t_iterator& operator++() {
    ++_inst;
    return *this;
  }
  xed_inst_t_iterator  operator++(int) {
    const auto retval = *this;
    ++(*this);
    return retval;
  }

  bool operator==(xed_inst_t_iterator that) const {
    return _inst == that._inst;
  }
  bool operator!=(xed_inst_t_iterator that) const {
    return !(*this == that);
  }

  const xed_inst_t *operator*() const { return _inst; }
private:
  xed_inst_t_iterator(const xed_inst_t *inst) : _inst{inst} {}

  const xed_inst_t *_inst;
};



/// Iterates over all possible XED
/// Can be used in a range-based for loop
/// @code
///   for (XED_Encoder_State state : XED_Encoder_State_iterator::all()) {
///     ...
///   }
/// @endcode
struct XED_Encoder_State_iterator {
  /// @name ctor/dtor
  /// @{

  /// iterator over all XED_Encoder_State we can handle for now.
  static XED_Encoder_State_iterator all() {
    return XED_Encoder_State_iterator{all_cpu_mode, all_eosz, all_easz, all_allow_rex};
  }

  XED_Encoder_State_iterator(
    Span<XED_CPU_Mode> cpu_mode,
    Span<Effective_Operand_Width> eosz,
    Span<Effective_Address_Width> easz,
    Span<bool> allow_rex
  ) : cpu_mode{cpu_mode}, eosz{eosz}, easz{easz}, allow_rex{allow_rex} {}

  /// create an empty iterator
  XED_Encoder_State_iterator() = default;

  /// @}

  /// @name allow use in range-based for loops

  /// helper for detecting when an iterator has been exhausted.
  struct sentinel_t {};

  XED_Encoder_State_iterator begin() { return *this; }
  sentinel_t                 end()   { return sentinel_t{}; }

  XED_Encoder_State_iterator& operator++() {
    if (cpu_mode.size() > 1) {
      cpu_mode  = cpu_mode.drop_front();
      return *this;
    }

    if (eosz.size() > 1) {
      cpu_mode  = all_cpu_mode;
      eosz      = eosz.drop_front();
      return *this;
    }

    if (easz.size() > 1) {
      cpu_mode  = all_cpu_mode;
      eosz      = all_eosz;
      easz      = easz.drop_front();
      return *this;
    }

    if (!allow_rex.empty()) {
      cpu_mode  = all_cpu_mode;
      eosz      = all_eosz;
      easz      = all_easz;
      allow_rex = allow_rex.drop_front();
      return *this;
    }

    /// all iterators exhausted, done
    return *this;
  }
  XED_Encoder_State_iterator  operator++(int) {
    const auto retval = *this;
    ++(*this);
    return retval;
  }

  bool operator==(sentinel_t) const {
    return allow_rex.empty();
  }
  bool operator!=(sentinel_t that) const {
    return !(*this == that);
  }

  XED_Encoder_State operator*() const {
    return XED_Encoder_State{
      cpu_mode.front(),
      eosz.front(),
      easz.front(),
      allow_rex.front(),
    };
  }
private:
  static constexpr const XED_CPU_Mode            all_cpu_mode[]  = {
    // @todo 32-bit mode, 16-bit mode.
    XED_CPU_Mode::make(XED_MACHINE_MODE_LONG_64, XED_ADDRESS_WIDTH_64b),
  };
  static constexpr const Effective_Operand_Width all_eosz[]      = {EOSZ_16, EOSZ_32, EOSZ_64};
  static constexpr const Effective_Address_Width all_easz[]      = {EASZ_16, EASZ_32, EASZ_64};
  static constexpr const bool                    all_allow_rex[] = {true, false};

  Span<XED_CPU_Mode> cpu_mode;
  Span<Effective_Operand_Width> eosz;
  Span<Effective_Address_Width> easz;
  Span<bool> allow_rex;
};


/// Abstract base class for helpers for iterating over all possible combinations
/// of operands.
/// One xed_encoder_operand_iterator iterates over all possible values we can
/// put in one xed_encoder_operand_t.
///
/// An xed_encoder_operand_iterator fills in the operand passed in by value.
/// @code
///   xed_encoder_operand_t operand;
///   xed_encoder_operand_iterator *iter = new ...;
///
///   while (!iter->is_done()) {
///     // writes to operand
///     iter->advance();
///
///     do_something_with_operand(operand);
///   }
/// @endcode
struct xed_encoder_operand_iterator {
  /// constructor of subclasses must fill in the first possible value
  xed_encoder_operand_iterator(xed_encoder_operand_t &operand) : operand{operand} {}

  virtual ~xed_encoder_operand_iterator() {}

  /// Are we done exploring all options of this iterator?.
  virtual bool is_done() const = 0;

  /// advance to the next option.
  virtual void advance() = 0;

  /// reset iterator to its original state
  virtual void reset() = 0;

  /// print current operand this iterator holds to stream.
  void show(std::ostream &O) const {
    switch (operand.type) {
      case XED_ENCODER_OPERAND_TYPE_IMM0:
        O << "imm" << operand.width_bits << ":" << operand.u.imm0;
        break;
      case XED_ENCODER_OPERAND_TYPE_REG:
        O << "reg: " << xed_reg_enum_t2str(operand.u.reg);
        break;
      case XED_ENCODER_OPERAND_TYPE_MEM:
        O << "mem" << operand.width_bits << ":" << xed_reg_enum_t2str(operand.u.mem.base);
        break;
      default:
        std::cerr << "unsupported operand type: " << unsigned(operand.type) << std::endl;
        abort();
    }
  }
protected:
  xed_encoder_operand_t &operand;
};


/// iterator for trying different registers for an xed_encoder_operand_t
struct xed_encoder_register_operand_iterator : xed_encoder_operand_iterator{
  xed_encoder_register_operand_iterator(
    xed_encoder_operand_t &operand, Register_Class reg_class
  ) : xed_encoder_operand_iterator{operand}, curr_reg_class{reg_class}, reg_class{reg_class} {
  }

  bool is_done() const override {
    return curr_reg_class.empty();
  }

  void advance() override {
    operand        = xed_reg(curr_reg_class.front());
    curr_reg_class = curr_reg_class.drop_front();
  }

  void reset() override {
    curr_reg_class = reg_class;
  }
private:
  Register_Class curr_reg_class;
  const Register_Class reg_class;
};


/// iterator for iterating over all possible memory operands for an xed_encoder_operand_t
/// @todo explore more base registers.
/// @todo explore more addressing modes.
struct xed_encoder_memory_operand_iterator : xed_encoder_operand_iterator{
  xed_encoder_memory_operand_iterator(xed_encoder_operand_t &operand, xed_reg_enum_t base_reg)
  : xed_encoder_operand_iterator{operand}, base_reg{base_reg}
  {
    reset();
  }

  bool is_done() const override {
    return memory_widths.empty();
  }

  void advance() override {
    operand       = xed_mem_b(base_reg, memory_widths.front());
    memory_widths = memory_widths.drop_front();
  }

  void reset() override {
    memory_widths = {8, 16, 32, 64, 128, 256, 512, 1024};
  }
private:
  Span<int> memory_widths;
  const xed_reg_enum_t base_reg;
};


/// Iterates over all possible xed_encoder_instructions we can
/// generate for one xed_inst_t.
struct xed_encoder_instruction_t_iterator {
  /// @name ctor/dtor
  /// @{

  static std::unique_ptr<xed_encoder_instruction_t_iterator> make(
    XED_Instruction inst,
    XED_Encoder_State state,
    unsigned memory_width
  ) {
    ///// figure out how many operands the instruction has

    /// xed_inst fails if you pass SUPPRESSED operands.
    size_t num_encoder_operands = inst.num_operands();
    while (num_encoder_operands) {
      const xed_operand_visibility_enum_t opvis = inst.operand(num_encoder_operands - 1).visibility();

      if (opvis != XED_OPVIS_SUPPRESSED) { break; }

      num_encoder_operands--;
    }

    std::unique_ptr<xed_encoder_instruction_t_iterator> out{new xed_encoder_instruction_t_iterator{inst, state}};
    out->_encoder_operands.resize(num_encoder_operands);

    ///// fill in constant operands & set up iterators for varying operands

    for (size_t i = 0, end = num_encoder_operands; i < end; i++) {
      XED_Instruction_Operand op = inst.operand(i);

      const std::optional<Machine_Value> value = Convert::machine_value_from_xed(inst, op, state, memory_width);

      /// can not represent operand
      if (!value) {
#if XED_EXTRACT_DEVELOPER_MODE
        std::cerr << "# TODO: " << __FILE__ << ":" << __LINE__ << ":" << __func__ << ":" << std::endl;
        std::cerr << "# TODO: Can not represent " << inst.iform_str() << " operand " << i << std::endl;
        abort();
#endif
        out.reset();
        return out;
      }

      switch (value->kind()) {
        case Machine_Value::REG:
          if (auto reg = value->reg()) {
            /// fixed register
            out->_encoder_operands[i] = xed_reg(*reg);
          } else {
            /// register class
            out->_operand_iterators.emplace_back(new xed_encoder_register_operand_iterator{
              out->_encoder_operands[i],
              value->reg_class(),
            });
            out->_operand_iterators.back()->advance();
          }
          break;
        case Machine_Value::MEM:
        case Machine_Value::ADDR:
          {
            xed_reg_enum_t base;
            if (auto reg = value->base_reg()) {
              base = *reg;
            } else {
              base = XED_Register_Info::base_reg(state);
            }
            out->_encoder_operands[i] = xed_mem_b(base, memory_width);
          }
          break;
        case Machine_Value::BRDISP:
          /// @todo try some different immediate values
          out->_encoder_operands[i] = xed_relbr(0, value->brdisp_bits());
          break;
        case Machine_Value::IMM:
          /// @todo try some different immediate values
          {
            xed_uint64_t imm_value = 0;

            if (auto v = value->imm_value()) {
              imm_value = *v;
            }

            out->_encoder_operands[i] = xed_imm0(imm_value, value->imm_bits());
          }
          break;
        case Machine_Value::FLAGS:
          /// FLAGS operands are always supressed
          std::cerr << "error: flag operand is not suppressed" << std::endl;
          abort();
          break;
      }
    }

    out->_update_current_inst();
    return out;
  }

  /// @}

  /// @name API to iterate over all possible encoder instructions
  /// @{

  /// get current instruction encoding
  const xed_encoder_instruction_t &get() const {
    return _current_inst;
  }

  /// Advance to next possible instruction encoding.
  /// Returns true if an encoding was found.
  /// Returns false is iterator is exhausted.
  bool advance() {
    for (auto &operand_iterator : _operand_iterators) {
      if (operand_iterator->is_done()) {
        operand_iterator->reset();
      } else {
        operand_iterator->advance();
        _update_current_inst();
        /// there are more values
        return true;
      }
    }

    /// all operand iterators returned is_done, so we are done
    return false;
  }

  /// @}
private:
  xed_encoder_instruction_t_iterator(
    XED_Instruction inst,
    XED_Encoder_State state
  ) : _inst{inst}, _state{state} {}

  xed_encoder_instruction_t_iterator(const xed_encoder_instruction_t_iterator&) = delete;
  xed_encoder_instruction_t_iterator(xed_encoder_instruction_t_iterator&&) = default;

  xed_encoder_instruction_t_iterator &operator=(xed_encoder_instruction_t_iterator&&) = default;


  void _update_current_inst() {
    xed_inst(
      &_current_inst, _state.cpu_mode.raw(), _inst.iclass(),
      _state.effective_operand_width,
      _encoder_operands.size(), _encoder_operands.data()
    );
  }

  const XED_Instruction   _inst;
  const XED_Encoder_State _state;

  std::vector<xed_encoder_operand_t>                         _encoder_operands;
  std::vector<std::unique_ptr<xed_encoder_operand_iterator>> _operand_iterators;
  xed_encoder_instruction_t _current_inst;
};

