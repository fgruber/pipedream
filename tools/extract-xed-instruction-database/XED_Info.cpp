///
/// Helpers for working with Intel XED
///

#include "XED_Info.hpp"
#include "utils.hpp"

namespace {

struct XED_Regs {
  /// #######################################################################
  /// # Expand the generic registers using the effective address size EASZ
  /// #######################################################################
  /// xed_reg_enum_t ArAX()::
  static constexpr xed_reg_enum_t ArAX(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_AX
      case 1:
        return XED_REG_AX;
        /// EASZ=2 | XED_REG_EAX
      case 2:
        return XED_REG_EAX;
        /// EASZ=3 | XED_REG_RAX
      case 3:
        return XED_REG_RAX;
    }
    abort();
  }

  /// xed_reg_enum_t ArBX()::
  static constexpr xed_reg_enum_t ArBX(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_BX
      case 1:
        return XED_REG_BX;
        /// EASZ=2 | XED_REG_EBX
      case 2:
        return XED_REG_EBX;
        /// EASZ=3 | XED_REG_RBX
      case 3:
        return XED_REG_RBX;
    }
    abort();
  }

  /// xed_reg_enum_t ArCX()::
  static constexpr xed_reg_enum_t ArCX(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_CX
      case 1:
        return XED_REG_CX;
        /// EASZ=2 | XED_REG_ECX
      case 2:
        return XED_REG_ECX;
        /// EASZ=3 | XED_REG_RCX
      case 3:
        return XED_REG_RCX;
    }
    abort();
  }


  /// xed_reg_enum_t ArDX()::
  static constexpr xed_reg_enum_t ArDX(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_DX
      case 1:
        return XED_REG_DX;
        /// EASZ=2 | XED_REG_EDX
      case 2:
        return XED_REG_EDX;
        /// EASZ=3 | XED_REG_RDX
      case 3:
        return XED_REG_RDX;
    }
    abort();
  }

  /// xed_reg_enum_t ArSI()::
  static constexpr xed_reg_enum_t ArSI(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_SI
      case 1:
        return XED_REG_SI;
        /// EASZ=2 | XED_REG_ESI
      case 2:
        return XED_REG_ESI;
        /// EASZ=3 | XED_REG_RSI
      case 3:
        return XED_REG_RSI;
    }
    abort();
  }

  /// xed_reg_enum_t ArDI()::
  static constexpr xed_reg_enum_t ArDI(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_DI
      case 1:
        return XED_REG_DI;
        /// EASZ=2 | XED_REG_EDI
      case 2:
        return XED_REG_EDI;
        /// EASZ=3 | XED_REG_RDI
      case 3:
        return XED_REG_RDI;
    }
    abort();
  }

  /// xed_reg_enum_t ArSP()::
  static constexpr xed_reg_enum_t ArSP(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_SP
      case 1:
        return XED_REG_SP;
        /// EASZ=2 | XED_REG_ESP
      case 2:
        return XED_REG_ESP;
        /// EASZ=3 | XED_REG_RSP
      case 3:
        return XED_REG_RSP;
    }
    abort();
  }


  /// xed_reg_enum_t ArBP()::
  static constexpr xed_reg_enum_t ArBP(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_BP
      case 1: return XED_REG_BP;
        /// EASZ=2 | XED_REG_EBP
      case 2: return XED_REG_EBP;
        /// EASZ=3 | XED_REG_RBP
      case 3: return XED_REG_RBP;
    }
    abort();
  }

  /// xed_reg_enum_t SrSP()::
  static constexpr xed_reg_enum_t SrSP(unsigned stack_address_width) {
    switch (stack_address_width) {
      /// stack_address_width=16 | XED_REG_SP
      case 16: return XED_REG_SP;
        /// stack_address_width=32 | XED_REG_ESP
      case 32: return XED_REG_ESP;
        /// stack_address_width=64 | XED_REG_RSP
      case 64: return XED_REG_RSP;
    }
    abort();
  }


  /// xed_reg_enum_t SrBP()::
  static constexpr xed_reg_enum_t SrBP(unsigned stack_address_width) {
    switch (stack_address_width) {
      /// stack_address_width=16 | XED_REG_BP
      case 16: return XED_REG_BP;
        /// stack_address_width=32 | XED_REG_EBP
      case 32: return XED_REG_EBP;
        /// stack_address_width=64 | XED_REG_RBP
      case 64: return XED_REG_RBP;
    }
    abort();
  }

  /// xed_reg_enum_t Ar8()::
  static constexpr xed_reg_enum_t Ar8(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_R8W
      case 1:
        return XED_REG_R8W;
        /// EASZ=2 | XED_REG_R8D
      case 2:
        return XED_REG_R8D;
        /// EASZ=3 | XED_REG_R8
      case 3:
        return XED_REG_R8;
    }
    abort();
  }

  /// xed_reg_enum_t Ar9()::
  static constexpr xed_reg_enum_t Ar9(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_R9W
      case 1:
        return XED_REG_R9W;
        /// EASZ=2 | XED_REG_R9D
      case 2:
        return XED_REG_R9D;
        /// EASZ=3 | XED_REG_R9
      case 3:
        return XED_REG_R9;
    }
    abort();
  }

  /// xed_reg_enum_t Ar10()::
  static constexpr xed_reg_enum_t Ar10(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_R10W
      case 1:
        return XED_REG_R10W;
        /// EASZ=2 | XED_REG_R10D
      case 2:
        return XED_REG_R10D;
        /// EASZ=3 | XED_REG_R10
      case 3:
        return XED_REG_R10;
    }
    abort();
  }

  /// xed_reg_enum_t Ar11()::
  static constexpr xed_reg_enum_t Ar11(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_R11W
      case 1:
        return XED_REG_R11W;
        /// EASZ=2 | XED_REG_R11D
      case 2:
        return XED_REG_R11D;
        /// EASZ=3 | XED_REG_R11
      case 3:
        return XED_REG_R11;
    }
    abort();
  }

  /// xed_reg_enum_t Ar12()::
  static constexpr xed_reg_enum_t Ar12(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_R12W
      case 1:
        return XED_REG_R12W;
        /// EASZ=2 | XED_REG_R12D
      case 2:
        return XED_REG_R12D;
        /// EASZ=3 | XED_REG_R12
      case 3:
        return XED_REG_R12;
    }
    abort();
  }

  /// xed_reg_enum_t Ar13()::
  static constexpr xed_reg_enum_t Ar13(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_R13W
      case 1:
        return XED_REG_R13W;
        /// EASZ=2 | XED_REG_R13D
      case 2:
        return XED_REG_R13D;
        /// EASZ=3 | XED_REG_R13
      case 3:
        return XED_REG_R13;
    }
    abort();
  }

  /// xed_reg_enum_t Ar14()::
  static constexpr xed_reg_enum_t Ar14(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_R14W
      case 1:
        return XED_REG_R14W;
        /// EASZ=2 | XED_REG_R14D
      case 2:
        return XED_REG_R14D;
        /// EASZ=3 | XED_REG_R14
      case 3:
        return XED_REG_R14;
    }
    abort();
  }


  /// xed_reg_enum_t Ar15()::
  static constexpr xed_reg_enum_t Ar15(int EASZ) {
    switch (EASZ) {
      /// EASZ=1 | XED_REG_R15W
      case 1:
        return XED_REG_R15W;
        /// EASZ=2 | XED_REG_R15D
      case 2:
        return XED_REG_R15D;
        /// EASZ=3 | XED_REG_R15
      case 3:
        return XED_REG_R15;
    }
    abort();
  }


  /// xed_reg_enum_t rIP()::
  static constexpr xed_reg_enum_t rIP(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | XED_REG_EIP
      case 16:
        return XED_REG_EIP;
        /// cpu_mode=32 | XED_REG_EIP
      case 32:
        return XED_REG_EIP;
        /// cpu_mode=64 | XED_REG_RIP
      case 64:
        return XED_REG_RIP;
    }
    abort();
  }

  /// #######################################################################
  /// # Expand the generic registers using the effective address size EOSZ - limit 32b
  /// #######################################################################

  /// xed_reg_enum_t rIPa()::
  static constexpr xed_reg_enum_t rIPa(int EASZ) {
    switch (EASZ) {
      /// EASZ=2 | XED_REG_EIP
      case 2:
        return XED_REG_EIP;
        /// EASZ=3 | XED_REG_RIP
      case 3:
        return XED_REG_RIP;
    }
    abort();
  }



  /// #######################################################################
  /// # Expand the generic registers using the effective address size EOSZ - limit 64b
  /// #######################################################################

  /// xed_reg_enum_t OeAX()::
  static constexpr xed_reg_enum_t OeAX(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=1 | XED_REG_AX
      case 1:
        return XED_REG_AX;
        /// EOSZ=2 | XED_REG_EAX
      case 2:
        return XED_REG_EAX;
        /// EOSZ=3 | XED_REG_EAX
      case 3:
        return XED_REG_EAX;
    }
    abort();
  }

  /// xed_reg_enum_t OrAX()::
  static constexpr xed_reg_enum_t OrAX(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=1 | XED_REG_AX
      case 1:
        return XED_REG_AX;
        /// EOSZ=2 | XED_REG_EAX
      case 2:
        return XED_REG_EAX;
        /// EOSZ=3 | XED_REG_RAX
      case 3:
        return XED_REG_RAX;
    }
    abort();
  }


  /// # only used for VIA PADLOCK ISA:
  /// xed_reg_enum_t OrDX()::
  static constexpr xed_reg_enum_t OrDX(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=1 | XED_REG_DX
      case 1:
        return XED_REG_DX;
        /// EOSZ=2 | XED_REG_EDX
      case 2:
        return XED_REG_EDX;
        /// EOSZ=3 | XED_REG_RDX
      case 3:
        return XED_REG_RDX;
    }
    abort();
  }

  /// # only used for VIA PADLOCK ISA:
  /// xed_reg_enum_t OrCX()::
  static constexpr xed_reg_enum_t OrCX(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=1 | XED_REG_CX
      case 1:
        return XED_REG_CX;
        /// EOSZ=2 | XED_REG_ECX
      case 2:
        return XED_REG_ECX;
        /// EOSZ=3 | XED_REG_RCX
      case 3:
        return XED_REG_RCX;
    }
    abort();
  }


  /// xed_reg_enum_t OrBX()::
  static constexpr xed_reg_enum_t OrBX(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=1 | XED_REG_BX
      case 1:
        return XED_REG_BX;
        /// EOSZ=2 | XED_REG_EBX
      case 2:
        return XED_REG_EBX;
        /// EOSZ=3 | XED_REG_RBX
      case 3:
        return XED_REG_RBX;
    }
    abort();
  }

  /// xed_reg_enum_t OrSP()::
  static constexpr xed_reg_enum_t OrSP(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=1 | XED_REG_SP
      case 1:
        return XED_REG_SP;
        /// EOSZ=2 | XED_REG_ESP
      case 2:
        return XED_REG_ESP;
        /// EOSZ=3 | XED_REG_RSP
      case 3:
        return XED_REG_RSP;
    }
    abort();
  }



  /// #####################################################

  /// xed_reg_enum_t OrBP()::
  static constexpr xed_reg_enum_t OrBP(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=1 | XED_REG_BP
      case 1:
        return XED_REG_BP;
        /// EOSZ=2 | XED_REG_EBP
      case 2:
        return XED_REG_EBP;
        /// EOSZ=3 | XED_REG_RBP
      case 3:
        return XED_REG_RBP;
    }
    abort();
  }


  /// #####################################################


  /// xed_reg_enum_t rFLAGS()::
  static constexpr xed_reg_enum_t rFLAGS(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | XED_REG_FLAGS
      case 16:
        return XED_REG_FLAGS;
        /// cpu_mode=32 | XED_REG_EFLAGS
      case 32:
        return XED_REG_EFLAGS;
        /// cpu_mode=64 | XED_REG_RFLAGS
      case 64:
        return XED_REG_RFLAGS;
    }
    abort();
  }


  /// xed_reg_enum_t MMX_R()::
  static constexpr xed_reg_enum_t MMX_R_array[] = {
    /// REG=0x0   | XED_REG_MMX0
    XED_REG_MMX0,
    /// REG=0x1   | XED_REG_MMX1
    XED_REG_MMX1,
    /// REG=0x2   | XED_REG_MMX2
    XED_REG_MMX2,
    /// REG=0x3   | XED_REG_MMX3
    XED_REG_MMX3,
    /// REG=0x4   | XED_REG_MMX4
    XED_REG_MMX4,
    /// REG=0x5   | XED_REG_MMX5
    XED_REG_MMX5,
    /// REG=0x6   | XED_REG_MMX6
    XED_REG_MMX6,
    /// REG=0x7   | XED_REG_MMX7
    XED_REG_MMX7,
  };
  static constexpr Register_Class MMX_R = MMX_R_array;


  /// #################################

  /// # Things that scale with effective operand size



  /// # When used as the MODRM.REG register
  /// xed_reg_enum_t MMX_B()::
  static constexpr xed_reg_enum_t MMX_B_array[] = {
    /// RM=0x0   | XED_REG_MMX0
    XED_REG_MMX0,
    /// RM=0x1   | XED_REG_MMX1
    XED_REG_MMX1,
    /// RM=0x2   | XED_REG_MMX2
    XED_REG_MMX2,
    /// RM=0x3   | XED_REG_MMX3
    XED_REG_MMX3,
    /// RM=0x4   | XED_REG_MMX4
    XED_REG_MMX4,
    /// RM=0x5   | XED_REG_MMX5
    XED_REG_MMX5,
    /// RM=0x6   | XED_REG_MMX6
    XED_REG_MMX6,
    /// RM=0x7   | XED_REG_MMX7
    XED_REG_MMX7,
  };
  static constexpr Register_Class MMX_B = MMX_B_array;


  /// xed_reg_enum_t GPRv_R()::
  static constexpr Register_Class GPRv_R(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=3 | GPR64_R()
      case 3:
        return GPR64_R;
        /// EOSZ=2 | GPR32_R()
      case 2:
        return GPR32_R;
        /// EOSZ=1 | GPR16_R()
      case 1:
        return GPR16_R;
    }
    abort();
  }

  /// xed_reg_enum_t GPRv_SB()::
  static constexpr Register_Class GPRv_SB(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=3 | GPR64_SB()
      case 3:
        return GPR64_SB;
        /// EOSZ=2 | GPR32_SB()
      case 2:
        return GPR32_SB;
        /// EOSZ=1 | GPR16_SB()
      case 1:
        return GPR16_SB;
    }
    abort();
  }


  /// # When used as the MOD=11/RM register
  /// xed_reg_enum_t GPRz_R()::
  static constexpr Register_Class GPRz_R(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=3 | GPR32_R()
      case 3:
        return GPR32_R;
        /// EOSZ=2 | GPR32_R()
      case 2:
        return GPR32_R;
        /// EOSZ=1 | GPR16_R()
      case 1:
        return GPR16_R;
    }
    abort();
  }

  /// xed_reg_enum_t GPRv_B()::
  static constexpr Register_Class GPRv_B(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=3 | GPR64_B()
      case 3:
        return GPR64_B;
        /// EOSZ=2 | GPR32_B()
      case 2:
        return GPR32_B;
        /// EOSZ=1 | GPR16_B()
      case 1:
        return GPR16_B;
    }
    abort();
  }

  /// xed_reg_enum_t GPRz_B()::
  static constexpr Register_Class GPRz_B(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=3 | GPR32_B()
      case 3:
        return GPR32_B;
        /// EOSZ=2 | GPR32_B()
      case 2:
        return GPR32_B;
        /// EOSZ=1 | GPR16_B()
      case 1:
        return GPR16_B;
    }
    abort();
  }

  /// xed_reg_enum_t GPRy_B()::
  static constexpr Register_Class GPRy_B(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=3 | GPR64_B()
      case 3:
        return GPR64_B;
        /// EOSZ=2 | GPR32_B()
      case 2:
        return GPR32_B;
        /// EOSZ=1 | GPR32_B()
      case 1:
        return GPR32_B;
    }
    abort();
  }

  /// #####################################

  /// xed_reg_enum_t GPRy_R()::
  static constexpr Register_Class GPRy_R(int EOSZ) {
    switch (EOSZ) {
      /// EOSZ=3 | GPR64_R()
      case 3:
        return GPR64_R;
        /// EOSZ=2 | GPR32_R()
      case 2:
        return GPR32_R;
        /// EOSZ=1 | GPR32_R()
      case 1:
        return GPR32_R;
    }
    abort();
  }

  /// xed_reg_enum_t GPR64_R()::
  static constexpr xed_reg_enum_t GPR64_R_array[] = {
          /// REXR=0 REG=0x0   | XED_REG_RAX
          XED_REG_RAX,
          /// REXR=0 REG=0x1   | XED_REG_RCX
          XED_REG_RCX,
          /// REXR=0 REG=0x2   | XED_REG_RDX
          XED_REG_RDX,
          /// REXR=0 REG=0x3   | XED_REG_RBX
          XED_REG_RBX,
          /// REXR=0 REG=0x4   | XED_REG_RSP
          XED_REG_RSP,
          /// REXR=0 REG=0x5   | XED_REG_RBP
          XED_REG_RBP,
          /// REXR=0 REG=0x6   | XED_REG_RSI
          XED_REG_RSI,
          /// REXR=0 REG=0x7   | XED_REG_RDI
          XED_REG_RDI,
          /// REXR=1 REG=0x0   | XED_REG_R8
          XED_REG_R8,
          /// REXR=1 REG=0x1   | XED_REG_R9
          XED_REG_R9,
          /// REXR=1 REG=0x2   | XED_REG_R10
          XED_REG_R10,
          /// REXR=1 REG=0x3   | XED_REG_R11
          XED_REG_R11,
          /// REXR=1 REG=0x4   | XED_REG_R12
          XED_REG_R12,
          /// REXR=1 REG=0x5   | XED_REG_R13
          XED_REG_R13,
          /// REXR=1 REG=0x6   | XED_REG_R14
          XED_REG_R14,
          /// REXR=1 REG=0x7   | XED_REG_R15
          XED_REG_R15,
  };
  static constexpr Register_Class GPR64_R = GPR64_R_array;


  /// xed_reg_enum_t GPR64_B()::
  static constexpr xed_reg_enum_t GPR64_B_array[] = {
          /// REXB=0 RM=0x0   | XED_REG_RAX
          XED_REG_RAX,
          /// REXB=0 RM=0x1   | XED_REG_RCX
          XED_REG_RCX,
          /// REXB=0 RM=0x2   | XED_REG_RDX
          XED_REG_RDX,
          /// REXB=0 RM=0x3   | XED_REG_RBX
          XED_REG_RBX,
          /// REXB=0 RM=0x4   | XED_REG_RSP
          XED_REG_RSP,
          /// REXB=0 RM=0x5   | XED_REG_RBP
          XED_REG_RBP,
          /// REXB=0 RM=0x6   | XED_REG_RSI
          XED_REG_RSI,
          /// REXB=0 RM=0x7   | XED_REG_RDI
          XED_REG_RDI,
          /// REXB=1 RM=0x0   | XED_REG_R8
          XED_REG_R8,
          /// REXB=1 RM=0x1   | XED_REG_R9
          XED_REG_R9,
          /// REXB=1 RM=0x2   | XED_REG_R10
          XED_REG_R10,
          /// REXB=1 RM=0x3   | XED_REG_R11
          XED_REG_R11,
          /// REXB=1 RM=0x4   | XED_REG_R12
          XED_REG_R12,
          /// REXB=1 RM=0x5   | XED_REG_R13
          XED_REG_R13,
          /// REXB=1 RM=0x6   | XED_REG_R14
          XED_REG_R14,
          /// REXB=1 RM=0x7   | XED_REG_R15
          XED_REG_R15,
  };
  static constexpr Register_Class GPR64_B = GPR64_B_array;

  /// xed_reg_enum_t GPR64_SB()::
  static constexpr xed_reg_enum_t GPR64_SB_array[] = {
          /// REXB=0 SRM=0x0   | XED_REG_RAX
          XED_REG_RAX,
          /// REXB=0 SRM=0x1   | XED_REG_RCX
          XED_REG_RCX,
          /// REXB=0 SRM=0x2   | XED_REG_RDX
          XED_REG_RDX,
          /// REXB=0 SRM=0x3   | XED_REG_RBX
          XED_REG_RBX,
          /// REXB=0 SRM=0x4   | XED_REG_RSP
          XED_REG_RSP,
          /// REXB=0 SRM=0x5   | XED_REG_RBP
          XED_REG_RBP,
          /// REXB=0 SRM=0x6   | XED_REG_RSI
          XED_REG_RSI,
          /// REXB=0 SRM=0x7   | XED_REG_RDI
          XED_REG_RDI,
          /// REXB=1 SRM=0x0   | XED_REG_R8
          XED_REG_R8,
          /// REXB=1 SRM=0x1   | XED_REG_R9
          XED_REG_R9,
          /// REXB=1 SRM=0x2   | XED_REG_R10
          XED_REG_R10,
          /// REXB=1 SRM=0x3   | XED_REG_R11
          XED_REG_R11,
          /// REXB=1 SRM=0x4   | XED_REG_R12
          XED_REG_R12,
          /// REXB=1 SRM=0x5   | XED_REG_R13
          XED_REG_R13,
          /// REXB=1 SRM=0x6   | XED_REG_R14
          XED_REG_R14,
          /// REXB=1 SRM=0x7   | XED_REG_R15
          XED_REG_R15,
  };
  static constexpr Register_Class GPR64_SB = GPR64_SB_array;

  /// #################################

  /// xed_reg_enum_t GPR32_R()::
  static constexpr xed_reg_enum_t GPR32_R_array[] = {
          /// REXR=0 REG=0x0   | XED_REG_EAX
          XED_REG_EAX,
          /// REXR=0 REG=0x1   | XED_REG_ECX
          XED_REG_ECX,
          /// REXR=0 REG=0x2   | XED_REG_EDX
          XED_REG_EDX,
          /// REXR=0 REG=0x3   | XED_REG_EBX
          XED_REG_EBX,
          /// REXR=0 REG=0x4   | XED_REG_ESP
          XED_REG_ESP,
          /// REXR=0 REG=0x5   | XED_REG_EBP
          XED_REG_EBP,
          /// REXR=0 REG=0x6   | XED_REG_ESI
          XED_REG_ESI,
          /// REXR=0 REG=0x7   | XED_REG_EDI
          XED_REG_EDI,
          /// REXR=1 REG=0x0   | XED_REG_R8D
          XED_REG_R8D,
          /// REXR=1 REG=0x1   | XED_REG_R9D
          XED_REG_R9D,
          /// REXR=1 REG=0x2   | XED_REG_R10D
          XED_REG_R10D,
          /// REXR=1 REG=0x3   | XED_REG_R11D
          XED_REG_R11D,
          /// REXR=1 REG=0x4   | XED_REG_R12D
          XED_REG_R12D,
          /// REXR=1 REG=0x5   | XED_REG_R13D
          XED_REG_R13D,
          /// REXR=1 REG=0x6   | XED_REG_R14D
          XED_REG_R14D,
          /// REXR=1 REG=0x7   | XED_REG_R15D
          XED_REG_R15D,
  };
  static constexpr Register_Class GPR32_R = GPR32_R_array;


  /// xed_reg_enum_t GPR32_B()::
  static constexpr xed_reg_enum_t GPR32_B_array[] = {
          /// REXB=0 RM=0x0   | XED_REG_EAX
          XED_REG_EAX,
          /// REXB=0 RM=0x1   | XED_REG_ECX
          XED_REG_ECX,
          /// REXB=0 RM=0x2   | XED_REG_EDX
          XED_REG_EDX,
          /// REXB=0 RM=0x3   | XED_REG_EBX
          XED_REG_EBX,
          /// REXB=0 RM=0x4   | XED_REG_ESP
          XED_REG_ESP,
          /// REXB=0 RM=0x5   | XED_REG_EBP
          XED_REG_EBP,
          /// REXB=0 RM=0x6   | XED_REG_ESI
          XED_REG_ESI,
          /// REXB=0 RM=0x7   | XED_REG_EDI
          XED_REG_EDI,
          /// REXB=1 RM=0x0   | XED_REG_R8D
          XED_REG_R8D,
          /// REXB=1 RM=0x1   | XED_REG_R9D
          XED_REG_R9D,
          /// REXB=1 RM=0x2   | XED_REG_R10D
          XED_REG_R10D,
          /// REXB=1 RM=0x3   | XED_REG_R11D
          XED_REG_R11D,
          /// REXB=1 RM=0x4   | XED_REG_R12D
          XED_REG_R12D,
          /// REXB=1 RM=0x5   | XED_REG_R13D
          XED_REG_R13D,
          /// REXB=1 RM=0x6   | XED_REG_R14D
          XED_REG_R14D,
          /// REXB=1 RM=0x7   | XED_REG_R15D
          XED_REG_R15D,
  };
  static constexpr Register_Class GPR32_B = GPR32_B_array;

  /// xed_reg_enum_t GPR32_SB()::
  static constexpr xed_reg_enum_t GPR32_SB_array[] = {
          /// REXB=0 SRM=0x0   | XED_REG_EAX
          XED_REG_EAX,
          /// REXB=0 SRM=0x1   | XED_REG_ECX
          XED_REG_ECX,
          /// REXB=0 SRM=0x2   | XED_REG_EDX
          XED_REG_EDX,
          /// REXB=0 SRM=0x3   | XED_REG_EBX
          XED_REG_EBX,
          /// REXB=0 SRM=0x4   | XED_REG_ESP
          XED_REG_ESP,
          /// REXB=0 SRM=0x5   | XED_REG_EBP
          XED_REG_EBP,
          /// REXB=0 SRM=0x6   | XED_REG_ESI
          XED_REG_ESI,
          /// REXB=0 SRM=0x7   | XED_REG_EDI
          XED_REG_EDI,
          /// REXB=1 SRM=0x0   | XED_REG_R8D
          XED_REG_R8D,
          /// REXB=1 SRM=0x1   | XED_REG_R9D
          XED_REG_R9D,
          /// REXB=1 SRM=0x2   | XED_REG_R10D
          XED_REG_R10D,
          /// REXB=1 SRM=0x3   | XED_REG_R11D
          XED_REG_R11D,
          /// REXB=1 SRM=0x4   | XED_REG_R12D
          XED_REG_R12D,
          /// REXB=1 SRM=0x5   | XED_REG_R13D
          XED_REG_R13D,
          /// REXB=1 SRM=0x6   | XED_REG_R14D
          XED_REG_R14D,
          /// REXB=1 SRM=0x7   | XED_REG_R15D
          XED_REG_R15D,
  };
  static constexpr Register_Class GPR32_SB = GPR32_SB_array;

  /// #############################

  /// xed_reg_enum_t GPR16_R()::
  static constexpr xed_reg_enum_t GPR16_R_array[] = {
          /// REXR=0 REG=0x0   | XED_REG_AX
          XED_REG_AX,
          /// REXR=0 REG=0x1   | XED_REG_CX
          XED_REG_CX,
          /// REXR=0 REG=0x2   | XED_REG_DX
          XED_REG_DX,
          /// REXR=0 REG=0x3   | XED_REG_BX
          XED_REG_BX,
          /// REXR=0 REG=0x4   | XED_REG_SP
          XED_REG_SP,
          /// REXR=0 REG=0x5   | XED_REG_BP
          XED_REG_BP,
          /// REXR=0 REG=0x6   | XED_REG_SI
          XED_REG_SI,
          /// REXR=0 REG=0x7   | XED_REG_DI
          XED_REG_DI,
          /// REXR=1 REG=0x0   | XED_REG_R8W
          XED_REG_R8W,
          /// REXR=1 REG=0x1   | XED_REG_R9W
          XED_REG_R9W,
          /// REXR=1 REG=0x2   | XED_REG_R10W
          XED_REG_R10W,
          /// REXR=1 REG=0x3   | XED_REG_R11W
          XED_REG_R11W,
          /// REXR=1 REG=0x4   | XED_REG_R12W
          XED_REG_R12W,
          /// REXR=1 REG=0x5   | XED_REG_R13W
          XED_REG_R13W,
          /// REXR=1 REG=0x6   | XED_REG_R14W
          XED_REG_R14W,
          /// REXR=1 REG=0x7   | XED_REG_R15W
          XED_REG_R15W,
  };
  static constexpr Register_Class GPR16_R = GPR16_R_array;


  /// xed_reg_enum_t GPR16_B()::
  static constexpr xed_reg_enum_t GPR16_B_array[] = {
          /// REXB=0 RM=0x0   | XED_REG_AX
          XED_REG_AX,
          /// REXB=0 RM=0x1   | XED_REG_CX
          XED_REG_CX,
          /// REXB=0 RM=0x2   | XED_REG_DX
          XED_REG_DX,
          /// REXB=0 RM=0x3   | XED_REG_BX
          XED_REG_BX,
          /// REXB=0 RM=0x4   | XED_REG_SP
          XED_REG_SP,
          /// REXB=0 RM=0x5   | XED_REG_BP
          XED_REG_BP,
          /// REXB=0 RM=0x6   | XED_REG_SI
          XED_REG_SI,
          /// REXB=0 RM=0x7   | XED_REG_DI
          XED_REG_DI,
          /// REXB=1 RM=0x0   | XED_REG_R8W
          XED_REG_R8W,
          /// REXB=1 RM=0x1   | XED_REG_R9W
          XED_REG_R9W,
          /// REXB=1 RM=0x2   | XED_REG_R10W
          XED_REG_R10W,
          /// REXB=1 RM=0x3   | XED_REG_R11W
          XED_REG_R11W,
          /// REXB=1 RM=0x4   | XED_REG_R12W
          XED_REG_R12W,
          /// REXB=1 RM=0x5   | XED_REG_R13W
          XED_REG_R13W,
          /// REXB=1 RM=0x6   | XED_REG_R14W
          XED_REG_R14W,
          /// REXB=1 RM=0x7   | XED_REG_R15W
          XED_REG_R15W,
  };
  static constexpr Register_Class GPR16_B = GPR16_B_array;


  /// xed_reg_enum_t GPR16_SB()::
  static constexpr xed_reg_enum_t GPR16_SB_array[] = {
          /// REXB=0 SRM=0x0   | XED_REG_AX
          XED_REG_AX,
          /// REXB=0 SRM=0x1   | XED_REG_CX
          XED_REG_CX,
          /// REXB=0 SRM=0x2   | XED_REG_DX
          XED_REG_DX,
          /// REXB=0 SRM=0x3   | XED_REG_BX
          XED_REG_BX,
          /// REXB=0 SRM=0x4   | XED_REG_SP
          XED_REG_SP,
          /// REXB=0 SRM=0x5   | XED_REG_BP
          XED_REG_BP,
          /// REXB=0 SRM=0x6   | XED_REG_SI
          XED_REG_SI,
          /// REXB=0 SRM=0x7   | XED_REG_DI
          XED_REG_DI,
          /// REXB=1 SRM=0x0   | XED_REG_R8W
          XED_REG_R8W,
          /// REXB=1 SRM=0x1   | XED_REG_R9W
          XED_REG_R9W,
          /// REXB=1 SRM=0x2   | XED_REG_R10W
          XED_REG_R10W,
          /// REXB=1 SRM=0x3   | XED_REG_R11W
          XED_REG_R11W,
          /// REXB=1 SRM=0x4   | XED_REG_R12W
          XED_REG_R12W,
          /// REXB=1 SRM=0x5   | XED_REG_R13W
          XED_REG_R13W,
          /// REXB=1 SRM=0x6   | XED_REG_R14W
          XED_REG_R14W,
          /// REXB=1 SRM=0x7   | XED_REG_R15W
          XED_REG_R15W,
  };
  static constexpr Register_Class GPR16_SB = GPR16_SB_array;

  /// xed_reg_enum_t GPR8_R()::
  static constexpr xed_reg_enum_t GPR8_R_REX0_array[] = {
    /// REXR=0 REG=0x0   | XED_REG_AL
    XED_REG_AL,
    /// REXR=0 REG=0x1   | XED_REG_CL
    XED_REG_CL,
    /// REXR=0 REG=0x2   | XED_REG_DL
    XED_REG_DL,
    /// REXR=0 REG=0x3   | XED_REG_BL
    XED_REG_BL,
    /// REXR=0 REG=0x4  REX=0    | XED_REG_AH
    XED_REG_AH,
    /// REXR=0 REG=0x5  REX=0    | XED_REG_CH
    XED_REG_CH,
    /// REXR=0 REG=0x6  REX=0    | XED_REG_DH
    XED_REG_DH,
    /// REXR=0 REG=0x7  REX=0    | XED_REG_BH
    XED_REG_BH,
  };
  static constexpr Register_Class GPR8_R_REX0 = GPR8_R_REX0_array;

  static constexpr xed_reg_enum_t GPR8_R_REX1_array[] = {
    /// REXR=0 REG=0x0   | XED_REG_AL
    XED_REG_AL,
    /// REXR=0 REG=0x1   | XED_REG_CL
    XED_REG_CL,
    /// REXR=0 REG=0x2   | XED_REG_DL
    XED_REG_DL,
    /// REXR=0 REG=0x3   | XED_REG_BL
    XED_REG_BL,
    /// REXR=0 REG=0x4  REX=1    | XED_REG_SPL
    XED_REG_SPL,
    /// REXR=0 REG=0x5  REX=1    | XED_REG_BPL
    XED_REG_BPL,
    /// REXR=0 REG=0x6  REX=1    | XED_REG_SIL
    XED_REG_SIL,
    /// REXR=0 REG=0x7  REX=1    | XED_REG_DIL
    XED_REG_DIL,
    /// REXR=1 REG=0x0   | XED_REG_R8B
    XED_REG_R8B,
    /// REXR=1 REG=0x1   | XED_REG_R9B
    XED_REG_R9B,
    /// REXR=1 REG=0x2   | XED_REG_R10B
    XED_REG_R10B,
    /// REXR=1 REG=0x3   | XED_REG_R11B
    XED_REG_R11B,
    /// REXR=1 REG=0x4   | XED_REG_R12B
    XED_REG_R12B,
    /// REXR=1 REG=0x5   | XED_REG_R13B
    XED_REG_R13B,
    /// REXR=1 REG=0x6   | XED_REG_R14B
    XED_REG_R14B,
    /// REXR=1 REG=0x7   | XED_REG_R15B
    XED_REG_R15B,
  };
  static constexpr Register_Class GPR8_R_REX1 = GPR8_R_REX1_array;

  static constexpr Register_Class GPR8_R(bool allow_rex) {
    return allow_rex ? GPR8_R_REX1 : GPR8_R_REX0;
  }

  /// xed_reg_enum_t GPR8_B()::
  static constexpr xed_reg_enum_t GPR8_B_REX0_array[] = {
    /// REXB=0 RM=0x0   | XED_REG_AL
    XED_REG_AL,
    /// REXB=0 RM=0x1   | XED_REG_CL
    XED_REG_CL,
    /// REXB=0 RM=0x2   | XED_REG_DL
    XED_REG_DL,
    /// REXB=0 RM=0x3   | XED_REG_BL
    XED_REG_BL,
    /// REXB=0 RM=0x4  REX=0   | XED_REG_AH
    XED_REG_AH,
    /// REXB=0 RM=0x5  REX=0   | XED_REG_CH
    XED_REG_CH,
    /// REXB=0 RM=0x6  REX=0   | XED_REG_DH
    XED_REG_DH,
    /// REXB=0 RM=0x7  REX=0   | XED_REG_BH
    XED_REG_BH,
  };
  static constexpr Register_Class GPR8_B_REX0 = GPR8_B_REX0_array;

  /// xed_reg_enum_t GPR8_B()::
  static constexpr xed_reg_enum_t GPR8_B_REX1_array[] = {
    /// REXB=0 RM=0x0   | XED_REG_AL
    XED_REG_AL,
    /// REXB=0 RM=0x1   | XED_REG_CL
    XED_REG_CL,
    /// REXB=0 RM=0x2   | XED_REG_DL
    XED_REG_DL,
    /// REXB=0 RM=0x3   | XED_REG_BL
    XED_REG_BL,
    /// REXB=0 RM=0x4  REX=1   | XED_REG_SPL
    XED_REG_SPL,
    /// REXB=0 RM=0x5  REX=1   | XED_REG_BPL
    XED_REG_BPL,
    /// REXB=0 RM=0x6  REX=1   | XED_REG_SIL
    XED_REG_SIL,
    /// REXB=0 RM=0x7  REX=1   | XED_REG_DIL
    XED_REG_DIL,
    /// REXB=1 RM=0x0   | XED_REG_R8B
    XED_REG_R8B,
    /// REXB=1 RM=0x1   | XED_REG_R9B
    XED_REG_R9B,
    /// REXB=1 RM=0x2   | XED_REG_R10B
    XED_REG_R10B,
    /// REXB=1 RM=0x3   | XED_REG_R11B
    XED_REG_R11B,
    /// REXB=1 RM=0x4   | XED_REG_R12B
    XED_REG_R12B,
    /// REXB=1 RM=0x5   | XED_REG_R13B
    XED_REG_R13B,
    /// REXB=1 RM=0x6   | XED_REG_R14B
    XED_REG_R14B,
    /// REXB=1 RM=0x7   | XED_REG_R15B
    XED_REG_R15B,
  };
  static constexpr Register_Class GPR8_B_REX1 = GPR8_B_REX1_array;

  /// xed_reg_enum_t GPR8_B()::
  static constexpr Register_Class GPR8_B(bool allow_rex) {
    return allow_rex ? GPR8_B_REX1 : GPR8_B_REX0;
  }

  /// xed_reg_enum_t GPR8_SB()::
  static constexpr xed_reg_enum_t GPR8_SB_REX0_array[] = {
    /// REXB=0 SRM=0x0   | XED_REG_AL
    XED_REG_AL,
    /// REXB=0 SRM=0x1   | XED_REG_CL
    XED_REG_CL,
    /// REXB=0 SRM=0x2   | XED_REG_DL
    XED_REG_DL,
    /// REXB=0 SRM=0x3   | XED_REG_BL
    XED_REG_BL,
    /// REXB=0 SRM=0x4  REX=0   | XED_REG_AH
    XED_REG_AH,
    /// REXB=0 SRM=0x5  REX=0   | XED_REG_CH
    XED_REG_CH,
    /// REXB=0 SRM=0x6  REX=0   | XED_REG_DH
    XED_REG_DH,
    /// REXB=0 SRM=0x7  REX=0   | XED_REG_BH
    XED_REG_BH,
  };
  static constexpr Register_Class GPR8_SB_REX0 = GPR8_SB_REX0_array;

  /// xed_reg_enum_t GPR8_SB()::
  static constexpr xed_reg_enum_t GPR8_SB_REX1_array[] = {
    /// REXB=0 SRM=0x0   | XED_REG_AL
    XED_REG_AL,
    /// REXB=0 SRM=0x1   | XED_REG_CL
    XED_REG_CL,
    /// REXB=0 SRM=0x2   | XED_REG_DL
    XED_REG_DL,
    /// REXB=0 SRM=0x3   | XED_REG_BL
    XED_REG_BL,
    /// REXB=0 SRM=0x4  REX=1   | XED_REG_SPL
    XED_REG_SPL,
    /// REXB=0 SRM=0x5  REX=1   | XED_REG_BPL
    XED_REG_BPL,
    /// REXB=0 SRM=0x6  REX=1   | XED_REG_SIL
    XED_REG_SIL,
    /// REXB=0 SRM=0x7  REX=1   | XED_REG_DIL
    XED_REG_DIL,
    /// REXB=1 SRM=0x0   | XED_REG_R8B
    XED_REG_R8B,
    /// REXB=1 SRM=0x1   | XED_REG_R9B
    XED_REG_R9B,
    /// REXB=1 SRM=0x2   | XED_REG_R10B
    XED_REG_R10B,
    /// REXB=1 SRM=0x3   | XED_REG_R11B
    XED_REG_R11B,
    /// REXB=1 SRM=0x4   | XED_REG_R12B
    XED_REG_R12B,
    /// REXB=1 SRM=0x5   | XED_REG_R13B
    XED_REG_R13B,
    /// REXB=1 SRM=0x6   | XED_REG_R14B
    XED_REG_R14B,
    /// REXB=1 SRM=0x7   | XED_REG_R15B
    XED_REG_R15B,
  };
  static constexpr Register_Class GPR8_SB_REX1 = GPR8_SB_REX1_array;

  /// xed_reg_enum_t GPR8_SB()::
  static constexpr Register_Class GPR8_SB(bool allow_rex) {
    return allow_rex ? GPR8_SB_REX1 : GPR8_SB_REX0;
  }

  /// xed_reg_enum_t A_GPR_R()::
  static constexpr xed_reg_enum_t A_GPR_R_array[] = {
    /// REXR=0 REG=0x1  | OUTREG=ArCX()
    XED_REG_CX,
    /// REXR=0 REG=0x2  | OUTREG=ArDX()
    XED_REG_DX,
    /// REXR=0 REG=0x3  | OUTREG=ArBX()
    XED_REG_BX,
    /// REXR=0 REG=0x4  | OUTREG=ArSP()
    XED_REG_SP,
    /// REXR=0 REG=0x5  | OUTREG=ArBP()
    XED_REG_BP,
    /// REXR=0 REG=0x6  | OUTREG=ArSI()
    XED_REG_SI,
    /// REXR=0 REG=0x7  | OUTREG=ArDI()
    XED_REG_DI,
    /// REXR=1 REG=0x0  | OUTREG=Ar8()
    XED_REG_R8,
    /// REXR=1 REG=0x1  | OUTREG=Ar9()
    XED_REG_R9,
    /// REXR=1 REG=0x2  | OUTREG=Ar10()
    XED_REG_R10,
    /// REXR=1 REG=0x3  | OUTREG=Ar11()
    XED_REG_R11,
    /// REXR=1 REG=0x4  | OUTREG=Ar12()
    XED_REG_R12,
    /// REXR=1 REG=0x5  | OUTREG=Ar13()
    XED_REG_R13,
    /// REXR=1 REG=0x6  | OUTREG=Ar14()
    XED_REG_R14,
    /// REXR=1 REG=0x7  | OUTREG=Ar15()
    XED_REG_R15,
  };

  static constexpr std::optional<Register_Class> A_GPR_R(bool allow_rex) {
    if (allow_rex) { return Register_Class{A_GPR_R_array}; }
    return std::nullopt;
  }

  /// xed_reg_enum_t A_GPR_B()::
  static constexpr xed_reg_enum_t A_GPR_B_array[] = {
    /// REXB=0 RM=0x0  | OUTREG=ArAX()
    XED_REG_AX,
    /// REXB=0 RM=0x1  | OUTREG=ArCX()
    XED_REG_CX,
    /// REXB=0 RM=0x2  | OUTREG=ArDX()
    XED_REG_DX,
    /// REXB=0 RM=0x3  | OUTREG=ArBX()
    XED_REG_BX,
    /// REXB=0 RM=0x4  | OUTREG=ArSP()
    XED_REG_SP,
    /// REXB=0 RM=0x5  | OUTREG=ArBP()
    XED_REG_BP,
    /// REXB=0 RM=0x6  | OUTREG=ArSI()
    XED_REG_SI,
    /// REXB=0 RM=0x7  | OUTREG=ArDI()
    XED_REG_DI,
    /// REXB=1 RM=0x0  | OUTREG=Ar8()
    XED_REG_R8,
    /// REXB=1 RM=0x1  | OUTREG=Ar9()
    XED_REG_R9,
    /// REXB=1 RM=0x2  | OUTREG=Ar10()
    XED_REG_R10,
    /// REXB=1 RM=0x3  | OUTREG=Ar11()
    XED_REG_R11,
    /// REXB=1 RM=0x4  | OUTREG=Ar12()
    XED_REG_R12,
    /// REXB=1 RM=0x5  | OUTREG=Ar13()
    XED_REG_R13,
    /// REXB=1 RM=0x6  | OUTREG=Ar14()
    XED_REG_R14,
    /// REXB=1 RM=0x7  | OUTREG=Ar15()
    XED_REG_R15,
  };

  static constexpr std::optional<Register_Class> A_GPR_B(bool allow_rex) {
    if (allow_rex) { return Register_Class{A_GPR_B_array}; }
    return std::nullopt;
  }

  /// xed_reg_enum_t X87()::
  static constexpr xed_reg_enum_t X87_array[] = {
          /// RM=0x0   | XED_REG_ST0
          XED_REG_ST0,
          /// RM=0x1   | XED_REG_ST1
          XED_REG_ST1,
          /// RM=0x2   | XED_REG_ST2
          XED_REG_ST2,
          /// RM=0x3   | XED_REG_ST3
          XED_REG_ST3,
          /// RM=0x4   | XED_REG_ST4
          XED_REG_ST4,
          /// RM=0x5   | XED_REG_ST5
          XED_REG_ST5,
          /// RM=0x6   | XED_REG_ST6
          XED_REG_ST6,
          /// RM=0x7   | XED_REG_ST7
          XED_REG_ST7,
  };
  static constexpr Register_Class X87 = X87_array;

  /// xed_reg_enum_t XMM_R()::
  static constexpr Register_Class XMM_R(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | XMM_R_32()
      case 16:
        return XMM_R_32;
        /// cpu_mode=32 | XMM_R_32()
      case 32:
        return XMM_R_32;
        /// cpu_mode=64 | XMM_R_64()
      case 64:
        return XMM_R_64;
    }
    abort();
  }


  /// xed_reg_enum_t XMM_R_32()::
  static constexpr xed_reg_enum_t XMM_R_32_array[] = {
          /// REG=0x0   | XED_REG_XMM0
          XED_REG_XMM0,
          /// REG=0x1   | XED_REG_XMM1
          XED_REG_XMM1,
          /// REG=0x2   | XED_REG_XMM2
          XED_REG_XMM2,
          /// REG=0x3   | XED_REG_XMM3
          XED_REG_XMM3,
          /// REG=0x4   | XED_REG_XMM4
          XED_REG_XMM4,
          /// REG=0x5   | XED_REG_XMM5
          XED_REG_XMM5,
          /// REG=0x6   | XED_REG_XMM6
          XED_REG_XMM6,
          /// REG=0x7   | XED_REG_XMM7
          XED_REG_XMM7,
  };
  static constexpr Register_Class XMM_R_32 = XMM_R_32_array;

  /// xed_reg_enum_t XMM_R_64()::
  static constexpr xed_reg_enum_t XMM_R_64_array[] = {
          /// REXR=0 REG=0x0   | XED_REG_XMM0
          XED_REG_XMM0,
          /// REXR=0 REG=0x1   | XED_REG_XMM1
          XED_REG_XMM1,
          /// REXR=0 REG=0x2   | XED_REG_XMM2
          XED_REG_XMM2,
          /// REXR=0 REG=0x3   | XED_REG_XMM3
          XED_REG_XMM3,
          /// REXR=0 REG=0x4   | XED_REG_XMM4
          XED_REG_XMM4,
          /// REXR=0 REG=0x5   | XED_REG_XMM5
          XED_REG_XMM5,
          /// REXR=0 REG=0x6   | XED_REG_XMM6
          XED_REG_XMM6,
          /// REXR=0 REG=0x7   | XED_REG_XMM7
          XED_REG_XMM7,
          /// REXR=1 REG=0x0   | XED_REG_XMM8
          XED_REG_XMM8,
          /// REXR=1 REG=0x1   | XED_REG_XMM9
          XED_REG_XMM9,
          /// REXR=1 REG=0x2   | XED_REG_XMM10
          XED_REG_XMM10,
          /// REXR=1 REG=0x3   | XED_REG_XMM11
          XED_REG_XMM11,
          /// REXR=1 REG=0x4   | XED_REG_XMM12
          XED_REG_XMM12,
          /// REXR=1 REG=0x5   | XED_REG_XMM13
          XED_REG_XMM13,
          /// REXR=1 REG=0x6   | XED_REG_XMM14
          XED_REG_XMM14,
          /// REXR=1 REG=0x7   | XED_REG_XMM15
          XED_REG_XMM15,
  };
  static constexpr Register_Class XMM_R_64 = XMM_R_64_array;


  /// xed_reg_enum_t XMM_B()::
  static constexpr Register_Class XMM_B(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | XMM_B_32()
      case 16:
        return XMM_B_32;
        /// cpu_mode=32 | XMM_B_32()
      case 32:
        return XMM_B_32;
        /// cpu_mode=64 | XMM_B_64()
      case 64:
        return XMM_B_64;
    }
    abort();
  }

  /// xed_reg_enum_t XMM_B_32()::
  static constexpr xed_reg_enum_t XMM_B_32_array[] = {
          /// RM=0x0   | XED_REG_XMM0
          XED_REG_XMM0,
          /// RM=0x1   | XED_REG_XMM1
          XED_REG_XMM1,
          /// RM=0x2   | XED_REG_XMM2
          XED_REG_XMM2,
          /// RM=0x3   | XED_REG_XMM3
          XED_REG_XMM3,
          /// RM=0x4   | XED_REG_XMM4
          XED_REG_XMM4,
          /// RM=0x5   | XED_REG_XMM5
          XED_REG_XMM5,
          /// RM=0x6   | XED_REG_XMM6
          XED_REG_XMM6,
          /// RM=0x7   | XED_REG_XMM7
          XED_REG_XMM7,
  };
  static constexpr Register_Class XMM_B_32 = XMM_B_32_array;

  /// xed_reg_enum_t XMM_B_64()::
  static constexpr xed_reg_enum_t XMM_B_64_array[] = {
          /// REXB=0 RM=0x0   | XED_REG_XMM0
          XED_REG_XMM0,
          /// REXB=0 RM=0x1   | XED_REG_XMM1
          XED_REG_XMM1,
          /// REXB=0 RM=0x2   | XED_REG_XMM2
          XED_REG_XMM2,
          /// REXB=0 RM=0x3   | XED_REG_XMM3
          XED_REG_XMM3,
          /// REXB=0 RM=0x4   | XED_REG_XMM4
          XED_REG_XMM4,
          /// REXB=0 RM=0x5   | XED_REG_XMM5
          XED_REG_XMM5,
          /// REXB=0 RM=0x6   | XED_REG_XMM6
          XED_REG_XMM6,
          /// REXB=0 RM=0x7   | XED_REG_XMM7
          XED_REG_XMM7,
          /// REXB=1 RM=0x0   | XED_REG_XMM8
          XED_REG_XMM8,
          /// REXB=1 RM=0x1   | XED_REG_XMM9
          XED_REG_XMM9,
          /// REXB=1 RM=0x2   | XED_REG_XMM10
          XED_REG_XMM10,
          /// REXB=1 RM=0x3   | XED_REG_XMM11
          XED_REG_XMM11,
          /// REXB=1 RM=0x4   | XED_REG_XMM12
          XED_REG_XMM12,
          /// REXB=1 RM=0x5   | XED_REG_XMM13
          XED_REG_XMM13,
          /// REXB=1 RM=0x6   | XED_REG_XMM14
          XED_REG_XMM14,
          /// REXB=1 RM=0x7   | XED_REG_XMM15
          XED_REG_XMM15,
  };
  static constexpr Register_Class XMM_B_64 = XMM_B_64_array;

  /// xed_reg_enum_t VGPR32_B()::
  static constexpr Register_Class VGPR32_B(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | VGPR32_B_32
      case 16: return VGPR32_B_32;
        /// cpu_mode=32 | VGPR32_B_32
      case 32: return VGPR32_B_32;
        /// cpu_mode=64 | VGPR32_B_64
      case 64: return VGPR32_B_64;
    }
    abort();
  }

  /// xed_reg_enum_t VGPR32_R()::
  static constexpr Register_Class VGPR32_R(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | VGPR32_R_32
      case 16: return VGPR32_R_32;
        /// cpu_mode=32 | VGPR32_R_32
      case 32: return VGPR32_R_32;
        /// cpu_mode=64 | VGPR32_R_64
      case 64: return VGPR32_R_64;
    }
    abort();
  }

  /// xed_reg_enum_t VGPR32_N_32()::
  static constexpr xed_reg_enum_t VGPR32_N_32_array[] = {
          /// VEXDEST210=7    | XED_REG_EAX
          XED_REG_EAX,
          /// VEXDEST210=6    | XED_REG_ECX
          XED_REG_ECX,
          /// VEXDEST210=5    | XED_REG_EDX
          XED_REG_EDX,
          /// VEXDEST210=4    | XED_REG_EBX
          XED_REG_EBX,
          /// VEXDEST210=3    | XED_REG_ESP
          XED_REG_ESP,
          /// VEXDEST210=2    | XED_REG_EBP
          XED_REG_EBP,
          /// VEXDEST210=1    | XED_REG_ESI
          XED_REG_ESI,
          /// VEXDEST210=0    | XED_REG_EDI
          XED_REG_EDI,
  };
  static constexpr Register_Class VGPR32_N_32 = VGPR32_N_32_array;

  /// xed_reg_enum_t VGPR32_N_64()::
  static constexpr xed_reg_enum_t VGPR32_N_64_array[] = {
          /// VEXDEST3=1 VEXDEST210=7    | XED_REG_EAX
          XED_REG_EAX,
          /// VEXDEST3=1 VEXDEST210=6    | XED_REG_ECX
          XED_REG_ECX,
          /// VEXDEST3=1 VEXDEST210=5    | XED_REG_EDX
          XED_REG_EDX,
          /// VEXDEST3=1 VEXDEST210=4    | XED_REG_EBX
          XED_REG_EBX,
          /// VEXDEST3=1 VEXDEST210=3    | XED_REG_ESP
          XED_REG_ESP,
          /// VEXDEST3=1 VEXDEST210=2    | XED_REG_EBP
          XED_REG_EBP,
          /// VEXDEST3=1 VEXDEST210=1    | XED_REG_ESI
          XED_REG_ESI,
          /// VEXDEST3=1 VEXDEST210=0    | XED_REG_EDI
          XED_REG_EDI,
          /// VEXDEST3=0 VEXDEST210=7    | XED_REG_R8D
          XED_REG_R8D,
          /// VEXDEST3=0 VEXDEST210=6    | XED_REG_R9D
          XED_REG_R9D,
          /// VEXDEST3=0 VEXDEST210=5    | XED_REG_R10D
          XED_REG_R10D,
          /// VEXDEST3=0 VEXDEST210=4    | XED_REG_R11D
          XED_REG_R11D,
          /// VEXDEST3=0 VEXDEST210=3    | XED_REG_R12D
          XED_REG_R12D,
          /// VEXDEST3=0 VEXDEST210=2    | XED_REG_R13D
          XED_REG_R13D,
          /// VEXDEST3=0 VEXDEST210=1    | XED_REG_R14D
          XED_REG_R14D,
          /// VEXDEST3=0 VEXDEST210=0    | XED_REG_R15D
          XED_REG_R15D,
  };
  static constexpr Register_Class VGPR32_N_64 = VGPR32_N_64_array;

  /// xed_reg_enum_t VGPR64_N()::
  static constexpr xed_reg_enum_t VGPR64_N_array[] = {
          /// VEXDEST3=1 VEXDEST210=7   | XED_REG_RAX
          XED_REG_RAX,
          /// VEXDEST3=1 VEXDEST210=6   | XED_REG_RCX
          XED_REG_RCX,
          /// VEXDEST3=1 VEXDEST210=5   | XED_REG_RDX
          XED_REG_RDX,
          /// VEXDEST3=1 VEXDEST210=4   | XED_REG_RBX
          XED_REG_RBX,
          /// VEXDEST3=1 VEXDEST210=3   | XED_REG_RSP
          XED_REG_RSP,
          /// VEXDEST3=1 VEXDEST210=2   | XED_REG_RBP
          XED_REG_RBP,
          /// VEXDEST3=1 VEXDEST210=1   | XED_REG_RSI
          XED_REG_RSI,
          /// VEXDEST3=1 VEXDEST210=0   | XED_REG_RDI
          XED_REG_RDI,
          /// VEXDEST3=0 VEXDEST210=7   | XED_REG_R8
          XED_REG_R8,
          /// VEXDEST3=0 VEXDEST210=6   | XED_REG_R9
          XED_REG_R9,
          /// VEXDEST3=0 VEXDEST210=5   | XED_REG_R10
          XED_REG_R10,
          /// VEXDEST3=0 VEXDEST210=4   | XED_REG_R11
          XED_REG_R11,
          /// VEXDEST3=0 VEXDEST210=3   | XED_REG_R12
          XED_REG_R12,
          /// VEXDEST3=0 VEXDEST210=2   | XED_REG_R13
          XED_REG_R13,
          /// VEXDEST3=0 VEXDEST210=1   | XED_REG_R14
          XED_REG_R14,
          /// VEXDEST3=0 VEXDEST210=0   | XED_REG_R15
          XED_REG_R15,
  };
  static constexpr Register_Class VGPR64_N = VGPR64_N_array;

  /// xed_reg_enum_t VGPR32_R_32()::
  static constexpr xed_reg_enum_t VGPR32_R_32_array[] = {
          /// REG=0    | XED_REG_EAX
          XED_REG_EAX,
          /// REG=1    | XED_REG_ECX
          XED_REG_ECX,
          /// REG=2    | XED_REG_EDX
          XED_REG_EDX,
          /// REG=3    | XED_REG_EBX
          XED_REG_EBX,
          /// REG=4    | XED_REG_ESP
          XED_REG_ESP,
          /// REG=5    | XED_REG_EBP
          XED_REG_EBP,
          /// REG=6    | XED_REG_ESI
          XED_REG_ESI,
          /// REG=7    | XED_REG_EDI
          XED_REG_EDI,
  };
  static constexpr Register_Class VGPR32_R_32 = VGPR32_R_32_array;

  /// xed_reg_enum_t VGPR32_R_64()::
  static constexpr xed_reg_enum_t VGPR32_R_64_array[] = {
          /// REXR=0 REG=0   | XED_REG_EAX
          XED_REG_EAX,
          /// REXR=0 REG=1   | XED_REG_ECX
          XED_REG_ECX,
          /// REXR=0 REG=2   | XED_REG_EDX
          XED_REG_EDX,
          /// REXR=0 REG=3   | XED_REG_EBX
          XED_REG_EBX,
          /// REXR=0 REG=4   | XED_REG_ESP
          XED_REG_ESP,
          /// REXR=0 REG=5   | XED_REG_EBP
          XED_REG_EBP,
          /// REXR=0 REG=6   | XED_REG_ESI
          XED_REG_ESI,
          /// REXR=0 REG=7   | XED_REG_EDI
          XED_REG_EDI,
          /// REXR=1 REG=0   | XED_REG_R8D
          XED_REG_R8D,
          /// REXR=1 REG=1   | XED_REG_R9D
          XED_REG_R9D,
          /// REXR=1 REG=2   | XED_REG_R10D
          XED_REG_R10D,
          /// REXR=1 REG=3   | XED_REG_R11D
          XED_REG_R11D,
          /// REXR=1 REG=4   | XED_REG_R12D
          XED_REG_R12D,
          /// REXR=1 REG=5   | XED_REG_R13D
          XED_REG_R13D,
          /// REXR=1 REG=6   | XED_REG_R14D
          XED_REG_R14D,
          /// REXR=1 REG=7   | XED_REG_R15D
          XED_REG_R15D,
  };
  static constexpr Register_Class VGPR32_R_64 = VGPR32_R_64_array;

  /// xed_reg_enum_t VGPR64_R()::
  static constexpr xed_reg_enum_t VGPR64_R_array[] = {
          /// REXR=0 REG=0   | XED_REG_RAX
          XED_REG_RAX,
          /// REXR=0 REG=1   | XED_REG_RCX
          XED_REG_RCX,
          /// REXR=0 REG=2   | XED_REG_RDX
          XED_REG_RDX,
          /// REXR=0 REG=3   | XED_REG_RBX
          XED_REG_RBX,
          /// REXR=0 REG=4   | XED_REG_RSP
          XED_REG_RSP,
          /// REXR=0 REG=5   | XED_REG_RBP
          XED_REG_RBP,
          /// REXR=0 REG=6   | XED_REG_RSI
          XED_REG_RSI,
          /// REXR=0 REG=7   | XED_REG_RDI
          XED_REG_RDI,
          /// REXR=1 REG=0   | XED_REG_R8
          XED_REG_R8,
          /// REXR=1 REG=1   | XED_REG_R9
          XED_REG_R9,
          /// REXR=1 REG=2   | XED_REG_R10
          XED_REG_R10,
          /// REXR=1 REG=3   | XED_REG_R11
          XED_REG_R11,
          /// REXR=1 REG=4   | XED_REG_R12
          XED_REG_R12,
          /// REXR=1 REG=5   | XED_REG_R13
          XED_REG_R13,
          /// REXR=1 REG=6   | XED_REG_R14
          XED_REG_R14,
          /// REXR=1 REG=7   | XED_REG_R15
          XED_REG_R15,
  };
  static constexpr Register_Class VGPR64_R = VGPR64_R_array;

  /// xed_reg_enum_t VGPR32_B_32()::
  static constexpr xed_reg_enum_t VGPR32_B_32_array[] = {
          /// RM=0    | XED_REG_EAX
          XED_REG_EAX,
          /// RM=1    | XED_REG_ECX
          XED_REG_ECX,
          /// RM=2    | XED_REG_EDX
          XED_REG_EDX,
          /// RM=3    | XED_REG_EBX
          XED_REG_EBX,
          /// RM=4    | XED_REG_ESP
          XED_REG_ESP,
          /// RM=5    | XED_REG_EBP
          XED_REG_EBP,
          /// RM=6    | XED_REG_ESI
          XED_REG_ESI,
          /// RM=7    | XED_REG_EDI
          XED_REG_EDI,
  };
  static constexpr Register_Class VGPR32_B_32 = VGPR32_B_32_array;

  /// xed_reg_enum_t VGPR32_B_64()::
  static constexpr xed_reg_enum_t VGPR32_B_64_array[] = {
          /// REXB=0 RM=0   | XED_REG_EAX
          XED_REG_EAX,
          /// REXB=0 RM=1   | XED_REG_ECX
          XED_REG_ECX,
          /// REXB=0 RM=2   | XED_REG_EDX
          XED_REG_EDX,
          /// REXB=0 RM=3   | XED_REG_EBX
          XED_REG_EBX,
          /// REXB=0 RM=4   | XED_REG_ESP
          XED_REG_ESP,
          /// REXB=0 RM=5   | XED_REG_EBP
          XED_REG_EBP,
          /// REXB=0 RM=6   | XED_REG_ESI
          XED_REG_ESI,
          /// REXB=0 RM=7   | XED_REG_EDI
          XED_REG_EDI,
          /// REXB=1 RM=0   | XED_REG_R8D
          XED_REG_R8D,
          /// REXB=1 RM=1   | XED_REG_R9D
          XED_REG_R9D,
          /// REXB=1 RM=2   | XED_REG_R10D
          XED_REG_R10D,
          /// REXB=1 RM=3   | XED_REG_R11D
          XED_REG_R11D,
          /// REXB=1 RM=4   | XED_REG_R12D
          XED_REG_R12D,
          /// REXB=1 RM=5   | XED_REG_R13D
          XED_REG_R13D,
          /// REXB=1 RM=6   | XED_REG_R14D
          XED_REG_R14D,
          /// REXB=1 RM=7   | XED_REG_R15D
          XED_REG_R15D,
  };
  static constexpr Register_Class VGPR32_B_64 = VGPR32_B_64_array;

  /// xed_reg_enum_t VGPR64_B()::
  static constexpr xed_reg_enum_t VGPR64_B_array[] = {
          /// REXB=0 RM=0   | XED_REG_RAX
          XED_REG_RAX,
          /// REXB=0 RM=1   | XED_REG_RCX
          XED_REG_RCX,
          /// REXB=0 RM=2   | XED_REG_RDX
          XED_REG_RDX,
          /// REXB=0 RM=3   | XED_REG_RBX
          XED_REG_RBX,
          /// REXB=0 RM=4   | XED_REG_RSP
          XED_REG_RSP,
          /// REXB=0 RM=5   | XED_REG_RBP
          XED_REG_RBP,
          /// REXB=0 RM=6   | XED_REG_RSI
          XED_REG_RSI,
          /// REXB=0 RM=7   | XED_REG_RDI
          XED_REG_RDI,
          /// REXB=1 RM=0   | XED_REG_R8
          XED_REG_R8,
          /// REXB=1 RM=1   | XED_REG_R9
          XED_REG_R9,
          /// REXB=1 RM=2   | XED_REG_R10
          XED_REG_R10,
          /// REXB=1 RM=3   | XED_REG_R11
          XED_REG_R11,
          /// REXB=1 RM=4   | XED_REG_R12
          XED_REG_R12,
          /// REXB=1 RM=5   | XED_REG_R13
          XED_REG_R13,
          /// REXB=1 RM=6   | XED_REG_R14
          XED_REG_R14,
          /// REXB=1 RM=7   | XED_REG_R15
          XED_REG_R15,
  };
  static constexpr Register_Class VGPR64_B = VGPR64_B_array;

  /// xed_reg_enum_t VGPRy_N()::
  static constexpr Register_Class VGPRy_N(int EOSZ, unsigned cpu_mode) {
    switch (EOSZ) {
      /// EOSZ=1 | VGPR32_N()
      case 1:
        return VGPR32_N(cpu_mode);
        /// EOSZ=2 | VGPR32_N()
      case 2:
        return VGPR32_N(cpu_mode);
        /// EOSZ=3 | VGPR64_N()
      case 3:
        return VGPR64_N;
    }
    abort();
  }

  /// xed_reg_enum_t VGPR32_N()::
  static constexpr Register_Class VGPR32_N(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | VGPR32_N_32()
      case 16: return VGPR32_N_32;
        /// cpu_mode=32 | VGPR32_N_32()
      case 32: return VGPR32_N_32;
        /// cpu_mode=64 | VGPR32_N_64()
      case 64: return VGPR32_N_64;
    }
    abort();
  }

  /// xed_reg_enum_t XMM_R3()::
  static constexpr Register_Class XMM_R3(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | XMM_R3_32
      case 16: return XMM_R3_32;
        /// cpu_mode=32 | XMM_R3_32
      case 32: return XMM_R3_32;
        /// cpu_mode=64 | XMM_R3_64
      case 64: return XMM_R3_64;
    }
    abort();
  }

  /// xed_reg_enum_t XMM_R3_32()::
  static constexpr xed_reg_enum_t XMM_R3_32_array[] = {
          /// REG=0  | XED_REG_XMM0
          XED_REG_XMM0,
          /// REG=1  | XED_REG_XMM1
          XED_REG_XMM1,
          /// REG=2  | XED_REG_XMM2
          XED_REG_XMM2,
          /// REG=3  | XED_REG_XMM3
          XED_REG_XMM3,
          /// REG=4  | XED_REG_XMM4
          XED_REG_XMM4,
          /// REG=5  | XED_REG_XMM5
          XED_REG_XMM5,
          /// REG=6  | XED_REG_XMM6
          XED_REG_XMM6,
          /// REG=7  | XED_REG_XMM7
          XED_REG_XMM7,
  };
  static constexpr Register_Class XMM_R3_32 = XMM_R3_32_array;

  /// xed_reg_enum_t XMM_R3_64()::
  static constexpr xed_reg_enum_t XMM_R3_64_array[] = {
          /// REXRR=0 REXR=0 REG=0  | XED_REG_XMM0
          XED_REG_XMM0,
          /// REXRR=0 REXR=0 REG=1  | XED_REG_XMM1
          XED_REG_XMM1,
          /// REXRR=0 REXR=0 REG=2  | XED_REG_XMM2
          XED_REG_XMM2,
          /// REXRR=0 REXR=0 REG=3  | XED_REG_XMM3
          XED_REG_XMM3,
          /// REXRR=0 REXR=0 REG=4  | XED_REG_XMM4
          XED_REG_XMM4,
          /// REXRR=0 REXR=0 REG=5  | XED_REG_XMM5
          XED_REG_XMM5,
          /// REXRR=0 REXR=0 REG=6  | XED_REG_XMM6
          XED_REG_XMM6,
          /// REXRR=0 REXR=0 REG=7  | XED_REG_XMM7
          XED_REG_XMM7,
          /// REXRR=0 REXR=1 REG=0  | XED_REG_XMM8
          XED_REG_XMM8,
          /// REXRR=0 REXR=1 REG=1  | XED_REG_XMM9
          XED_REG_XMM9,
          /// REXRR=0 REXR=1 REG=2  | XED_REG_XMM10
          XED_REG_XMM10,
          /// REXRR=0 REXR=1 REG=3  | XED_REG_XMM11
          XED_REG_XMM11,
          /// REXRR=0 REXR=1 REG=4  | XED_REG_XMM12
          XED_REG_XMM12,
          /// REXRR=0 REXR=1 REG=5  | XED_REG_XMM13
          XED_REG_XMM13,
          /// REXRR=0 REXR=1 REG=6  | XED_REG_XMM14
          XED_REG_XMM14,
          /// REXRR=0 REXR=1 REG=7  | XED_REG_XMM15
          XED_REG_XMM15,
          /// REXRR=1 REXR=0 REG=0  | XED_REG_XMM16
          XED_REG_XMM16,
          /// REXRR=1 REXR=0 REG=1  | XED_REG_XMM17
          XED_REG_XMM17,
          /// REXRR=1 REXR=0 REG=2  | XED_REG_XMM18
          XED_REG_XMM18,
          /// REXRR=1 REXR=0 REG=3  | XED_REG_XMM19
          XED_REG_XMM19,
          /// REXRR=1 REXR=0 REG=4  | XED_REG_XMM20
          XED_REG_XMM20,
          /// REXRR=1 REXR=0 REG=5  | XED_REG_XMM21
          XED_REG_XMM21,
          /// REXRR=1 REXR=0 REG=6  | XED_REG_XMM22
          XED_REG_XMM22,
          /// REXRR=1 REXR=0 REG=7  | XED_REG_XMM23
          XED_REG_XMM23,
          /// REXRR=1 REXR=1 REG=0  | XED_REG_XMM24
          XED_REG_XMM24,
          /// REXRR=1 REXR=1 REG=1  | XED_REG_XMM25
          XED_REG_XMM25,
          /// REXRR=1 REXR=1 REG=2  | XED_REG_XMM26
          XED_REG_XMM26,
          /// REXRR=1 REXR=1 REG=3  | XED_REG_XMM27
          XED_REG_XMM27,
          /// REXRR=1 REXR=1 REG=4  | XED_REG_XMM28
          XED_REG_XMM28,
          /// REXRR=1 REXR=1 REG=5  | XED_REG_XMM29
          XED_REG_XMM29,
          /// REXRR=1 REXR=1 REG=6  | XED_REG_XMM30
          XED_REG_XMM30,
          /// REXRR=1 REXR=1 REG=7  | XED_REG_XMM31
          XED_REG_XMM31,
  };
  static constexpr Register_Class XMM_R3_64 = XMM_R3_64_array;

  /// xed_reg_enum_t YMM_R3()::
  static constexpr Register_Class YMM_R3(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | YMM_R3_32
      case 16: return YMM_R3_32;
        /// cpu_mode=32 | YMM_R3_32
      case 32: return YMM_R3_32;
        /// cpu_mode=64 | YMM_R3_64
      case 64: return YMM_R3_64;
    }
    abort();
  }

  /// xed_reg_enum_t YMM_R3_32()::
  static constexpr xed_reg_enum_t YMM_R3_32_array[] = {
          /// REG=0  | XED_REG_YMM0
          XED_REG_YMM0,
          /// REG=1  | XED_REG_YMM1
          XED_REG_YMM1,
          /// REG=2  | XED_REG_YMM2
          XED_REG_YMM2,
          /// REG=3  | XED_REG_YMM3
          XED_REG_YMM3,
          /// REG=4  | XED_REG_YMM4
          XED_REG_YMM4,
          /// REG=5  | XED_REG_YMM5
          XED_REG_YMM5,
          /// REG=6  | XED_REG_YMM6
          XED_REG_YMM6,
          /// REG=7  | XED_REG_YMM7
          XED_REG_YMM7,
  };
  static constexpr Register_Class YMM_R3_32 = YMM_R3_32_array;

  /// xed_reg_enum_t YMM_R3_64()::
  static constexpr xed_reg_enum_t YMM_R3_64_array[] = {
          /// REXRR=0 REXR=0 REG=0  | XED_REG_YMM0
          XED_REG_YMM0,
          /// REXRR=0 REXR=0 REG=1  | XED_REG_YMM1
          XED_REG_YMM1,
          /// REXRR=0 REXR=0 REG=2  | XED_REG_YMM2
          XED_REG_YMM2,
          /// REXRR=0 REXR=0 REG=3  | XED_REG_YMM3
          XED_REG_YMM3,
          /// REXRR=0 REXR=0 REG=4  | XED_REG_YMM4
          XED_REG_YMM4,
          /// REXRR=0 REXR=0 REG=5  | XED_REG_YMM5
          XED_REG_YMM5,
          /// REXRR=0 REXR=0 REG=6  | XED_REG_YMM6
          XED_REG_YMM6,
          /// REXRR=0 REXR=0 REG=7  | XED_REG_YMM7
          XED_REG_YMM7,
          /// REXRR=0 REXR=1 REG=0  | XED_REG_YMM8
          XED_REG_YMM8,
          /// REXRR=0 REXR=1 REG=1  | XED_REG_YMM9
          XED_REG_YMM9,
          /// REXRR=0 REXR=1 REG=2  | XED_REG_YMM10
          XED_REG_YMM10,
          /// REXRR=0 REXR=1 REG=3  | XED_REG_YMM11
          XED_REG_YMM11,
          /// REXRR=0 REXR=1 REG=4  | XED_REG_YMM12
          XED_REG_YMM12,
          /// REXRR=0 REXR=1 REG=5  | XED_REG_YMM13
          XED_REG_YMM13,
          /// REXRR=0 REXR=1 REG=6  | XED_REG_YMM14
          XED_REG_YMM14,
          /// REXRR=0 REXR=1 REG=7  | XED_REG_YMM15
          XED_REG_YMM15,
          /// REXRR=1 REXR=0 REG=0  | XED_REG_YMM16
          XED_REG_YMM16,
          /// REXRR=1 REXR=0 REG=1  | XED_REG_YMM17
          XED_REG_YMM17,
          /// REXRR=1 REXR=0 REG=2  | XED_REG_YMM18
          XED_REG_YMM18,
          /// REXRR=1 REXR=0 REG=3  | XED_REG_YMM19
          XED_REG_YMM19,
          /// REXRR=1 REXR=0 REG=4  | XED_REG_YMM20
          XED_REG_YMM20,
          /// REXRR=1 REXR=0 REG=5  | XED_REG_YMM21
          XED_REG_YMM21,
          /// REXRR=1 REXR=0 REG=6  | XED_REG_YMM22
          XED_REG_YMM22,
          /// REXRR=1 REXR=0 REG=7  | XED_REG_YMM23
          XED_REG_YMM23,
          /// REXRR=1 REXR=1 REG=0  | XED_REG_YMM24
          XED_REG_YMM24,
          /// REXRR=1 REXR=1 REG=1  | XED_REG_YMM25
          XED_REG_YMM25,
          /// REXRR=1 REXR=1 REG=2  | XED_REG_YMM26
          XED_REG_YMM26,
          /// REXRR=1 REXR=1 REG=3  | XED_REG_YMM27
          XED_REG_YMM27,
          /// REXRR=1 REXR=1 REG=4  | XED_REG_YMM28
          XED_REG_YMM28,
          /// REXRR=1 REXR=1 REG=5  | XED_REG_YMM29
          XED_REG_YMM29,
          /// REXRR=1 REXR=1 REG=6  | XED_REG_YMM30
          XED_REG_YMM30,
          /// REXRR=1 REXR=1 REG=7  | XED_REG_YMM31
          XED_REG_YMM31,
  };
  static constexpr Register_Class YMM_R3_64 = YMM_R3_64_array;


  /// xed_reg_enum_t ZMM_R3()::
  static constexpr Register_Class ZMM_R3(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | ZMM_R3_32
      case 16: return ZMM_R3_32;
        /// cpu_mode=32 | ZMM_R3_32
      case 32: return ZMM_R3_32;
        /// cpu_mode=64 | ZMM_R3_64
      case 64: return ZMM_R3_64;
    }
    abort();
  }

  /// xed_reg_enum_t ZMM_R3_32()::
  static constexpr xed_reg_enum_t ZMM_R3_32_array[] = {
          /// REG=0  | XED_REG_ZMM0
          XED_REG_ZMM0,
          /// REG=1  | XED_REG_ZMM1
          XED_REG_ZMM1,
          /// REG=2  | XED_REG_ZMM2
          XED_REG_ZMM2,
          /// REG=3  | XED_REG_ZMM3
          XED_REG_ZMM3,
          /// REG=4  | XED_REG_ZMM4
          XED_REG_ZMM4,
          /// REG=5  | XED_REG_ZMM5
          XED_REG_ZMM5,
          /// REG=6  | XED_REG_ZMM6
          XED_REG_ZMM6,
          /// REG=7  | XED_REG_ZMM7
          XED_REG_ZMM7,
  };
  static constexpr Register_Class ZMM_R3_32 = ZMM_R3_32_array;

  /// xed_reg_enum_t ZMM_R3_64()::
  static constexpr xed_reg_enum_t ZMM_R3_64_array[] = {
          /// REXRR=0 REXR=0 REG=0  | XED_REG_ZMM0
          XED_REG_ZMM0,
          /// REXRR=0 REXR=0 REG=1  | XED_REG_ZMM1
          XED_REG_ZMM1,
          /// REXRR=0 REXR=0 REG=2  | XED_REG_ZMM2
          XED_REG_ZMM2,
          /// REXRR=0 REXR=0 REG=3  | XED_REG_ZMM3
          XED_REG_ZMM3,
          /// REXRR=0 REXR=0 REG=4  | XED_REG_ZMM4
          XED_REG_ZMM4,
          /// REXRR=0 REXR=0 REG=5  | XED_REG_ZMM5
          XED_REG_ZMM5,
          /// REXRR=0 REXR=0 REG=6  | XED_REG_ZMM6
          XED_REG_ZMM6,
          /// REXRR=0 REXR=0 REG=7  | XED_REG_ZMM7
          XED_REG_ZMM7,
          /// REXRR=0 REXR=1 REG=0  | XED_REG_ZMM8
          XED_REG_ZMM8,
          /// REXRR=0 REXR=1 REG=1  | XED_REG_ZMM9
          XED_REG_ZMM9,
          /// REXRR=0 REXR=1 REG=2  | XED_REG_ZMM10
          XED_REG_ZMM10,
          /// REXRR=0 REXR=1 REG=3  | XED_REG_ZMM11
          XED_REG_ZMM11,
          /// REXRR=0 REXR=1 REG=4  | XED_REG_ZMM12
          XED_REG_ZMM12,
          /// REXRR=0 REXR=1 REG=5  | XED_REG_ZMM13
          XED_REG_ZMM13,
          /// REXRR=0 REXR=1 REG=6  | XED_REG_ZMM14
          XED_REG_ZMM14,
          /// REXRR=0 REXR=1 REG=7  | XED_REG_ZMM15
          XED_REG_ZMM15,
          /// REXRR=1 REXR=0 REG=0  | XED_REG_ZMM16
          XED_REG_ZMM16,
          /// REXRR=1 REXR=0 REG=1  | XED_REG_ZMM17
          XED_REG_ZMM17,
          /// REXRR=1 REXR=0 REG=2  | XED_REG_ZMM18
          XED_REG_ZMM18,
          /// REXRR=1 REXR=0 REG=3  | XED_REG_ZMM19
          XED_REG_ZMM19,
          /// REXRR=1 REXR=0 REG=4  | XED_REG_ZMM20
          XED_REG_ZMM20,
          /// REXRR=1 REXR=0 REG=5  | XED_REG_ZMM21
          XED_REG_ZMM21,
          /// REXRR=1 REXR=0 REG=6  | XED_REG_ZMM22
          XED_REG_ZMM22,
          /// REXRR=1 REXR=0 REG=7  | XED_REG_ZMM23
          XED_REG_ZMM23,
          /// REXRR=1 REXR=1 REG=0  | XED_REG_ZMM24
          XED_REG_ZMM24,
          /// REXRR=1 REXR=1 REG=1  | XED_REG_ZMM25
          XED_REG_ZMM25,
          /// REXRR=1 REXR=1 REG=2  | XED_REG_ZMM26
          XED_REG_ZMM26,
          /// REXRR=1 REXR=1 REG=3  | XED_REG_ZMM27
          XED_REG_ZMM27,
          /// REXRR=1 REXR=1 REG=4  | XED_REG_ZMM28
          XED_REG_ZMM28,
          /// REXRR=1 REXR=1 REG=5  | XED_REG_ZMM29
          XED_REG_ZMM29,
          /// REXRR=1 REXR=1 REG=6  | XED_REG_ZMM30
          XED_REG_ZMM30,
          /// REXRR=1 REXR=1 REG=7  | XED_REG_ZMM31
          XED_REG_ZMM31,
  };
  static constexpr Register_Class ZMM_R3_64 = ZMM_R3_64_array;

  /// xed_reg_enum_t XMM_B3()::
  static constexpr Register_Class XMM_B3(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | XMM_B3_32
      case 16: return XMM_B3_32;
        /// cpu_mode=32 | XMM_B3_32
      case 32: return XMM_B3_32;
        /// cpu_mode=64 | XMM_B3_64
      case 64: return XMM_B3_64;
    }
    abort();
  }

  /// xed_reg_enum_t XMM_B3_32()::
  static constexpr xed_reg_enum_t XMM_B3_32_array[] = {
          /// RM=0  | XED_REG_XMM0
          XED_REG_XMM0,
          /// RM=1  | XED_REG_XMM1
          XED_REG_XMM1,
          /// RM=2  | XED_REG_XMM2
          XED_REG_XMM2,
          /// RM=3  | XED_REG_XMM3
          XED_REG_XMM3,
          /// RM=4  | XED_REG_XMM4
          XED_REG_XMM4,
          /// RM=5  | XED_REG_XMM5
          XED_REG_XMM5,
          /// RM=6  | XED_REG_XMM6
          XED_REG_XMM6,
          /// RM=7  | XED_REG_XMM7
          XED_REG_XMM7,
  };
  static constexpr Register_Class XMM_B3_32 = XMM_B3_32_array;

  /// xed_reg_enum_t XMM_B3_64()::
  static constexpr xed_reg_enum_t XMM_B3_64_array[] = {
          /// REXX=0 REXB=0 RM=0  | XED_REG_XMM0
          XED_REG_XMM0,
          /// REXX=0 REXB=0 RM=1  | XED_REG_XMM1
          XED_REG_XMM1,
          /// REXX=0 REXB=0 RM=2  | XED_REG_XMM2
          XED_REG_XMM2,
          /// REXX=0 REXB=0 RM=3  | XED_REG_XMM3
          XED_REG_XMM3,
          /// REXX=0 REXB=0 RM=4  | XED_REG_XMM4
          XED_REG_XMM4,
          /// REXX=0 REXB=0 RM=5  | XED_REG_XMM5
          XED_REG_XMM5,
          /// REXX=0 REXB=0 RM=6  | XED_REG_XMM6
          XED_REG_XMM6,
          /// REXX=0 REXB=0 RM=7  | XED_REG_XMM7
          XED_REG_XMM7,
          /// REXX=0 REXB=1 RM=0  | XED_REG_XMM8
          XED_REG_XMM8,
          /// REXX=0 REXB=1 RM=1  | XED_REG_XMM9
          XED_REG_XMM9,
          /// REXX=0 REXB=1 RM=2  | XED_REG_XMM10
          XED_REG_XMM10,
          /// REXX=0 REXB=1 RM=3  | XED_REG_XMM11
          XED_REG_XMM11,
          /// REXX=0 REXB=1 RM=4  | XED_REG_XMM12
          XED_REG_XMM12,
          /// REXX=0 REXB=1 RM=5  | XED_REG_XMM13
          XED_REG_XMM13,
          /// REXX=0 REXB=1 RM=6  | XED_REG_XMM14
          XED_REG_XMM14,
          /// REXX=0 REXB=1 RM=7  | XED_REG_XMM15
          XED_REG_XMM15,
          /// REXX=1 REXB=0 RM=0  | XED_REG_XMM16
          XED_REG_XMM16,
          /// REXX=1 REXB=0 RM=1  | XED_REG_XMM17
          XED_REG_XMM17,
          /// REXX=1 REXB=0 RM=2  | XED_REG_XMM18
          XED_REG_XMM18,
          /// REXX=1 REXB=0 RM=3  | XED_REG_XMM19
          XED_REG_XMM19,
          /// REXX=1 REXB=0 RM=4  | XED_REG_XMM20
          XED_REG_XMM20,
          /// REXX=1 REXB=0 RM=5  | XED_REG_XMM21
          XED_REG_XMM21,
          /// REXX=1 REXB=0 RM=6  | XED_REG_XMM22
          XED_REG_XMM22,
          /// REXX=1 REXB=0 RM=7  | XED_REG_XMM23
          XED_REG_XMM23,
          /// REXX=1 REXB=1 RM=0  | XED_REG_XMM24
          XED_REG_XMM24,
          /// REXX=1 REXB=1 RM=1  | XED_REG_XMM25
          XED_REG_XMM25,
          /// REXX=1 REXB=1 RM=2  | XED_REG_XMM26
          XED_REG_XMM26,
          /// REXX=1 REXB=1 RM=3  | XED_REG_XMM27
          XED_REG_XMM27,
          /// REXX=1 REXB=1 RM=4  | XED_REG_XMM28
          XED_REG_XMM28,
          /// REXX=1 REXB=1 RM=5  | XED_REG_XMM29
          XED_REG_XMM29,
          /// REXX=1 REXB=1 RM=6  | XED_REG_XMM30
          XED_REG_XMM30,
          /// REXX=1 REXB=1 RM=7  | XED_REG_XMM31
          XED_REG_XMM31,
  };
  static constexpr Register_Class XMM_B3_64 = XMM_B3_64_array;

  /// xed_reg_enum_t YMM_B3()::
  static constexpr Register_Class YMM_B3(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | YMM_B3_32
      case 16: return YMM_B3_32;
        /// cpu_mode=32 | YMM_B3_32
      case 32: return YMM_B3_32;
        /// cpu_mode=64 | YMM_B3_64
      case 64: return YMM_B3_64;
    }
    abort();
  }

  /// xed_reg_enum_t YMM_B3_32()::
  static constexpr xed_reg_enum_t YMM_B3_32_array[] = {
          /// RM=0  | XED_REG_YMM0
          XED_REG_YMM0,
          /// RM=1  | XED_REG_YMM1
          XED_REG_YMM1,
          /// RM=2  | XED_REG_YMM2
          XED_REG_YMM2,
          /// RM=3  | XED_REG_YMM3
          XED_REG_YMM3,
          /// RM=4  | XED_REG_YMM4
          XED_REG_YMM4,
          /// RM=5  | XED_REG_YMM5
          XED_REG_YMM5,
          /// RM=6  | XED_REG_YMM6
          XED_REG_YMM6,
          /// RM=7  | XED_REG_YMM7
          XED_REG_YMM7,
  };
  static constexpr Register_Class YMM_B3_32 = YMM_B3_32_array;

  /// xed_reg_enum_t YMM_B3_64()::
  static constexpr xed_reg_enum_t YMM_B3_64_array[] = {
          /// REXX=0 REXB=0 RM=0  | XED_REG_YMM0
          XED_REG_YMM0,
          /// REXX=0 REXB=0 RM=1  | XED_REG_YMM1
          XED_REG_YMM1,
          /// REXX=0 REXB=0 RM=2  | XED_REG_YMM2
          XED_REG_YMM2,
          /// REXX=0 REXB=0 RM=3  | XED_REG_YMM3
          XED_REG_YMM3,
          /// REXX=0 REXB=0 RM=4  | XED_REG_YMM4
          XED_REG_YMM4,
          /// REXX=0 REXB=0 RM=5  | XED_REG_YMM5
          XED_REG_YMM5,
          /// REXX=0 REXB=0 RM=6  | XED_REG_YMM6
          XED_REG_YMM6,
          /// REXX=0 REXB=0 RM=7  | XED_REG_YMM7
          XED_REG_YMM7,
          /// REXX=0 REXB=1 RM=0  | XED_REG_YMM8
          XED_REG_YMM8,
          /// REXX=0 REXB=1 RM=1  | XED_REG_YMM9
          XED_REG_YMM9,
          /// REXX=0 REXB=1 RM=2  | XED_REG_YMM10
          XED_REG_YMM10,
          /// REXX=0 REXB=1 RM=3  | XED_REG_YMM11
          XED_REG_YMM11,
          /// REXX=0 REXB=1 RM=4  | XED_REG_YMM12
          XED_REG_YMM12,
          /// REXX=0 REXB=1 RM=5  | XED_REG_YMM13
          XED_REG_YMM13,
          /// REXX=0 REXB=1 RM=6  | XED_REG_YMM14
          XED_REG_YMM14,
          /// REXX=0 REXB=1 RM=7  | XED_REG_YMM15
          XED_REG_YMM15,
          /// REXX=1 REXB=0 RM=0  | XED_REG_YMM16
          XED_REG_YMM16,
          /// REXX=1 REXB=0 RM=1  | XED_REG_YMM17
          XED_REG_YMM17,
          /// REXX=1 REXB=0 RM=2  | XED_REG_YMM18
          XED_REG_YMM18,
          /// REXX=1 REXB=0 RM=3  | XED_REG_YMM19
          XED_REG_YMM19,
          /// REXX=1 REXB=0 RM=4  | XED_REG_YMM20
          XED_REG_YMM20,
          /// REXX=1 REXB=0 RM=5  | XED_REG_YMM21
          XED_REG_YMM21,
          /// REXX=1 REXB=0 RM=6  | XED_REG_YMM22
          XED_REG_YMM22,
          /// REXX=1 REXB=0 RM=7  | XED_REG_YMM23
          XED_REG_YMM23,
          /// REXX=1 REXB=1 RM=0  | XED_REG_YMM24
          XED_REG_YMM24,
          /// REXX=1 REXB=1 RM=1  | XED_REG_YMM25
          XED_REG_YMM25,
          /// REXX=1 REXB=1 RM=2  | XED_REG_YMM26
          XED_REG_YMM26,
          /// REXX=1 REXB=1 RM=3  | XED_REG_YMM27
          XED_REG_YMM27,
          /// REXX=1 REXB=1 RM=4  | XED_REG_YMM28
          XED_REG_YMM28,
          /// REXX=1 REXB=1 RM=5  | XED_REG_YMM29
          XED_REG_YMM29,
          /// REXX=1 REXB=1 RM=6  | XED_REG_YMM30
          XED_REG_YMM30,
          /// REXX=1 REXB=1 RM=7  | XED_REG_YMM31
          XED_REG_YMM31,
  };
  static constexpr Register_Class YMM_B3_64 = YMM_B3_64_array;

  /// xed_reg_enum_t ZMM_B3()::
  static constexpr Register_Class ZMM_B3(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | ZMM_B3_32
      case 16: return ZMM_B3_32;
        /// cpu_mode=32 | ZMM_B3_32
      case 32: return ZMM_B3_32;
        /// cpu_mode=64 | ZMM_B3_64
      case 64: return ZMM_B3_64;
    }
    abort();
  }

  /// xed_reg_enum_t ZMM_B3_32()::
  static constexpr xed_reg_enum_t ZMM_B3_32_array[] = {
          /// RM=0  | XED_REG_ZMM0
          XED_REG_ZMM0,
          /// RM=1  | XED_REG_ZMM1
          XED_REG_ZMM1,
          /// RM=2  | XED_REG_ZMM2
          XED_REG_ZMM2,
          /// RM=3  | XED_REG_ZMM3
          XED_REG_ZMM3,
          /// RM=4  | XED_REG_ZMM4
          XED_REG_ZMM4,
          /// RM=5  | XED_REG_ZMM5
          XED_REG_ZMM5,
          /// RM=6  | XED_REG_ZMM6
          XED_REG_ZMM6,
          /// RM=7  | XED_REG_ZMM7
          XED_REG_ZMM7,
  };
  static constexpr Register_Class ZMM_B3_32 = ZMM_B3_32_array;

  /// xed_reg_enum_t ZMM_B3_64()::
  static constexpr xed_reg_enum_t ZMM_B3_64_array[] = {
          /// REXX=0 REXB=0 RM=0  | XED_REG_ZMM0
          XED_REG_ZMM0,
          /// REXX=0 REXB=0 RM=1  | XED_REG_ZMM1
          XED_REG_ZMM1,
          /// REXX=0 REXB=0 RM=2  | XED_REG_ZMM2
          XED_REG_ZMM2,
          /// REXX=0 REXB=0 RM=3  | XED_REG_ZMM3
          XED_REG_ZMM3,
          /// REXX=0 REXB=0 RM=4  | XED_REG_ZMM4
          XED_REG_ZMM4,
          /// REXX=0 REXB=0 RM=5  | XED_REG_ZMM5
          XED_REG_ZMM5,
          /// REXX=0 REXB=0 RM=6  | XED_REG_ZMM6
          XED_REG_ZMM6,
          /// REXX=0 REXB=0 RM=7  | XED_REG_ZMM7
          XED_REG_ZMM7,
          /// REXX=0 REXB=1 RM=0  | XED_REG_ZMM8
          XED_REG_ZMM8,
          /// REXX=0 REXB=1 RM=1  | XED_REG_ZMM9
          XED_REG_ZMM9,
          /// REXX=0 REXB=1 RM=2  | XED_REG_ZMM10
          XED_REG_ZMM10,
          /// REXX=0 REXB=1 RM=3  | XED_REG_ZMM11
          XED_REG_ZMM11,
          /// REXX=0 REXB=1 RM=4  | XED_REG_ZMM12
          XED_REG_ZMM12,
          /// REXX=0 REXB=1 RM=5  | XED_REG_ZMM13
          XED_REG_ZMM13,
          /// REXX=0 REXB=1 RM=6  | XED_REG_ZMM14
          XED_REG_ZMM14,
          /// REXX=0 REXB=1 RM=7  | XED_REG_ZMM15
          XED_REG_ZMM15,
          /// REXX=1 REXB=0 RM=0  | XED_REG_ZMM16
          XED_REG_ZMM16,
          /// REXX=1 REXB=0 RM=1  | XED_REG_ZMM17
          XED_REG_ZMM17,
          /// REXX=1 REXB=0 RM=2  | XED_REG_ZMM18
          XED_REG_ZMM18,
          /// REXX=1 REXB=0 RM=3  | XED_REG_ZMM19
          XED_REG_ZMM19,
          /// REXX=1 REXB=0 RM=4  | XED_REG_ZMM20
          XED_REG_ZMM20,
          /// REXX=1 REXB=0 RM=5  | XED_REG_ZMM21
          XED_REG_ZMM21,
          /// REXX=1 REXB=0 RM=6  | XED_REG_ZMM22
          XED_REG_ZMM22,
          /// REXX=1 REXB=0 RM=7  | XED_REG_ZMM23
          XED_REG_ZMM23,
          /// REXX=1 REXB=1 RM=0  | XED_REG_ZMM24
          XED_REG_ZMM24,
          /// REXX=1 REXB=1 RM=1  | XED_REG_ZMM25
          XED_REG_ZMM25,
          /// REXX=1 REXB=1 RM=2  | XED_REG_ZMM26
          XED_REG_ZMM26,
          /// REXX=1 REXB=1 RM=3  | XED_REG_ZMM27
          XED_REG_ZMM27,
          /// REXX=1 REXB=1 RM=4  | XED_REG_ZMM28
          XED_REG_ZMM28,
          /// REXX=1 REXB=1 RM=5  | XED_REG_ZMM29
          XED_REG_ZMM29,
          /// REXX=1 REXB=1 RM=6  | XED_REG_ZMM30
          XED_REG_ZMM30,
          /// REXX=1 REXB=1 RM=7  | XED_REG_ZMM31
          XED_REG_ZMM31,
  };
  static constexpr Register_Class ZMM_B3_64 = ZMM_B3_64_array;

  /// xed_reg_enum_t XMM_SE()::
  static constexpr Register_Class XMM_SE(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | XMM_SE32
      case 16: return XMM_SE32;
        /// cpu_mode=32 | XMM_SE32
      case 32: return XMM_SE32;
        /// cpu_mode=64 | XMM_SE64
      case 64: return XMM_SE64;
    }
    abort();
  }

  /// xed_reg_enum_t XMM_SE64()::
  static constexpr xed_reg_enum_t XMM_SE64_array[] = {
          /// ESRC=0x0  | XED_REG_XMM0
          XED_REG_XMM0,
          /// ESRC=0x1  | XED_REG_XMM1
          XED_REG_XMM1,
          /// ESRC=0x2  | XED_REG_XMM2
          XED_REG_XMM2,
          /// ESRC=0x3  | XED_REG_XMM3
          XED_REG_XMM3,
          /// ESRC=0x4  | XED_REG_XMM4
          XED_REG_XMM4,
          /// ESRC=0x5  | XED_REG_XMM5
          XED_REG_XMM5,
          /// ESRC=0x6  | XED_REG_XMM6
          XED_REG_XMM6,
          /// ESRC=0x7  | XED_REG_XMM7
          XED_REG_XMM7,
          /// ESRC=0x8  | XED_REG_XMM8
          XED_REG_XMM8,
          /// ESRC=0x9  | XED_REG_XMM9
          XED_REG_XMM9,
          /// ESRC=0xA  | XED_REG_XMM10
          XED_REG_XMM10,
          /// ESRC=0xB  | XED_REG_XMM11
          XED_REG_XMM11,
          /// ESRC=0xC  | XED_REG_XMM12
          XED_REG_XMM12,
          /// ESRC=0xD  | XED_REG_XMM13
          XED_REG_XMM13,
          /// ESRC=0xE  | XED_REG_XMM14
          XED_REG_XMM14,
          /// ESRC=0xF  | XED_REG_XMM15
          XED_REG_XMM15,
  };
  static constexpr Register_Class XMM_SE64 = XMM_SE64_array;

  /// xed_reg_enum_t XMM_SE32()::
  static constexpr xed_reg_enum_t XMM_SE32_array[] = {
          /// # ignoring the high bit in non64b modes. Really just 0...7
          /// ESRC=0x8 | OUTREG=XED_REG_XMM0
          XED_REG_XMM0,
          /// ESRC=0x9 | OUTREG=XED_REG_XMM1
          XED_REG_XMM1,
          /// ESRC=0xA | OUTREG=XED_REG_XMM2
          XED_REG_XMM2,
          /// ESRC=0xB | OUTREG=XED_REG_XMM3
          XED_REG_XMM3,
          /// ESRC=0xC | OUTREG=XED_REG_XMM4
          XED_REG_XMM4,
          /// ESRC=0xD | OUTREG=XED_REG_XMM5
          XED_REG_XMM5,
          /// ESRC=0xE | OUTREG=XED_REG_XMM6
          XED_REG_XMM6,
          /// ESRC=0xF | OUTREG=XED_REG_XMM7
          XED_REG_XMM7,
  };
  static constexpr Register_Class XMM_SE32 = XMM_SE32_array;

  /// xed_reg_enum_t YMM_SE()::
  static constexpr Register_Class YMM_SE(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | YMM_SE32
      case 16: return YMM_SE32;
        /// cpu_mode=32 | YMM_SE32
      case 32: return YMM_SE32;
        /// cpu_mode=64 | YMM_SE64
      case 64: return YMM_SE64;
    }
    abort();
  }

  /// xed_reg_enum_t YMM_SE64()::
  static constexpr xed_reg_enum_t YMM_SE64_array[] = {
          /// ESRC=0x0  | XED_REG_YMM0
          XED_REG_YMM0,
          /// ESRC=0x1  | XED_REG_YMM1
          XED_REG_YMM1,
          /// ESRC=0x2  | XED_REG_YMM2
          XED_REG_YMM2,
          /// ESRC=0x3  | XED_REG_YMM3
          XED_REG_YMM3,
          /// ESRC=0x4  | XED_REG_YMM4
          XED_REG_YMM4,
          /// ESRC=0x5  | XED_REG_YMM5
          XED_REG_YMM5,
          /// ESRC=0x6  | XED_REG_YMM6
          XED_REG_YMM6,
          /// ESRC=0x7  | XED_REG_YMM7
          XED_REG_YMM7,
          /// ESRC=0x8  | XED_REG_YMM8
          XED_REG_YMM8,
          /// ESRC=0x9  | XED_REG_YMM9
          XED_REG_YMM9,
          /// ESRC=0xA  | XED_REG_YMM10
          XED_REG_YMM10,
          /// ESRC=0xB  | XED_REG_YMM11
          XED_REG_YMM11,
          /// ESRC=0xC  | XED_REG_YMM12
          XED_REG_YMM12,
          /// ESRC=0xD  | XED_REG_YMM13
          XED_REG_YMM13,
          /// ESRC=0xE  | XED_REG_YMM14
          XED_REG_YMM14,
          /// ESRC=0xF  | XED_REG_YMM15
          XED_REG_YMM15,
  };
  static constexpr Register_Class YMM_SE64 = YMM_SE64_array;

  /// xed_reg_enum_t YMM_SE32()::
  static constexpr xed_reg_enum_t YMM_SE32_array[] = {
          /// # ignoring the high bit in non64b modes. Really just 0...7
          /// ESRC=0x8 | OUTREG=XED_REG_YMM0
          XED_REG_YMM0,
          /// ESRC=0x9 | OUTREG=XED_REG_YMM1
          XED_REG_YMM1,
          /// ESRC=0xA | OUTREG=XED_REG_YMM2
          XED_REG_YMM2,
          /// ESRC=0xB | OUTREG=XED_REG_YMM3
          XED_REG_YMM3,
          /// ESRC=0xC | OUTREG=XED_REG_YMM4
          XED_REG_YMM4,
          /// ESRC=0xD | OUTREG=XED_REG_YMM5
          XED_REG_YMM5,
          /// ESRC=0xE | OUTREG=XED_REG_YMM6
          XED_REG_YMM6,
          /// ESRC=0xF | OUTREG=XED_REG_YMM7
          XED_REG_YMM7,
  };
  static constexpr Register_Class YMM_SE32 = YMM_SE32_array;

  /// xed_reg_enum_t XMM_N()::
  static constexpr Register_Class XMM_N(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | XMM_N_32
      case 16: return XMM_N_32;
        /// cpu_mode=32 | XMM_N_32
      case 32: return XMM_N_32;
        /// cpu_mode=64 | XMM_N_64
      case 64: return XMM_N_64;
    }
    abort();
  }

  /// xed_reg_enum_t XMM_N_32()::
  static constexpr xed_reg_enum_t XMM_N_32_array[] = {
          /// VEXDEST210=7  | XED_REG_XMM0
          XED_REG_XMM0,
          /// VEXDEST210=6  | XED_REG_XMM1
          XED_REG_XMM1,
          /// VEXDEST210=5  | XED_REG_XMM2
          XED_REG_XMM2,
          /// VEXDEST210=4  | XED_REG_XMM3
          XED_REG_XMM3,
          /// VEXDEST210=3  | XED_REG_XMM4
          XED_REG_XMM4,
          /// VEXDEST210=2  | XED_REG_XMM5
          XED_REG_XMM5,
          /// VEXDEST210=1  | XED_REG_XMM6
          XED_REG_XMM6,
          /// VEXDEST210=0  | XED_REG_XMM7
          XED_REG_XMM7,
  };
  static constexpr Register_Class XMM_N_32 = XMM_N_32_array;

  /// xed_reg_enum_t XMM_N_64()::
  static constexpr xed_reg_enum_t XMM_N_64_array[] = {
          /// VEXDEST3=1 VEXDEST210=7  | XED_REG_XMM0
          XED_REG_XMM0,
          /// VEXDEST3=1 VEXDEST210=6  | XED_REG_XMM1
          XED_REG_XMM1,
          /// VEXDEST3=1 VEXDEST210=5  | XED_REG_XMM2
          XED_REG_XMM2,
          /// VEXDEST3=1 VEXDEST210=4  | XED_REG_XMM3
          XED_REG_XMM3,
          /// VEXDEST3=1 VEXDEST210=3  | XED_REG_XMM4
          XED_REG_XMM4,
          /// VEXDEST3=1 VEXDEST210=2  | XED_REG_XMM5
          XED_REG_XMM5,
          /// VEXDEST3=1 VEXDEST210=1  | XED_REG_XMM6
          XED_REG_XMM6,
          /// VEXDEST3=1 VEXDEST210=0  | XED_REG_XMM7
          XED_REG_XMM7,
          /// VEXDEST3=0 VEXDEST210=7  | XED_REG_XMM8
          XED_REG_XMM8,
          /// VEXDEST3=0 VEXDEST210=6  | XED_REG_XMM9
          XED_REG_XMM9,
          /// VEXDEST3=0 VEXDEST210=5  | XED_REG_XMM10
          XED_REG_XMM10,
          /// VEXDEST3=0 VEXDEST210=4  | XED_REG_XMM11
          XED_REG_XMM11,
          /// VEXDEST3=0 VEXDEST210=3  | XED_REG_XMM12
          XED_REG_XMM12,
          /// VEXDEST3=0 VEXDEST210=2  | XED_REG_XMM13
          XED_REG_XMM13,
          /// VEXDEST3=0 VEXDEST210=1  | XED_REG_XMM14
          XED_REG_XMM14,
          /// VEXDEST3=0 VEXDEST210=0  | XED_REG_XMM15
          XED_REG_XMM15,
  };
  static constexpr Register_Class XMM_N_64 = XMM_N_64_array;

  /// xed_reg_enum_t YMM_N()::
  static constexpr Register_Class YMM_N(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | YMM_N_32
      case 16: return YMM_N_32;
        /// cpu_mode=32 | YMM_N_32
      case 32: return YMM_N_32;
        /// cpu_mode=64 | YMM_N_64
      case 64: return YMM_N_64;
    }
    abort();
  }

  /// xed_reg_enum_t YMM_N_32()::
  static constexpr xed_reg_enum_t YMM_N_32_array[] = {
          /// VEXDEST210=7  | XED_REG_YMM0
          XED_REG_YMM0,
          /// VEXDEST210=6  | XED_REG_YMM1
          XED_REG_YMM1,
          /// VEXDEST210=5  | XED_REG_YMM2
          XED_REG_YMM2,
          /// VEXDEST210=4  | XED_REG_YMM3
          XED_REG_YMM3,
          /// VEXDEST210=3  | XED_REG_YMM4
          XED_REG_YMM4,
          /// VEXDEST210=2  | XED_REG_YMM5
          XED_REG_YMM5,
          /// VEXDEST210=1  | XED_REG_YMM6
          XED_REG_YMM6,
          /// VEXDEST210=0  | XED_REG_YMM7
          XED_REG_YMM7,
  };
  static constexpr Register_Class YMM_N_32 = YMM_N_32_array;

  /// xed_reg_enum_t YMM_N_64()::
  static constexpr xed_reg_enum_t YMM_N_64_array[] = {
          /// VEXDEST3=1 VEXDEST210=7  | XED_REG_YMM0
          XED_REG_YMM0,
          /// VEXDEST3=1 VEXDEST210=6  | XED_REG_YMM1
          XED_REG_YMM1,
          /// VEXDEST3=1 VEXDEST210=5  | XED_REG_YMM2
          XED_REG_YMM2,
          /// VEXDEST3=1 VEXDEST210=4  | XED_REG_YMM3
          XED_REG_YMM3,
          /// VEXDEST3=1 VEXDEST210=3  | XED_REG_YMM4
          XED_REG_YMM4,
          /// VEXDEST3=1 VEXDEST210=2  | XED_REG_YMM5
          XED_REG_YMM5,
          /// VEXDEST3=1 VEXDEST210=1  | XED_REG_YMM6
          XED_REG_YMM6,
          /// VEXDEST3=1 VEXDEST210=0  | XED_REG_YMM7
          XED_REG_YMM7,
          /// VEXDEST3=0 VEXDEST210=7  | XED_REG_YMM8
          XED_REG_YMM8,
          /// VEXDEST3=0 VEXDEST210=6  | XED_REG_YMM9
          XED_REG_YMM9,
          /// VEXDEST3=0 VEXDEST210=5  | XED_REG_YMM10
          XED_REG_YMM10,
          /// VEXDEST3=0 VEXDEST210=4  | XED_REG_YMM11
          XED_REG_YMM11,
          /// VEXDEST3=0 VEXDEST210=3  | XED_REG_YMM12
          XED_REG_YMM12,
          /// VEXDEST3=0 VEXDEST210=2  | XED_REG_YMM13
          XED_REG_YMM13,
          /// VEXDEST3=0 VEXDEST210=1  | XED_REG_YMM14
          XED_REG_YMM14,
          /// VEXDEST3=0 VEXDEST210=0  | XED_REG_YMM15
          XED_REG_YMM15,
  };
  static constexpr Register_Class YMM_N_64 = YMM_N_64_array;

  /// xed_reg_enum_t YMM_R()::
  static constexpr Register_Class YMM_R(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | YMM_R_32
      case 16: return YMM_R_32;
        /// cpu_mode=32 | YMM_R_32
      case 32: return YMM_R_32;
        /// cpu_mode=64 | YMM_R_64
      case 64: return YMM_R_64;
    }
    abort();
  }

  /// xed_reg_enum_t YMM_R_32()::
  static constexpr xed_reg_enum_t YMM_R_32_array[] = {
          /// REG=0   | XED_REG_YMM0
          XED_REG_YMM0,
          /// REG=1   | XED_REG_YMM1
          XED_REG_YMM1,
          /// REG=2   | XED_REG_YMM2
          XED_REG_YMM2,
          /// REG=3   | XED_REG_YMM3
          XED_REG_YMM3,
          /// REG=4   | XED_REG_YMM4
          XED_REG_YMM4,
          /// REG=5   | XED_REG_YMM5
          XED_REG_YMM5,
          /// REG=6   | XED_REG_YMM6
          XED_REG_YMM6,
          /// REG=7   | XED_REG_YMM7
          XED_REG_YMM7,
  };
  static constexpr Register_Class YMM_R_32 = YMM_R_32_array;

  /// xed_reg_enum_t YMM_R_64()::
  static constexpr xed_reg_enum_t YMM_R_64_array[] = {
          /// REXR=0 REG=0   | XED_REG_YMM0
          XED_REG_YMM0,
          /// REXR=0 REG=1   | XED_REG_YMM1
          XED_REG_YMM1,
          /// REXR=0 REG=2   | XED_REG_YMM2
          XED_REG_YMM2,
          /// REXR=0 REG=3   | XED_REG_YMM3
          XED_REG_YMM3,
          /// REXR=0 REG=4   | XED_REG_YMM4
          XED_REG_YMM4,
          /// REXR=0 REG=5   | XED_REG_YMM5
          XED_REG_YMM5,
          /// REXR=0 REG=6   | XED_REG_YMM6
          XED_REG_YMM6,
          /// REXR=0 REG=7   | XED_REG_YMM7
          XED_REG_YMM7,
          /// REXR=1 REG=0   | XED_REG_YMM8
          XED_REG_YMM8,
          /// REXR=1 REG=1   | XED_REG_YMM9
          XED_REG_YMM9,
          /// REXR=1 REG=2   | XED_REG_YMM10
          XED_REG_YMM10,
          /// REXR=1 REG=3   | XED_REG_YMM11
          XED_REG_YMM11,
          /// REXR=1 REG=4   | XED_REG_YMM12
          XED_REG_YMM12,
          /// REXR=1 REG=5   | XED_REG_YMM13
          XED_REG_YMM13,
          /// REXR=1 REG=6   | XED_REG_YMM14
          XED_REG_YMM14,
          /// REXR=1 REG=7   | XED_REG_YMM15
          XED_REG_YMM15,
  };
  static constexpr Register_Class YMM_R_64 = YMM_R_64_array;

  /// xed_reg_enum_t YMM_B()::
  static constexpr Register_Class YMM_B(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | YMM_B_32
      case 16: return YMM_B_32;
        /// cpu_mode=32 | YMM_B_32
      case 32: return YMM_B_32;
        /// cpu_mode=64 | YMM_B_64
      case 64: return YMM_B_64;
    }
    abort();
  }

  /// xed_reg_enum_t YMM_B_32()::
  static constexpr xed_reg_enum_t YMM_B_32_array[] = {
          /// RM=0   | XED_REG_YMM0
          XED_REG_YMM0,
          /// RM=1   | XED_REG_YMM1
          XED_REG_YMM1,
          /// RM=2   | XED_REG_YMM2
          XED_REG_YMM2,
          /// RM=3   | XED_REG_YMM3
          XED_REG_YMM3,
          /// RM=4   | XED_REG_YMM4
          XED_REG_YMM4,
          /// RM=5   | XED_REG_YMM5
          XED_REG_YMM5,
          /// RM=6   | XED_REG_YMM6
          XED_REG_YMM6,
          /// RM=7   | XED_REG_YMM7
          XED_REG_YMM7,
  };
  static constexpr Register_Class YMM_B_32 = YMM_B_32_array;

  /// xed_reg_enum_t YMM_B_64()::
  static constexpr xed_reg_enum_t YMM_B_64_array[] = {
          /// REXB=0 RM=0   | XED_REG_YMM0
          XED_REG_YMM0,
          /// REXB=0 RM=1   | XED_REG_YMM1
          XED_REG_YMM1,
          /// REXB=0 RM=2   | XED_REG_YMM2
          XED_REG_YMM2,
          /// REXB=0 RM=3   | XED_REG_YMM3
          XED_REG_YMM3,
          /// REXB=0 RM=4   | XED_REG_YMM4
          XED_REG_YMM4,
          /// REXB=0 RM=5   | XED_REG_YMM5
          XED_REG_YMM5,
          /// REXB=0 RM=6   | XED_REG_YMM6
          XED_REG_YMM6,
          /// REXB=0 RM=7   | XED_REG_YMM7
          XED_REG_YMM7,
          /// REXB=1 RM=0   | XED_REG_YMM8
          XED_REG_YMM8,
          /// REXB=1 RM=1   | XED_REG_YMM9
          XED_REG_YMM9,
          /// REXB=1 RM=2   | XED_REG_YMM10
          XED_REG_YMM10,
          /// REXB=1 RM=3   | XED_REG_YMM11
          XED_REG_YMM11,
          /// REXB=1 RM=4   | XED_REG_YMM12
          XED_REG_YMM12,
          /// REXB=1 RM=5   | XED_REG_YMM13
          XED_REG_YMM13,
          /// REXB=1 RM=6   | XED_REG_YMM14
          XED_REG_YMM14,
          /// REXB=1 RM=7   | XED_REG_YMM15
          XED_REG_YMM15,
  };
  static constexpr Register_Class YMM_B_64 = YMM_B_64_array;

  /// xed_reg_enum_t XMM_N3()::
  static constexpr Register_Class XMM_N3(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | XMM_N3_32
      case 16: return XMM_N3_32;
        /// cpu_mode=32 | XMM_N3_32
      case 32: return XMM_N3_32;
        /// cpu_mode=64 | XMM_N3_64
      case 64: return XMM_N3_64;
    }
    abort();
  }

  /// xed_reg_enum_t XMM_N3_32()::
  static constexpr xed_reg_enum_t XMM_N3_32_array[] = {
          /// VEXDEST210=7  | XED_REG_XMM0
          XED_REG_XMM0,
          /// VEXDEST210=6  | XED_REG_XMM1
          XED_REG_XMM1,
          /// VEXDEST210=5  | XED_REG_XMM2
          XED_REG_XMM2,
          /// VEXDEST210=4  | XED_REG_XMM3
          XED_REG_XMM3,
          /// VEXDEST210=3  | XED_REG_XMM4
          XED_REG_XMM4,
          /// VEXDEST210=2  | XED_REG_XMM5
          XED_REG_XMM5,
          /// VEXDEST210=1  | XED_REG_XMM6
          XED_REG_XMM6,
          /// VEXDEST210=0  | XED_REG_XMM7
          XED_REG_XMM7,
  };
  static constexpr Register_Class XMM_N3_32 = XMM_N3_32_array;

  /// xed_reg_enum_t XMM_N3_64()::
  static constexpr xed_reg_enum_t XMM_N3_64_array[] = {
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=7  | XED_REG_XMM0
          XED_REG_XMM0,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=6  | XED_REG_XMM1
          XED_REG_XMM1,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=5  | XED_REG_XMM2
          XED_REG_XMM2,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=4  | XED_REG_XMM3
          XED_REG_XMM3,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=3  | XED_REG_XMM4
          XED_REG_XMM4,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=2  | XED_REG_XMM5
          XED_REG_XMM5,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=1  | XED_REG_XMM6
          XED_REG_XMM6,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=0  | XED_REG_XMM7
          XED_REG_XMM7,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=7  | XED_REG_XMM8
          XED_REG_XMM8,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=6  | XED_REG_XMM9
          XED_REG_XMM9,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=5  | XED_REG_XMM10
          XED_REG_XMM10,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=4  | XED_REG_XMM11
          XED_REG_XMM11,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=3  | XED_REG_XMM12
          XED_REG_XMM12,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=2  | XED_REG_XMM13
          XED_REG_XMM13,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=1  | XED_REG_XMM14
          XED_REG_XMM14,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=0  | XED_REG_XMM15
          XED_REG_XMM15,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=7  | XED_REG_XMM16
          XED_REG_XMM16,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=6  | XED_REG_XMM17
          XED_REG_XMM17,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=5  | XED_REG_XMM18
          XED_REG_XMM18,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=4  | XED_REG_XMM19
          XED_REG_XMM19,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=3  | XED_REG_XMM20
          XED_REG_XMM20,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=2  | XED_REG_XMM21
          XED_REG_XMM21,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=1  | XED_REG_XMM22
          XED_REG_XMM22,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=0  | XED_REG_XMM23
          XED_REG_XMM23,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=7  | XED_REG_XMM24
          XED_REG_XMM24,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=6  | XED_REG_XMM25
          XED_REG_XMM25,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=5  | XED_REG_XMM26
          XED_REG_XMM26,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=4  | XED_REG_XMM27
          XED_REG_XMM27,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=3  | XED_REG_XMM28
          XED_REG_XMM28,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=2  | XED_REG_XMM29
          XED_REG_XMM29,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=1  | XED_REG_XMM30
          XED_REG_XMM30,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=0  | XED_REG_XMM31
          XED_REG_XMM31,
  };
  static constexpr Register_Class XMM_N3_64 = XMM_N3_64_array;

  /// xed_reg_enum_t YMM_N3()::
  static constexpr Register_Class YMM_N3(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | YMM_N3_32
      case 16: return YMM_N3_32;
        /// cpu_mode=32 | YMM_N3_32
      case 32: return YMM_N3_32;
        /// cpu_mode=64 | YMM_N3_64
      case 64: return YMM_N3_64;
    }
    abort();
  }

  /// xed_reg_enum_t YMM_N3_32()::
  static constexpr xed_reg_enum_t YMM_N3_32_array[] = {
          /// VEXDEST210=7  | XED_REG_YMM0
          XED_REG_YMM0,
          /// VEXDEST210=6  | XED_REG_YMM1
          XED_REG_YMM1,
          /// VEXDEST210=5  | XED_REG_YMM2
          XED_REG_YMM2,
          /// VEXDEST210=4  | XED_REG_YMM3
          XED_REG_YMM3,
          /// VEXDEST210=3  | XED_REG_YMM4
          XED_REG_YMM4,
          /// VEXDEST210=2  | XED_REG_YMM5
          XED_REG_YMM5,
          /// VEXDEST210=1  | XED_REG_YMM6
          XED_REG_YMM6,
          /// VEXDEST210=0  | XED_REG_YMM7
          XED_REG_YMM7,
  };
  static constexpr Register_Class YMM_N3_32 = YMM_N3_32_array;

  /// xed_reg_enum_t YMM_N3_64()::
  static constexpr xed_reg_enum_t YMM_N3_64_array[] = {
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=7  | XED_REG_YMM0
          XED_REG_YMM0,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=6  | XED_REG_YMM1
          XED_REG_YMM1,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=5  | XED_REG_YMM2
          XED_REG_YMM2,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=4  | XED_REG_YMM3
          XED_REG_YMM3,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=3  | XED_REG_YMM4
          XED_REG_YMM4,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=2  | XED_REG_YMM5
          XED_REG_YMM5,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=1  | XED_REG_YMM6
          XED_REG_YMM6,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=0  | XED_REG_YMM7
          XED_REG_YMM7,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=7  | XED_REG_YMM8
          XED_REG_YMM8,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=6  | XED_REG_YMM9
          XED_REG_YMM9,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=5  | XED_REG_YMM10
          XED_REG_YMM10,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=4  | XED_REG_YMM11
          XED_REG_YMM11,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=3  | XED_REG_YMM12
          XED_REG_YMM12,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=2  | XED_REG_YMM13
          XED_REG_YMM13,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=1  | XED_REG_YMM14
          XED_REG_YMM14,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=0  | XED_REG_YMM15
          XED_REG_YMM15,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=7  | XED_REG_YMM16
          XED_REG_YMM16,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=6  | XED_REG_YMM17
          XED_REG_YMM17,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=5  | XED_REG_YMM18
          XED_REG_YMM18,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=4  | XED_REG_YMM19
          XED_REG_YMM19,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=3  | XED_REG_YMM20
          XED_REG_YMM20,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=2  | XED_REG_YMM21
          XED_REG_YMM21,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=1  | XED_REG_YMM22
          XED_REG_YMM22,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=0  | XED_REG_YMM23
          XED_REG_YMM23,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=7  | XED_REG_YMM24
          XED_REG_YMM24,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=6  | XED_REG_YMM25
          XED_REG_YMM25,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=5  | XED_REG_YMM26
          XED_REG_YMM26,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=4  | XED_REG_YMM27
          XED_REG_YMM27,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=3  | XED_REG_YMM28
          XED_REG_YMM28,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=2  | XED_REG_YMM29
          XED_REG_YMM29,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=1  | XED_REG_YMM30
          XED_REG_YMM30,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=0  | XED_REG_YMM31
          XED_REG_YMM31,
  };
  static constexpr Register_Class YMM_N3_64 = YMM_N3_64_array;

  /// xed_reg_enum_t ZMM_N3()::
  static constexpr Register_Class ZMM_N3(unsigned cpu_mode) {
    switch (cpu_mode) {
      /// cpu_mode=16 | ZMM_N3_32
      case 16: return ZMM_N3_32;
        /// cpu_mode=32 | ZMM_N3_32
      case 32: return ZMM_N3_32;
        /// cpu_mode=64 | ZMM_N3_64
      case 64: return ZMM_N3_64;
    }
    abort();
  }

  /// xed_reg_enum_t ZMM_N3_32()::
  static constexpr xed_reg_enum_t ZMM_N3_32_array[] = {
          /// VEXDEST210=7  | XED_REG_ZMM0
          XED_REG_ZMM0,
          /// VEXDEST210=6  | XED_REG_ZMM1
          XED_REG_ZMM1,
          /// VEXDEST210=5  | XED_REG_ZMM2
          XED_REG_ZMM2,
          /// VEXDEST210=4  | XED_REG_ZMM3
          XED_REG_ZMM3,
          /// VEXDEST210=3  | XED_REG_ZMM4
          XED_REG_ZMM4,
          /// VEXDEST210=2  | XED_REG_ZMM5
          XED_REG_ZMM5,
          /// VEXDEST210=1  | XED_REG_ZMM6
          XED_REG_ZMM6,
          /// VEXDEST210=0  | XED_REG_ZMM7
          XED_REG_ZMM7,
  };
  static constexpr Register_Class ZMM_N3_32 = ZMM_N3_32_array;

  /// xed_reg_enum_t ZMM_N3_64()::
  static constexpr xed_reg_enum_t ZMM_N3_64_array[] = {
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=7  | XED_REG_ZMM0
          XED_REG_ZMM0,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=6  | XED_REG_ZMM1
          XED_REG_ZMM1,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=5  | XED_REG_ZMM2
          XED_REG_ZMM2,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=4  | XED_REG_ZMM3
          XED_REG_ZMM3,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=3  | XED_REG_ZMM4
          XED_REG_ZMM4,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=2  | XED_REG_ZMM5
          XED_REG_ZMM5,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=1  | XED_REG_ZMM6
          XED_REG_ZMM6,
          /// VEXDEST4=0 VEXDEST3=1 VEXDEST210=0  | XED_REG_ZMM7
          XED_REG_ZMM7,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=7  | XED_REG_ZMM8
          XED_REG_ZMM8,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=6  | XED_REG_ZMM9
          XED_REG_ZMM9,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=5  | XED_REG_ZMM10
          XED_REG_ZMM10,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=4  | XED_REG_ZMM11
          XED_REG_ZMM11,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=3  | XED_REG_ZMM12
          XED_REG_ZMM12,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=2  | XED_REG_ZMM13
          XED_REG_ZMM13,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=1  | XED_REG_ZMM14
          XED_REG_ZMM14,
          /// VEXDEST4=0 VEXDEST3=0 VEXDEST210=0  | XED_REG_ZMM15
          XED_REG_ZMM15,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=7  | XED_REG_ZMM16
          XED_REG_ZMM16,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=6  | XED_REG_ZMM17
          XED_REG_ZMM17,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=5  | XED_REG_ZMM18
          XED_REG_ZMM18,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=4  | XED_REG_ZMM19
          XED_REG_ZMM19,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=3  | XED_REG_ZMM20
          XED_REG_ZMM20,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=2  | XED_REG_ZMM21
          XED_REG_ZMM21,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=1  | XED_REG_ZMM22
          XED_REG_ZMM22,
          /// VEXDEST4=1 VEXDEST3=1 VEXDEST210=0  | XED_REG_ZMM23
          XED_REG_ZMM23,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=7  | XED_REG_ZMM24
          XED_REG_ZMM24,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=6  | XED_REG_ZMM25
          XED_REG_ZMM25,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=5  | XED_REG_ZMM26
          XED_REG_ZMM26,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=4  | XED_REG_ZMM27
          XED_REG_ZMM27,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=3  | XED_REG_ZMM28
          XED_REG_ZMM28,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=2  | XED_REG_ZMM29
          XED_REG_ZMM29,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=1  | XED_REG_ZMM30
          XED_REG_ZMM30,
          /// VEXDEST4=1 VEXDEST3=0 VEXDEST210=0  | XED_REG_ZMM31
          XED_REG_ZMM31,
  };
  static constexpr Register_Class ZMM_N3_64 = ZMM_N3_64_array;
};

} // end anonymous namespace

std::optional<xed_reg_enum_t> XED_Register_Info::reg(xed_nonterminal_enum_t nonterminal,
                                                     const XED_Encoder_State &state) {
  const unsigned cpu_width   = state.cpu_mode_bits();
  const unsigned stack_width = state.stack_addr_bits();
  const unsigned EOSZ        = state.xed_eosz();
  const unsigned EASZ        = state.xed_easz();

  switch (nonterminal) {
    case XED_NONTERMINAL_RFLAGS: return XED_Regs::rFLAGS(cpu_width);
    case XED_NONTERMINAL_RIP:    return XED_Regs::rIP(cpu_width);

    case XED_NONTERMINAL_SRSP: return XED_Regs::SrSP(stack_width);
    case XED_NONTERMINAL_SRBP: return XED_Regs::SrBP(stack_width);

    case XED_NONTERMINAL_OEAX: return XED_Regs::OeAX(EOSZ);
    case XED_NONTERMINAL_ORAX: return XED_Regs::OrAX(EOSZ);
    case XED_NONTERMINAL_ORBX: return XED_Regs::OrBX(EOSZ);
    case XED_NONTERMINAL_ORCX: return XED_Regs::OrCX(EOSZ);
    case XED_NONTERMINAL_ORDX: return XED_Regs::OrDX(EOSZ);
    case XED_NONTERMINAL_ORBP: return XED_Regs::OrBP(EOSZ);
    case XED_NONTERMINAL_ORSP: return XED_Regs::OrSP(EOSZ);

    case XED_NONTERMINAL_AR8:  return XED_Regs::Ar8(EASZ);
    case XED_NONTERMINAL_AR9:  return XED_Regs::Ar9(EASZ);
    case XED_NONTERMINAL_AR10: return XED_Regs::Ar10(EASZ);
    case XED_NONTERMINAL_AR11: return XED_Regs::Ar11(EASZ);
    case XED_NONTERMINAL_AR12: return XED_Regs::Ar12(EASZ);
    case XED_NONTERMINAL_AR13: return XED_Regs::Ar13(EASZ);
    case XED_NONTERMINAL_AR14: return XED_Regs::Ar14(EASZ);
    case XED_NONTERMINAL_AR15: return XED_Regs::Ar15(EASZ);
    case XED_NONTERMINAL_ARAX: return XED_Regs::ArAX(EASZ);
    case XED_NONTERMINAL_ARBP: return XED_Regs::ArBP(EASZ);
    case XED_NONTERMINAL_ARBX: return XED_Regs::ArBX(EASZ);
    case XED_NONTERMINAL_ARCX: return XED_Regs::ArCX(EASZ);
    case XED_NONTERMINAL_ARDI: return XED_Regs::ArDI(EASZ);
    case XED_NONTERMINAL_ARDX: return XED_Regs::ArDX(EASZ);
    case XED_NONTERMINAL_ARSI: return XED_Regs::ArSI(EASZ);
    case XED_NONTERMINAL_ARSP: return XED_Regs::ArSP(EASZ);
    case XED_NONTERMINAL_RIPA: return XED_Regs::rIPa(EASZ);

    default: return std::nullopt;
  }
}

std::optional<Register_Class> XED_Register_Info::reg_class(xed_nonterminal_enum_t nonterminal,
                                                           const XED_Encoder_State &state) {
  const unsigned cpu_width   = state.cpu_mode_bits();
//  const unsigned stack_width = state.stack_addr_bits();
  const unsigned EOSZ        = state.xed_eosz();

  switch (nonterminal) {
    /// *****
    case XED_NONTERMINAL_GPR8_B:  return XED_Regs::GPR8_B(state.allow_rex);
    case XED_NONTERMINAL_GPR16_B: return XED_Regs::GPR16_B;
    case XED_NONTERMINAL_GPR32_B: return XED_Regs::GPR32_B;
    case XED_NONTERMINAL_GPR64_B: return XED_Regs::GPR64_B;

    case XED_NONTERMINAL_GPR8_R:  return XED_Regs::GPR8_R(state.allow_rex);
    case XED_NONTERMINAL_GPR16_R: return XED_Regs::GPR16_R;
    case XED_NONTERMINAL_GPR32_R: return XED_Regs::GPR32_R;
    case XED_NONTERMINAL_GPR64_R: return XED_Regs::GPR64_R;

    case XED_NONTERMINAL_GPR8_SB: return XED_Regs::GPR8_SB(state.allow_rex);

    case XED_NONTERMINAL_A_GPR_R: return XED_Regs::A_GPR_R(state.allow_rex);
    case XED_NONTERMINAL_A_GPR_B: return XED_Regs::A_GPR_B(state.allow_rex);

    case XED_NONTERMINAL_MMX_R:   return XED_Regs::MMX_R;
    case XED_NONTERMINAL_MMX_B:   return XED_Regs::MMX_B;

    case XED_NONTERMINAL_XMM_B_32: return XED_Regs::XMM_B_32;
    case XED_NONTERMINAL_XMM_B_64: return XED_Regs::XMM_B_64;
    case XED_NONTERMINAL_XMM_R_32: return XED_Regs::XMM_R_32;
    case XED_NONTERMINAL_XMM_R_64: return XED_Regs::XMM_R_64;

    case XED_NONTERMINAL_YMM_B_32: return XED_Regs::YMM_B_32;
    case XED_NONTERMINAL_YMM_B_64: return XED_Regs::YMM_B_64;
    case XED_NONTERMINAL_YMM_R_32: return XED_Regs::YMM_R_32;
    case XED_NONTERMINAL_YMM_R_64: return XED_Regs::YMM_R_64;

    case XED_NONTERMINAL_ZMM_B3_32: return XED_Regs::ZMM_B3_32;
    case XED_NONTERMINAL_ZMM_B3_64: return XED_Regs::ZMM_B3_64;

    case XED_NONTERMINAL_VGPR64_B: return XED_Regs::VGPR64_B;
    case XED_NONTERMINAL_VGPR64_R: return XED_Regs::VGPR64_R;
    case XED_NONTERMINAL_VGPR64_N: return XED_Regs::VGPR64_N;

    case XED_NONTERMINAL_X87:      return XED_Regs::X87;

      /// *****
    case XED_NONTERMINAL_XMM_R:    return XED_Regs::XMM_R(cpu_width);
    case XED_NONTERMINAL_YMM_R:    return XED_Regs::YMM_R(cpu_width);
    case XED_NONTERMINAL_XMM_B:    return XED_Regs::XMM_B(cpu_width);
    case XED_NONTERMINAL_YMM_B:    return XED_Regs::YMM_B(cpu_width);
    case XED_NONTERMINAL_XMM_N:    return XED_Regs::XMM_N(cpu_width);
    case XED_NONTERMINAL_YMM_N:    return XED_Regs::YMM_N(cpu_width);

    case XED_NONTERMINAL_XMM_R3:   return XED_Regs::XMM_R3(cpu_width);
    case XED_NONTERMINAL_YMM_R3:   return XED_Regs::YMM_R3(cpu_width);
    case XED_NONTERMINAL_ZMM_R3:   return XED_Regs::ZMM_R3(cpu_width);
    case XED_NONTERMINAL_XMM_B3:   return XED_Regs::XMM_B3(cpu_width);
    case XED_NONTERMINAL_YMM_B3:   return XED_Regs::YMM_B3(cpu_width);
    case XED_NONTERMINAL_ZMM_B3:   return XED_Regs::ZMM_B3(cpu_width);
    case XED_NONTERMINAL_XMM_N3:   return XED_Regs::XMM_N3(cpu_width);
    case XED_NONTERMINAL_YMM_N3:   return XED_Regs::YMM_N3(cpu_width);
    case XED_NONTERMINAL_ZMM_N3:   return XED_Regs::ZMM_N3(cpu_width);

    case XED_NONTERMINAL_XMM_SE:   return XED_Regs::XMM_SE(cpu_width);
    case XED_NONTERMINAL_YMM_SE:   return XED_Regs::YMM_SE(cpu_width);

    case XED_NONTERMINAL_VGPR32_B: return XED_Regs::VGPR32_B(cpu_width);
    case XED_NONTERMINAL_VGPR32_R: return XED_Regs::VGPR32_R(cpu_width);
    case XED_NONTERMINAL_VGPR32_N: return XED_Regs::VGPR32_N(cpu_width);

      /// *****
    case XED_NONTERMINAL_GPRV_R:   return XED_Regs::GPRv_R(EOSZ);
    case XED_NONTERMINAL_GPRV_B:   return XED_Regs::GPRv_B(EOSZ);
    case XED_NONTERMINAL_GPRV_SB:  return XED_Regs::GPRv_SB(EOSZ);

    case XED_NONTERMINAL_GPRY_R:   return XED_Regs::GPRy_R(EOSZ);
    case XED_NONTERMINAL_GPRY_B:   return XED_Regs::GPRy_B(EOSZ);

    case XED_NONTERMINAL_GPRZ_R:   return XED_Regs::GPRz_R(EOSZ);
    case XED_NONTERMINAL_GPRZ_B:   return XED_Regs::GPRz_B(EOSZ);

      /// *****
    case XED_NONTERMINAL_VGPRY_N:  return XED_Regs::VGPRy_N(EOSZ, cpu_width);
    default: return std::nullopt;
  }
}

xed_reg_enum_t XED_Register_Info::base_reg(const XED_Encoder_State &state) {
  return XED_Regs::ArAX(state.xed_easz());
}


bool XED_Instruction_Operand::is_reg_op() const {
  return xed_operand_is_register(name());
}

bool XED_Instruction_Operand::is_base_op() const {
  switch (name()) {
    case XED_OPERAND_BASE0:
    case XED_OPERAND_BASE1:
      return true;
    default:
      return false;
  }
}

std::optional<xed_reg_enum_t> XED_Instruction_Operand::get_reg(const XED_Encoder_State &state) const {
  if (xed_operand_type(raw()) == XED_OPERAND_TYPE_REG) {
    return xed_operand_reg(raw());
  }

  if (is_lookup_fn()) {
    return XED_Register_Info::reg(get_lookup_fn(), state);
  }

  return std::nullopt;
}

std::optional<Register_Class> XED_Instruction_Operand::get_reg_class(const XED_Encoder_State &state) const {
  if (!is_lookup_fn()) { return std::nullopt; }

  return XED_Register_Info::reg_class(get_lookup_fn(), state);
}
